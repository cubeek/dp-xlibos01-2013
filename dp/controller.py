"""
Controller module
"""
try:
    from concurrent.futures import ThreadPoolExecutor
except ImportError:
    ThreadPoolExecutor = None
import logging
import time

from dp import generator
from dp.classifier.classifier import BuildingClassifier
from dp import common
from dp.config import config_parser
from dp.elements import GlassMesh
from dp import heightmap
from dp.view.window import SDLWindow
from dp.world import World


LOGGER = logging.getLogger('dpLogger')


class ControllerException(Exception):
    pass


class Controller(object):
    """
    Controller that holds window hanlder and renderer structure
    """
    def __init__(self, width, height, title):
        """
        Controller that holds libraries and window handler

        @param width: Width of the window in pixels
        @param height: Height of the window in pixels
        @param title: Title of the window
        """
        from dp.view import renderer
        self.lib = renderer
        self.window = SDLWindow(renderer, width, height, title)
        self.generators = None
        self._create_generators()
        self.world = World()
        self.classifier = None
        self.vertex_shader_path = None
        self.fragment_shader_path = None
        self.vert_nums = 0
        self.face_nums = 0

    def init_renderer(self, mode):
        """
        Initialize renderer
        """
        self.lp_renderer = self.lib.get_renderer()
        self.lp_renderer.contents.mode = mode
        ecode = self.lib.init_renderer(self.lp_renderer,
                                       self.vertex_shader_path,
                                       self.fragment_shader_path)
        if ecode != 0:
            raise ControllerException("Failed to initialize renderer")
        self.renderer = self.lp_renderer.contents
        if self.renderer.mode == self.lib.VIEW:
            generator.Generator.load_textures_from_dir(common.TEXTURE_DIR)
            self._load_textures_to_renderer(generator.Generator.textures)
        self.renderer.view_width = 800
        self.renderer.view_height = 600

    def _create_generators(self):
        """
        Creates generator for all types of building
        """
        self.generators = [
            generator.BOFGenerator(),
            generator.Generator(),
            generator.HouseGenerator(),
            generator.Generator(),
        ]

    def _load_textures_to_renderer(self, textures):
        """
        Load textures to renderer
        """
        self.lib.allocate_texture_memory(self.lp_renderer,
                                         textures.loaded_textures)
# Set color textures first, treate specially
        for index, c_textures in textures.colors:
            self.lib.load_texture(self.renderer.c_textures, c_textures, index)
        for key in textures:
            for index, path in textures[key]:
                self.lib.load_texture(self.renderer.textures, path, index)

    @property
    def world_size(self):
        """
        Property for setting world size
        """
        return self.world.projector.width, self.world.projector.height

    def create_classifier(self, training_file, testing_file):
        """
        Creates new classifier ready to be trained

        @param training_file: File containing dataset for training classifier
        @param testing_file: File containing dataset for testing classifier
        """
        self.classifier = BuildingClassifier(training_file, testing_file)

    def load_classifier(self, classifier_path, training_file):
        """
        Load classifier that is already trained

        @param classifier_path: Path to file containing trained classifier
        @param training_file: Path to file containing training dataset
        """
        self.classifier = BuildingClassifier(classifier_path, training_file)
        self.classifier.load()

    def load_height_map(self, server):
        """
        Stores heightmap to renderer

        @param server: Google Elevation API server
        """
        density = config_parser.get('heightmap', 'density')
        self.world.heightmap = heightmap.HeightMap(
            self.world.bounds, density, server, self.world.projector)

    def load_world(self, server):
        """
        Downloads and stores entities from OpenStreetMap

        @param server: OpenStreetMap server url
        """

    def set_building_classes(self):
        """
        Sets for all entities building classes from renderer
        """
        for i in xrange(self.renderer.buildings.entity_cnt):
            self.world.buildings[i].building_class = \
                self.renderer.buildings_classes[i]

    def _process_building(self, building, parallel=False):
        """
        Classifies building, sets its hight and generates 3D mesh of building

        @param building: Building that's being processed
        @param parallel: Determines whether heights of building will be
                         parallel
        """
        building.building_class = self.classifier.classify(
            building.feature_list())
        self.world.set_entrance(building)
        building.transform_nodes_to_vertices()
        self.world.set_heights(building.nodes, 0, parallel)
        start_time = time.time()
        self.generators[building.building_class].generate(building)
        building.generated_in = time.time() - start_time

    def process_buildings(self, parallel=True):
        """
        Classifies buildings and generates meshes
        """
        if parallel and ThreadPoolExecutor is not None:
            with ThreadPoolExecutor(max_workers=common.MAX_WORKERS) as \
                    executor:
                result = executor.map(self._process_building,
                                      self.world.buildings)
                for res in result:
                    pass
        else:
            for building in self.world.buildings:
                self._process_building(building, not parallel)

    def _copy_mesh_to_renderer(self, mesh):
        """
        Copies mesh to renderer structure

        @param mesh: Mesh to be copied
        """
        for vertex in mesh.vertices:
            self.vert_nums += 1
            self.lib.add_vertex(
                self.lp_renderer, vertex.x, vertex.y, vertex.z,
                vertex.normal.x, vertex.normal.y, vertex.normal.z,
                vertex.s, vertex.t)
        for index1, index2, index3 in mesh.tri_indices:
            self.face_nums += 1
            self.lib.add_index(self.lp_renderer, index1)
            self.lib.add_index(self.lp_renderer, index2)
            self.lib.add_index(self.lp_renderer, index3)

    def _copy_ways_to_renderer(self):
        """
        Subroutine for copying ways to gpu
        """
        for mesh in self.world.ways.meshes:
            self.lib.add_way(self.lp_renderer, len(mesh.faces) * 3,
                             mesh.texture)
            self._copy_mesh_to_renderer(mesh)

    def _copy_buildings_to_renderer(self):
        glasses = list()
        for building in self.world.buildings:
            verts = 0
            faces = 0
            for mesh in building.meshes:
                if isinstance(mesh, GlassMesh):
                    glasses.append(mesh)
                else:
                    self.lib.add_building(self.lp_renderer, len(mesh.faces)*3,
                                          mesh.texture)
                    for vertex in mesh.vertices:
                        verts += 1
                        self.lib.add_vertex(
                            self.lp_renderer, vertex.x, vertex.y, vertex.z,
                            vertex.normal.x, vertex.normal.y,
                            vertex.normal.z, vertex.s, vertex.t)
                    for index1, index2, index3 in mesh.tri_indices:
                        faces += 1
                        self.lib.add_index(self.lp_renderer, index1)
                        self.lib.add_index(self.lp_renderer, index2)
                        self.lib.add_index(self.lp_renderer, index3)
            self.vert_nums += verts
            self.face_nums += faces
            LOGGER.debug("Building generated in %f with %d faces and %d "
                         "vertices", building.generated_in, faces, verts)
        for glass in glasses:
            self.lib.add_glass(self.lp_renderer, len(glass.faces) * 3,
                               glass.texture)
            self._copy_mesh_to_renderer(glass)

    def _copy_heightmap_to_renderer(self):
        if self.world.heightmap is not None:
            texture = generator.Generator.textures.get_index('grass')
            self.world.heightmap.texutre = texture
            self.lib.add_heightmap(
                self.lp_renderer, len(self.world.heightmap.faces) * 3,
                texture)
            for vertex in self.world.heightmap.vertices:
                self.vert_nums += 1
                self.lib.add_vertex(self.lp_renderer,
                                    vertex.x, vertex.y, vertex.z,
                                    vertex.normal.x, vertex.normal.y,
                                    vertex.normal.z, vertex.s, vertex.t)
            for face in self.world.heightmap.faces:
                self.face_nums += 1
                self.lib.add_index(self.lp_renderer, face[0].index)
                self.lib.add_index(self.lp_renderer, face[1].index)
                self.lib.add_index(self.lp_renderer, face[2].index)

    def copy_entities_to_gpu(self, output=None):
        """
        Copies all entities from world to GPU
        """

        self._copy_ways_to_renderer()
        self._copy_heightmap_to_renderer()
        self._copy_buildings_to_renderer()
        LOGGER.info("Generated %d faces and %d vertices", self.face_nums,
                    self.vert_nums)

        self.lib.copy_data_to_buffers(self.lp_renderer)
        if output is not None:
            self.world.save_to_file(output)
        if self.renderer.mode == self.lib.VIEW:
            for building in self.world.buildings:
                del building
            del self.world.buildings
            for way in self.world.ways:
                del way
            del self.world.ways
            del self.world.heightmap

    def copy_pickable_to_gpu(self, world):
        """
        Loads stored world to GPU
        """
        for verts, idxs, texture, mesh_type in world:
            if mesh_type == 'way':
                self.lib.add_way(self.lp_renderer, len(idxs), texture)
            elif mesh_type == 'hm':
                self.lib.add_heightmap(self.lp_renderer, len(idxs), texture)
            elif mesh_type == 'building':
                self.lib.add_building(self.lp_renderer, len(idxs), texture)
            for vertex in verts:
                self.lib.add_vertex(self.lp_renderer, *vertex)
            for index in idxs:
                self.lib.add_index(self.lp_renderer, index)
        self.lib.copy_data_to_buffers(self.lp_renderer)
