"""
Module with HeightMap class which creates height map for OpenGL
"""
import logging
import numpy

from dp.computations.ageometr import Vector2, Vector3, Plane, Line, \
                                     fits_interval, contains_point
from dp.downloader.ggapi import GoogleElevationAPI
from dp.elements import Vertex, Vertices, Face

LOGGER = logging.getLogger("dpLogger")
PRECISION = 0.01


class HeightMapException(Exception):
    """
    HM base exception
    """
    pass


class PlaneNotFound(HeightMapException):
    """
    When plane wasn't found in planes
    """
    pass


class HeightMap(object):
    """
    Height map built from MapPoints
    """

    def __init__(self, rect, density, server, projector):
        """
        Initialize attributes and creates height map of listMap

        @param rect: Tupple (left, bottom, right, top)
        @param density: Density of heightmap for one axis
        @param server: Google Elevation API server URL
        @param projector: Projector to use for mapping to cartesian coordinates
        """
        self._heights = list()
        self.density = density
        self.projector = projector
        self.vertices = Vertices()
        self.faces = list()
        LOGGER.info("Height map boundries: %s" , rect)
        self._download_map(rect, server)

        self.edge_x_length = self._heights[0][1]['x'] - \
                             self._heights[0][0]['x']
        self.edge_y_length = self._heights[1][0]['y'] - \
                             self._heights[0][0]['y']
        self.edges = list()
        self._create_edge_lines()
        self.planes = list()
        self._create_triangles_planes()
        self.texture = -1

    def _create_triangles_planes(self):
        """
        Creates planes for all 
        """
        for y, xs in enumerate(self._heights[:-1]):
            x_count = len(xs)
            offset_y = x_count * y
            offset_next_y = x_count * (y + 1)
            for x, point in enumerate(xs[:-1]):
                # Planes
                point_x = self._heights[y][x+1]
                point_y = self._heights[y+1][x]
                point_xy = self._heights[y+1][x+1]
                first = Vector3(point['x'], point['y'], point['z'])
                second = Vector3(point_y['x'], point_y['y'], point_y['z'])
                third = Vector3(point_x['x'], point_x['y'], point_x['z'])
                self.planes.append(((first, second, third), 
                                     Plane(first, second, third)))
                first = second
                second = third
                third = Vector3(point_xy['x'], point_xy['y'], point_xy['z'])
                self.planes.append(((first, second, third), 
                                     Plane(first, second, third)))
                # Faces
                lu = offset_y + x
                lb = offset_next_y + x
                ru = offset_y + x + 1
                rb = offset_next_y + x + 1
                vertex_lu = self.vertices[lu]
                vertex_lu.s = x * self.edge_x_length
                vertex_lu.t = y * self.edge_x_length
                vertex_lb = self.vertices[lb]
                vertex_lb.s = x * self.edge_x_length
                vertex_lb.t = (y + 1) * self.edge_x_length
                vertex_ru = self.vertices[ru]
                vertex_ru.s = (x + 1) * self.edge_x_length
                vertex_ru.t = y * self.edge_x_length
                vertex_rb = self.vertices[rb]
                vertex_rb.s = (x + 1) * self.edge_x_length
                vertex_rb.t = (y + 1) * self.edge_x_length
                self.faces.append(Face((vertex_lu, vertex_rb, vertex_lb)))
                self.faces.append(Face((vertex_rb, vertex_lu, vertex_ru)))

    def get_plain(self, x, z):
        """
        Returns plane that contains point (x, z)
        """
        poi = Vector3(x, 0, z)
        planes = [plane for plane in self.planes
                  if contains_point(poi, plane[0][0], plane[0][1],
                                    plane[0][2])]
        if len(planes) > 1:
            LOGGER.warning("Got multiple plains for point %s: %s", poi, planes)
        elif not planes:
            LOGGER.error("Didn't find any plane for point %s", poi)
            raise PlaneNotFound(poi)
        return planes[0][1]

    def _download_map(self, rect, server):
        """
        Downloads the map given by rect and result stores

        @param rect: tuple (left, bottom, right, top)
        @param server: Google Elevation API server
        """
        ggapi = GoogleElevationAPI(server)
        # rect indexes:
        # 0 - left
        # 1 - bottom
        # 2 - right
        # 3 - top
        # Creates 2D array of required points
        x_axis = list(numpy.linspace(rect[0], rect[2], self.density))
        step = x_axis[1] - x_axis[0]
        x_axis.insert(0, x_axis[0] - step)
        x_axis.append(x_axis[-1] + step)
        y_axis = list(numpy.linspace(rect[1], rect[3], self.density))
        step = y_axis[1] - y_axis[0]
        y_axis.insert(0, y_axis[0] - step)
        y_axis.append(y_axis[-1] + step)
        LOGGER.debug("Heightmap x coordinates: %s"
                     "          y coordinates: %s", x_axis, y_axis)
        for lat in y_axis:
            for lon in x_axis:
                ggapi.insert_location(lat, lon)
            self._heights.append(ggapi.get_heights())
            for point in self._heights[-1]:
                lat = point['location']['lat']
                lon = point['location']['lng']
                x, z = self.projector.get_point((lat, lon), in_degrees=True)
                point['x'] = x
                point['y'] = point['elevation']
                point['z'] = z
                self.vertices.append(Vertex(x, point['elevation'], z))

    def __getitem__(self, key):
        """
        """
        return self._heights[key]

    def _create_edge_lines(self):
        """
        Creates equations for all edges in the heightmap
        """
        axis_cnt = len(self._heights[0])
        for i, x_p in enumerate(self._heights[0]):
            next = i + 1
            x_last = self._heights[-1][i]
            if abs(x_last['x'] - x_p['x']) > PRECISION:
                LOGGER.warn("Got two points with different x-coord: %f and %f",
                            x_last['x'], x_p['x'])
            first = Vector2(x_p['x'], x_p['z'])
            last = Vector2(x_last['x'], x_last['z'])
            edge = Line(first, last)
            self.edges.append((first, last, edge))
            if next < axis_cnt:
                x_last = self._heights[-next][-1]
                last = Vector2(x_last['x'], x_last['z'])
                edge = Line(first, last)
                self.edges.append((first, last, edge))

        for next, y_p in enumerate(self._heights, 1):
            first = Vector2(y_p[0]['x'], y_p[0]['z'])
            last = Vector2(y_p[-1]['x'], y_p[-1]['z'])
            if abs(y_p[-1]['z'] - y_p[0]['z']) > PRECISION:
                LOGGER.warn("Got two points with different z-coord: %f and %f",
                            y_p[-1]['z'], y_p[0]['z'])
            edge = Line(first, last)
            self.edges.append((first, last, edge))
            if 1 < next < axis_cnt:
                x_last = self._heights[-1][-next]
                last = Vector2(x_last['x'], x_last['z'])
                edge = Line(first, last)
                self.edges.append((first, last, edge))

    def get_intersections(self, line, boundries):
        """
        Computes intersections with edges of triangles in the heightmap

        @param line: Line that intersects the heightmap
        @param boundries: Tuple of two Vector2 boundring the edge
        """
        intersections = list()
        min_x = min(boundries[0].x, boundries[1].x)
        max_x = max(boundries[0].x, boundries[1].x)
        min_y = min(boundries[0].y, boundries[1].y)
        max_y = max(boundries[0].y, boundries[1].y)
        lb = Vector2(min_x, min_y)
        ru = Vector2(max_x, max_y)
        for edge in self.edges:
            intersection = line.get_intersect(edge[2])
            if fits_interval(intersection, lb, ru):
                intersections.append(intersection)

        return intersections

    def get_axis_cut(self, axis, min_coord, max_coord):
        """
        Returns list of x or y coordinates on axis, that fits min/max_coord
        interval on given axis.
        @param axis: String which axis (x, y, or xy) we want
        @param min_coord: minimal x or y coordinate
        @param max_coord: maximal x or y coordinate
        """
        if "x" in axis:
            points = [point['x'] for point in self._heights[0]
                      if min_coord < point['x'] < max_coord]
        elif axis == "y":
            points = [point[0]['z'] for point in self._heights
                      if min_coord < point[0]['z'] < max_coord]
        else:
            points = list()
        return points
