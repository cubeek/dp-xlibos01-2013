"""
This module provides installer interface for various platforms
"""

import getpass
import shlex
import subprocess

class InstallCommandException(Exception):
    pass


class Installer(object):
    """
    Base class for installers 
    """
    packages = ["lxml", ]
    posix = False
    PACKAGE_VERSIONS = {
        "lxml": "lxml-3.0.1.tgz",
        "libxml2": "libxml2-2.7.8.tgz",
        "libxslt": "libxslt-1.1.28.tgz"
    }
    PACKAGE_URLS = {
        "lxml": "http://lxml.de/files/%s" % PACKAGE_VERSIONS['lxml'],
        "libxslt": "ftp://xmlsoft.org/libxml2/%s" % PACKAGE_VERSIONS['libxml2'],
        "libxml2": "ftp://xmlsoft.org/libxml2/%s" % PACKAGE_VERSIONS['libxml2']
    }
    
    def __init__(self):
        """
        Constructor
        """
        pass


    def __run_command(command, input=None):
        """
        Runs command using command line
        @param command: Command we want to run in command line
        """
        cmd = shlex.split(command, posix=self.posix)
        pipe = subprocess.Popen(command)
        stdout, stderr = pipe.communicate(input)
    
        return pipe.returncode, stdout, stderr


    def install_package(self, package):
        """
        Installs given package
        @param pacakge: Package we want to install
        """
        if package == "lxml":
            self.__install_lxml()


    def __install_lxml(self):
        commands = [
            "pushd /tmp",
            "wget %s" % self.PACKAGE_URLS['lxml'],
            "tar -xzvf %s" % self.PACKAGE_VERSIONS['lxml'],
            "cd %s" % self.PACKAGE_VERSIONS['lxml'],
            "wget %s" % self.PACKAGE_URLS['libxml2'],
            "wget %s" % self.PACKAGE_URLS['libxslt'],
            "python setup.py build --static-deps --libxml2-version=2.7.3" \
            " --libxslt-version=1.1.24 ",
            "sudo python setup.py install"
        ]
        
        for command in commands:
            self.__run_command(command)


    def run(self):
        for p in self.packages:
           install_package(p)
        

class UbuntuInstaller(Installer):
    """
    Installer for Ubuntu platform
    """
    __INSTALL_CMD = 'sudo apt-get -y install %s'
    
    def __init__(self):
        """
        Constructor
        """
        super(UbuntuInstaller, self).__init__()
        
    def install_package(self, package):
        """
        Installs package using apt-get
        """
        print "Installing package, sudo command issued"
        password = getpass.getpass("[sudo] password for %s:" % getpass.getuser())
        install_command = self.__INSTALL_CMD % package
        rc, out, err = self.__run_command(install_command, password)
        del password
        if rc != 0:
            raise InstallCommandException("Installing command %s failed: rc=%d"
                                          "\nout=%s\nerr=%s" % (rc, out, err))
        
        
    def run(self):
        """
        Starts installation of packages
        """
        for package in self.packages:
            self.install_package(package)
