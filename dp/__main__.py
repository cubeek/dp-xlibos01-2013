#!/usr/bin/python

"""
This is main for running dp as a package on directory level
"""

def __fix_path_for_linux():
    """
    Sets LD_LIBRARY_PATH environment variable on linux boxes
    """
    import os
    import sys
    if sys.platform == "linux2":
        ld_library_path = os.environ.get('LD_LIBRARY_PATH', "")
        ld_library_path += ":%s" % os.path.join(
            os.path.dirname(__file__), "view/libs")
        os.environ['LD_LIBRARY_PATH'] = ld_library_path


if __name__ == "__main__":
    __fix_path_for_linux()
    from dp.main import main
    main()
