"""
Module for communication with OSM API and obtaining requests
"""
from concurrent.futures import ThreadPoolExecutor
import cPickle
import httplib
import gzip
import logging
import os
from Queue import Queue
import re
from threading import RLock
from time import sleep
import uuid

from dp import common
from dp.computations.geometrfunc import get_center

LOGGER = logging.getLogger("dpLogger")


class OsmException(Exception):
    """
    Basic exception for osm parser, can store http code
    """
    def __init__(self, message, code=200):
        super(OsmException, self).__init__()
        self.message = message
        self.code = code


class RectNotFound(Exception):
    pass


def _split_rectangle(rectangle):
    """
    Splits specified rectangle to 4 other rectangles and returns as a tuple
       L - left
       R - right
       T - top
       B - bottom

    @param rectangle: Rectangle to split
    @return: Tuple of 4 new rectangles
    """

    left, bottom, right, top = rectangle
    try:
        middle_horizontal = get_center(left, right)
        middle_vertical = get_center(top, bottom)
    except ValueError as value_e:
        LOGGER.error("Error while converting string to float: %s",
                     value_e)

    left_top = (left, middle_vertical, middle_horizontal, top)
    right_top = (middle_horizontal, middle_vertical, right, top)
    left_bottom = (left, bottom, middle_horizontal, middle_vertical)
    right_bottom = (middle_horizontal, bottom, right, middle_vertical)

    return left_top, right_top, left_bottom, right_bottom


class Cache(dict):
    """
    Object loading and storing files to the cache
    """
    _cache_dir = os.path.join(os.path.dirname(__file__), ".cache")
    _cache_file = os.path.join(_cache_dir, "cache")

    def __init__(self):
        """
        Creates cache directory
        """

        self._cache = None
        self._rlock = RLock()
        if not os.path.exists(self._cache_dir):
            os.mkdir(self._cache_dir)
        self._load_cache()

    def _load_cache(self):
        """
        Loads cache from cache_file or creates new one
        """
        if os.path.exists(self._cache_file):
            with gzip.open(self._cache_file, 'r') as cache_handler:
                self._cache = cPickle.load(cache_handler)
        else:
            self._cache = dict()

    def _save_cache(self):
        """
        Stores cache content into file
        """
        LOGGER.debug("Saving cache to %s:", self._cache_file)
        with gzip.open(self._cache_file, 'w') as cache_handler:
            cPickle.dump(self._cache, cache_handler, protocol=2)

    def __del__(self):
        """
        Destructor - saves cache
        """
        self._save_cache()

    def __setitem__(self, rect, content):
        """
        Adds content to cache
        """
        if not isinstance(content, list):
            filename = os.path.join(self._cache_dir, "%s.gz" % uuid.uuid4())
            if rect in self._cache:
                LOGGER.warn("Rectange %s is already in cache", rect)
                filename = self._cache[rect]
            with gzip.open(filename, 'w') as file_h:
                file_h.write(content)
            content = filename
            LOGGER.debug("Rectangle %s saved to %s", rect, filename)
        with self._rlock:
            self._cache[rect] = content

    def __getitem__(self, rect):
        """
        Gets the rect from cache. If rect is not in cache, exception
        RectNotFound is raised
        """
        if rect not in self._cache:
            raise RectNotFound("Rectangle %s not found in cache" % (rect,))
        if isinstance(self._cache[rect], list):
            return self._cache[rect]
        with gzip.open(self._cache[rect], 'r') as cache_file:
            LOGGER.debug("Rectanlge %s obtained from file %s", rect,
                         self._cache[rect])
            return cache_file.read()


class OsmApi(object):
    """
    API object for communication
    """

    def __init__(self, server):
        """
        Constructor, explicit server can be specified.

        @param server: HTTP server for OSM api
        """
        self.server = server
        # Cache for storing rectangles to download
        self._cache = Cache()
        # Connection object to API
        self.conn = None
        # Particular rectangles to download
        self.rect_queue = Queue()
        # Final XML document in string
        self.__final_xml = ""
        # Rectangle desired to download
        self.rect = None
        # url for downloading bounding box
        self.url = "/api/0.6/map?bbox=%s,%s,%s,%s"
        # Current running downloads
        self.running = 0
        # Lock for changing running attribute
        self.rlock = RLock()
        LOGGER.debug("OsmApi object created")

    def __enter__(self):
        """
        Entry point for 'with', connects to rest-api and returns itself

        @return: self
        """
        self.connect()
        return self

    def __exit__(self, type_, value, traceback):
        """
        Closes connection to HTTP restapi when object runs out of context

        @return: None
        """
        self.close()

    @property
    def final_xml(self):
        """
        Returns final XML document
        """
        return self.__final_xml

    def close(self):
        """
        Closes HTTP connection

        @return: None
        """
        self.conn.close()
        LOGGER.debug("Connection to %s has been closed" % self.server)

    def connect(self):
        """
        Creates a new connection to rest api and stores it
        """
        self.conn = self._connect()

    def _connect(self):
        """
        Connects to restapi
        """
        try:
            LOGGER.debug("Connecting to %s" % self.server)
            connection = httplib.HTTPConnection(self.server)
            LOGGER.debug("Connected to %s" % connection.host)
        except httplib.HTTPException as http_exception:
            LOGGER.error("Can't connect to %s: %s", self.server,
                         http_exception)
        return connection

    def get_capabilities(self):
        """
        Returns capabilities of rest-api server

        @return: String containing XML with capabilities
        """
        if self.conn is None:
            LOGGER.warning("Attempting to get capablities but not connected")
        try:
            LOGGER.debug("Getting capabilities from %s" % self.server)
            self.conn.request("GET", "/api/capabilities")
            response = self.conn.getresponse()
            if response.status != 200:
                LOGGER.error("Got response status %d" % response.status)
            else:
                data = response.read()
                return data

        except httplib.HTTPException as http_ex:
            LOGGER.error("Can't get API capabilities from server %s: %s" %
                         (self.server, http_ex))

    def _process_downloaded(self, part_map):
        """
        Appends particular downloaded XML document to final XML document

        @params part_map: Tuple of (rectangle, XML document descirbing map)
        """
        rect, part_map = part_map
        self._cache[rect] = part_map

        # We won't need first two lines
        data = part_map.split('\n', 3)

        if not self.__final_xml:
            # First XML downloaded
            # add first two lines containing the header
            self.__final_xml += "\n".join(data[:2])
            if self.rect is not None:
                self.__final_xml += " <bounds minlat=\"%s\" minlon=\"%s\" " \
                                    "maxlat=\"%s\" maxlon=\"%s\"/>\n" % \
                                    (self.rect[1], self.rect[0], self.rect[3],
                                     self.rect[2])
            else:
                LOGGER.warning("Rect doesn't exist, this should never happen")
                self.__final_xml += "\n" + data[2] + "\n"
                rerect = re.compile(r"minlat=\"(?P<minlat>[^\"]*)\".*minlon="
                                    "\"(?P<minlon>[^\"]*)\".*maxlat=\""
                                    "(?P<maxlat>[^\"]*)\".*maxlon="
                                    "\"(?P<maxlon>[^\"]*)\"")
                match = rerect.search("".join(data[:3]))
                if match:
                    self.rect = (float(match.group('minlat')),
                                 float(match.group('minlon')),
                                 float(match.group('maxlat')),
                                 float(match.group('maxlon')))
                else:
                    LOGGER.error("Cannot find bounding box")

        # append to final XML
        self.__final_xml += data[3][:-7]
        LOGGER.debug("Added another part of the map to final XML")

    def finish_downloaded(self):
        """
        Appends the tail and returns final merged XML
        """
        self.__final_xml += "</osm>\n"
        return self.__final_xml

    def get_xml_map(self, rect):
        """
        Downloads rectangle map from API specified by rect

        @param rect: Tuple with left, bottom, right and top coordinates
        @return root DOM node if something was downloaded. None otherwise
        """
        self.rect = rect
        self.rect_queue.put(rect)
        results = list()
        LOGGER.info("Downloading rectangle %s", self.rect)
        with ThreadPoolExecutor(max_workers=common.MAX_DOWNLOAD_WORKERS) \
                as executor:
            while self.running != 0 or not self.rect_queue.empty():
                LOGGER.debug("Rectangles in queue: %d\n"
                             "Downloads in progress: %d",
                             self.rect_queue.qsize(), self.running)
                if not self.rect_queue.empty():
                    rect = self.rect_queue.get()
                    LOGGER.debug("Got %s from queue, adding to executor", rect)
                    result = executor.submit(self._get_map, rect)
                    results.append(result)
                else:
                    running = self.running
                    LOGGER.debug("There are %d downloads in progress",
                                 self.running)
                    while self.running != 0 and running == self.running:
                        LOGGER.debug("Waiting for one download to finish, "
                                     "still have %d", self.running)
                        sleep(common.THREAD_WAIT)
                sleep(0.1)
            LOGGER.debug("Queue is empty: %d, all rectangles downloaded",
                         self.rect_queue.qsize())
        for result in results:
            if result.exception():
                LOGGER.error("Exception while downloading particular "
                             "rectangle: %s", result.exception())
            elif result.result() is not None:
                self._process_downloaded(result.result())
        return self.finish_downloaded()

    def _get_map(self, rect, attempt=3):
        """
        Downloads XML which describes map using HTTP via OSM REST-api

        @param rect: Tuple of left, bottom, right, top coordinates of rectangle
                     to download
        @param attempt: number for recursive attempt of download in case of
                        BadStatusLine exception
        @return: tuple rectangle and XML describing given rectangle
        """
        self._increase_running_sessions()
        connection = None
        try:
            try:
                data = self._cache[rect]
                if isinstance(data, list):
                    for srect in data:
                        self.rect_queue.put(srect)
                    return
                LOGGER.info("Obtained rectanlge %s from cache", rect)
                return rect, data
            except RectNotFound as rect_err:
                LOGGER.debug(rect_err)
            url = self.url % rect
            LOGGER.debug("Downloading map, url: %s" % url)
            connection = self._connect()
            try:
                connection.request("GET", url)
                response = connection.getresponse()
            except httplib.BadStatusLine:
                LOGGER.warning("BadStatusLine - attempting to download "
                               "rectangle again. Attempts left: %d" % attempt)
                if attempt > 0:
                    # Try to download again
                    return self._get_map(rect, attempt - 1)
                else:
                    raise OsmException("We ran out of attempts, "
                                       "cannot download rectangle %s" % rect)

            if response.status == 200:
                data = response.read()
                LOGGER.debug("Rectangle %s downloaded successfully, %d bytes "
                             "read", rect, len(data))
                return rect, data
            elif response.status == 400:
                #Too many nodes, split
                LOGGER.debug("Rectangle %s download failed: Any of the "
                             "node/way/relation limits are crossed", rect)
                self._cache[rect] = list()
                for srect in _split_rectangle(rect):
                    self._cache[rect].append(srect)
                    self.rect_queue.put(srect)
            elif response.status == 509:
                #Download limit is gone
                LOGGER.error("Rectangle %s download failed: You have probably "
                             "downloaded too much data.", rect)
        except:
            LOGGER.error("Toz", exc_info=True)
        finally:
            if connection is not None:
                connection.close()
            self._decrease_running_sessions()

    def _increase_running_sessions(self):
        """
        Thread-safe increasing counter of currently processed downloads
        """
        with self.rlock:
            self.running += 1
            LOGGER.debug("Increased running sessions to %d", self.running)

    def _decrease_running_sessions(self):
        """
        Thread-safe increasing counter of currently processed downloads
        """
        with self.rlock:
            self.running -= 1
            LOGGER.debug("Decreased running sessions to %d", self.running)
