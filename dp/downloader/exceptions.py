"""
Module containing exceptions for downloaders
"""


# Google API exceptions
class GoogleAPIExceptions(Exception):
    pass


class URLLongException(GoogleAPIExceptions):
    pass


class OverLimitException(GoogleAPIExceptions):
    pass


class RequestDeniedException(GoogleAPIExceptions):
    pass


class InvalidRequestException(GoogleAPIExceptions):
    pass


class UnknownError(GoogleAPIExceptions):
    pass


class AltitudeNotFound(GoogleAPIExceptions):
    pass


# OSM API exceptions
