"""
Module for communication with google API, mostly downloading height map
All altitudes are cached and saved to file, in case of querying one coordinate
multiple times, one from cache is returned
"""

import copy
import cPickle
import httplib
import json
import logging
import os
import time

from dp import common
from dp.downloader import exceptions

ALT_CACHE_FILE = os.path.join(os.path.dirname(__file__), ".altitudes")
LOGGER = logging.getLogger("dpLogger")


class GoogleElevationAPI(object):
    """
    Class encapsulating Google API methods and caching once downloaded data
    """

    def __init__(self, server, alt_cache_file=ALT_CACHE_FILE):
        """
        Initialize Google object with default URL
        @param alt_cache_file: Path to file containing cache for elevations
        """
        # Cache for altitudes
        self._alt_cache = None
        # Cache for urls to query
        self._urls = []
        # Updated number of urls in the list above
        self._urls_cnt = 0
        # Current locations in the url string for particular query
        self._loc_cnt = common.GAPI_LOCS+1
        # Result of queried locations
        self._results = list()
        # Server for google API
        self._server = server
        # Filename for caching altitudes
        self._cache_file_path = alt_cache_file

        self._load_altitudes()
        LOGGER.info("GoogleElevationAPI initialized")

    def __del__(self):
        """
        Destructor of this class, just save cached data
        """
        self._save_altitudes()

    def _load_altitudes(self):
        """
        Loads cached altitudes from the file cache_file and content
        stores to the self._alt_cache
        """
        if os.path.exists(self._cache_file_path):
            with open(self._cache_file_path, 'r') as conf_file:
                self._alt_cache = cPickle.load(conf_file)
                LOGGER.info("Loaded %d altitudes from %s",
                            len(self._alt_cache), self._cache_file_path)
#                self._round_cache()
        else:
            LOGGER.info("No cached altitudes for now...")
            self._alt_cache = dict()

    def _round_cache(self):
        """
        Round values obtained in from cache
        """
        for key, val in self._alt_cache.iteritems():
            new_key = (round(key[0], common.ROUND_VAL), round(key[1],
                                                        common.ROUND_VAL))
            del self._alt_cache[key]
            self._alt_cache[new_key] = val

    def _save_altitudes(self):
        """
        Saves cached altitudes to the file _cache_file_path
        """
        LOGGER.info("Saving %d altitudes to %s", len(self._alt_cache),
                    self._cache_file_path)
        with open(self._cache_file_path, 'w') as cache_file:
            cPickle.dump(self._alt_cache, cache_file)

    def _get_alt_from_cache(self, latitude, longitude):
        """
        Gets known altitude from cache, AltitudeNotFound exception
        is raised of altitude not found in cache

        @param latitude: Latitude
        @param longitude: Longitude
        @return: Altitude
        """
#        latitude_rnd = round(float(latitude), common.ROUND_VAL)
#        longitude_rnd = round(float(longitude), common.ROUND_VAL)
        latitude = str(latitude)
        longitude = str(longitude)

        point = self._alt_cache.get((latitude, longitude), None)
        if point is None:
            raise exceptions.AltitudeNotFound("Altitude on position %s, %s"
                                              "is not cached" %
                                              (latitude, longitude))
        return copy.deepcopy(point)

    def _insert_location(self, latitude, longitude):
        """
        Appends new location to query URL

        @param latitude: Latitude
        @param longitude: Longitude
        """

        if self._loc_cnt > common.GAPI_LOCS:
            self._urls.append("/maps/api/elevation/json?locations=")
            self._urls_cnt = len(self._urls)-1
            self._loc_cnt = 0

        lat_lon = "%s,%s" % (latitude, longitude)

        self._urls[self._urls_cnt] += lat_lon \
            if self._loc_cnt == 0 else "|%s" % lat_lon
        self._loc_cnt += 1

    def insert_location(self, latitude, longitude):
        """
        Appends new location to query URL if it's not cached

        @param latitude: Latitude
        @param longitude: Longitude
        """
        try:
            result = self._get_alt_from_cache(latitude, longitude)
            self._results.append(result)
        except exceptions.AltitudeNotFound:
            self._insert_location(latitude, longitude)

    def _add_to_results(self, results):
        """
        Adds to private results obtained results from API
        Also updates cache

        @params results: List of dictionaries with elevations
        """
        for point in results:
            latitude = str(point['location']['lat'])
            longitude = str(point['location']['lng'])
            if not (latitude, longitude) in self._alt_cache:
                self._alt_cache[(latitude, longitude)] = copy.deepcopy(point)
            self._results.append(point)

    def get_heights(self):
        """
        Gets the heights from Google Maps based on positions specified in
        this class

        @return: list of dictionaries with coordinates and elevation
        """
        results = []
        for url in self._urls:
            results.extend(self._get_response(url))
        LOGGER.debug("Downloaded: %d\tCached: %d" % (len(results),
                     len(self._alt_cache)))
        self._add_to_results(results)
        self._urls = list()
        self._urls_cnt = 0
        self._loc_cnt = common.GAPI_LOCS + 1
        ret_list = self._results
        ret_list.sort(key=lambda p: (p['location']['lng'],
                                     p['location']['lat']))
        self._results = list()
        LOGGER.debug("Height map: %s", ret_list)
        return ret_list

    def _get_response(self, url, attempt=3):
        """
        Queries google API for height map

        @param url: url containing bounding box
        @return: List of dictionaries
                 containing locations and elevations otherwise.
        """
        if url[-1] == "=":
            return dict()
        if "&sensor=false" not in url:
            url += "&sensor=false"

        try:
            connection = httplib.HTTPConnection(self._server)
            LOGGER.debug("Getting %s" % url)
            connection.request("GET", url)

            res = connection.getresponse()
            if res.status == 200:
                data = json.load(res)
            elif res.status == 414:
                raise exceptions.URLLongException("Requested URL is too "
                                                  "long: %d" % len(url))
            else:
                LOGGER.error("Got answer %d" % res.status)
        finally:
            connection.close()

        if data["status"] == "OK":
            LOGGER.info("Obtained height map successfully")
            return data["results"]
        elif data["status"] == "INVALID_REQUEST":
            raise exceptions.InvalidRequestException("Invalid request %s" %
                                                     url)
        elif data["status"] == "OVER_QUERY_LIMIT":
            if attempt > 0:
                LOGGER.warning("Query limit exceeded, trying again in 5 "
                               "seconds")
                time.sleep(5)
                self._get_response(url, attempt-1)
            raise exceptions.OverLimitException("You are over query limit,"
                                                "try tomorrow")
        elif data["status"] == "REQUEST_DENIED":
            raise exceptions.RequestDeniedException("Google API request "
                                                    "denied, cannot get height"
                                                    " map")
        elif data["status"] == "UNKNOWN_ERROR":
            raise exceptions.UnknownError("Unknown error has occured")
        else:
            raise exceptions.UnknownError("This will never happen - "
                                          "wrong status %s" % data["status"])


if __name__ == "__main__":
    locations = [(21.1, 22.3), (25.8, 26.2)]
    with GoogleElevationAPI() as google_api:
        for lat, lon in locations:
            google_api.insert_location(lat, lon)
        print google_api.get_heights()
