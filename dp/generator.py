"""
Building generator
"""
import logging
from math import ceil, sqrt
from matplotlib.delaunay import delaunay
from numpy import random, array
import os
import Polygon as poly
from random import uniform
import uuid

from dp.config import config_parser
from dp.classifier.classifier import BuildingClasses
from dp.computations.ageometr import Vector3, Line3, Vector2, get_distance, \
    Line
from dp.computations.geometrfunc import get_angle, is_non_left_turn
from dp.elements import Block, Face, GlassMesh, Mesh, Polygon, Vertex, scale,\
    sample, generate_polygon_mesh, get_bounding_box_center, get_longest_side,\
    order_polygon, shift_polygon_to_cener, get_polygon_sides,\
    get_shortest_side_length


LOGGER = logging.getLogger("dpLogger")
ONE_THIRD = 1.0 / 3.0


class UnknownAxis(Exception):
    pass


class FloorException(Exception):
    pass


class WallException(Exception):
    pass


def pair_room_with_wall(rooms, wall, orig_wall):
    """
    Pairs room with the new wall and removes old wall

    @param room: Room that contains original wall that was splitted
    @param wall: New wall created by split
    @param orig_wall: Wall that was splitted
    """
    for room in rooms:
        wall.add_room(room)
        if wall not in room:
            room.append_wall(wall)
        if orig_wall in room:
            room.remove(orig_wall)


class Roof(object):
    """
    Class that encapsulates generating meshes for roofs
    """

    @classmethod
    def get_skew_roof(cls, building, height, scale_fac):
        """

        @param building: Building object that needs roof
        @param height: Height where roof should start
        """
        def belongs_to_roof(polygon, v1, v2, v3):
            """
            Decides whether triangle is a part of the roof

            @param v1: First vertex of the triangle
            @param v2: Second vertex of the triangle
            @param v3: Third vertex of the triangle
            """
            xs = [v1.x, v2.x, v3.x]
            xs.sort()
            v1x, v2x, v3x = xs
            x_middle = (v1x + v2x + v2x + v3x) / 4.0

            zs = [v1.z, v2.z, v3.z]
            zs.sort()
            v1z, v2z, v3z = zs
            z_middle = (v1z + v2z + v2z + v3z) / 4.0
            return polygon.isInside(x_middle, z_middle)

        platform = scale(building.polygon, scale_fac)
        difference = platform - building.polygon
        bottom_roof_mesh = generate_polygon_mesh(difference, False)
        for vertex in bottom_roof_mesh.vertices:
            vertex.y = height
        bb_center = get_bounding_box_center(platform)
        shift_polygon_to_cener(platform, bb_center)
        xs, ys = sample(platform)
        LOGGER.info("Generating skew roof with %d samples", len(xs))

        xs = array(xs)
        ys = array(ys)
        circ, edges, tri, neigh = delaunay(xs, ys)
        vertices = list()
        for x, y in zip(xs, ys):
            v_h = min([get_distance(first, second, Vector2(x, y))
                      for first, second in get_polygon_sides(platform)])
            vertex = Vertex(x, v_h + height, y)
            vertices.append(vertex)

        roof_mesh = Mesh()
        roof_mesh.texture = Generator.textures.get_index('roof')
        bottom_roof_mesh.texture = Generator.textures.get_index('wood')
        for triangle in tri:
            vertex1 = vertices[triangle[0]]
            vertex1.s = vertex1.x
            vertex1.t = vertex1.z
            vertex2 = vertices[triangle[1]]
            vertex2.s = vertex2.x
            vertex2.t = vertex2.z
            vertex3 = vertices[triangle[2]]
            vertex3.s = vertex3.x
            vertex3.t = vertex3.z
            if belongs_to_roof(platform, vertex1, vertex2, vertex3):
                roof_mesh.add_face(Face((vertex1, vertex2, vertex3)))
        building.meshes.append(roof_mesh)
        building.meshes.append(bottom_roof_mesh)

    @classmethod
    def get_flat_roof(cls, building, height, texture):
        """
        Generates flat roof with borders
        """
        surface = generate_polygon_mesh(building.polygon)
        surface.texture = Generator.textures.get_index('flat_roof')
        for vertex in surface.vertices:
            vertex.y = height + 0.05
            vertex.s = vertex.x
            vertex.t = vertex.z
        building.meshes.append(surface)

        length = 0
        for start, end in zip(building.polygon[0], building.polygon[0][1:] +
                              [building.polygon[0][0]]):
            start = Vector3(start[0], height, start[1])
            end = Vector3(end[0], height, end[1])
            front, back = get_block_vertices(start, end, 0.2, 0.3)

            front[0].s = length
            front[0].t = 0.3
            front[1].s = length
            front[1].t = 0

            back[3].s = length
            back[3].t = 0.3
            back[2].s = length
            back[2].t = 0

            length += (end - start).length

            front[2].s = length
            front[2].t = 0
            front[3].s = length
            front[3].t = 0.3

            back[1].s = length
            back[1].t = 0
            back[0].s = length
            back[0].t = 0.3

            part = Block()
            part.quad_face(front[0], front[1], front[2], front[3])
            part.quad_face(back[3], back[2], back[1], back[0])
            part.texture = texture
            part.outer_side_face(front, back)
            building.meshes.append(part)


def get_block_vertices(start, end, thickness, height, front_offset=0):
    """
    Gets eight vertices of wall block

    @param start: Start of wall block (Vector3 to middle of wall)
    @param end: End of wall block
    @param thickness: Thickness of the wall
    @param height: Height of the wall
    @return: Tuple of two tuples
                First tuple are four vertices of front wall
                Second tuple are four vertices of back wall
    """
    direct = (end - start)
    normal = Vector3(-direct.z, direct.y, direct.x).normalize()
    thick = normal * thickness
    front_offset = normal * front_offset
    front_start = start + front_offset
    front_end = end + front_offset
    back_start = front_start + thick
    back_end = front_end + thick
    f_lu = Vertex(front_start.x, front_start.y + height,
                  front_start.z)
    f_lb = Vertex(front_start.x, front_start.y, front_start.z)
    f_rb = Vertex(front_end.x, front_end.y, front_end.z)
    f_ru = Vertex(front_end.x, front_end.y + height, front_end.z)

    b_lu = Vertex(back_start.x, back_start.y + height,
                  back_start.z)
    b_lb = Vertex(back_start.x, back_start.y, back_start.z)
    b_rb = Vertex(back_end.x, back_end.y, back_end.z)
    b_ru = Vertex(back_end.x, back_end.y + height, back_end.z)

    return ((f_lu, f_lb, f_rb, f_ru), (b_lu, b_lb, b_rb, b_ru))


class Floor(list):
    """
    One building flooe
    """
    def __del__(self):
        print "Floor destructed"

    def __init__(self, rooms, base_height, building,
                 entrance_position, is_base, outside_texture):
        """
        Platform of building

        @param rooms: List of polygons (Polygon.Polygon) type
        @param height: Height of the floor in y-coordination
        @param b_type: Type of building
        """
        super(Floor, self).__init__()
        self.base_height = base_height
        self.walls = list()
        self.building = building
        self.is_base = is_base
        self.specs = config_parser.get_building_specs(building.building_class)
        self.entrance = Wall(entrance_position[0], entrance_position[1], self)
        LOGGER.debug("Entrance at %s", self.entrance)

        self.textures = {
            'interior_texture': Generator.textures.get_index('interior'),
            'outside_texture': outside_texture,
            'door_out_texture': Generator.textures.get_index(
                self.specs['out_door']),
            'door_in_texture': Generator.textures.get_index('in_door'),
        }

        self.entrance_candidates = list()
        for room in rooms:
            self.append(room)
        # Set rooms neighbours and walls count according rooms
        self.set_rooms_neighbours()
        if is_base:
            self._choose_entrance()
        self.set_doors()

    @property
    def polygon(self):
        """
        Gets polygon of building
        """
        return self.building.polygon

    def order_entrance_candidates(self):
        """
        Orders outer walls according theirs entrance fit probability
        """
        ordered_walls = [wall for wall in self.walls if len(wall.rooms) == 1]
        ordered_walls.sort(key=lambda x: x.length)
        for wall in ordered_walls:
            if wall.can_have_entrance():
                self.entrance_candidates.append(wall)

    def _choose_entrance(self):
        """
        Chooses one of the entrance from candidates
        """
        if not self.entrance_candidates:
            self.order_entrance_candidates()

        selected = self.entrance_candidates[0]
        for candidate in self.entrance_candidates:
            if candidate.rooms[0].platform.area() > \
                    selected.rooms[0].platform.area():
                selected = candidate
        self.entrance = selected
        selected.is_entrance = True
        LOGGER.debug("Entrance chosen at %s", selected)

    def set_doors(self):
        """
        Chooses walls that will be doors
        """
        def get_neighbouring_door(room1, room2):
            """
            Gets wall that divides room1 and room2 and this wall must be long
            enough to contain door.

            @param room1: Room that is neighbour of room2
            @param room2: Room that is neighbour of room1
            @return: Wall object that can have door
            """
            for wall in room1:
                if wall in room2 and wall.can_have_door():
                    return wall

        LOGGER.debug("Setting doors")
        if self.is_base:
            room = self.entrance.rooms[0]
        else:
            room = self[int(len(self) * random.random())]
        room.door_length = 0
        will_visit = room.neighbours[:]
        room_ids = [room.id for room in will_visit]
        while will_visit:
            LOGGER.debug("Will visit %d rooms" % len(will_visit))
            room = will_visit.pop(0)
            room_ids.pop(0)
            for neighbour in room.neighbours:
                if neighbour.door_length < 0 and neighbour.id not in room_ids:
                    n_wall = get_neighbouring_door(room, neighbour)
                    if n_wall is not None:
                        if not hasattr(neighbour, 'last') or not neighbour.last:
                            LOGGER.debug("Adding room to will_visit")
                            will_visit.append(neighbour)
                            room_ids.append(neighbour.id)
                        if hasattr(neighbour, 'last'):
                            neighbour.last = True
                        else:
                            setattr(neighbour, 'last', False)

            selected = None
            for neighbour in room.neighbours:
                if neighbour.door_length >= 0:
                    wall = get_neighbouring_door(room, neighbour)
                    if wall is None:
                        continue
                    if selected is None or \
                            neighbour.door_length < selected.door_length:
                        selected = neighbour
                    elif neighbour.door_length == selected.door_length:
                        selected = min(neighbour, selected, key=lambda x:
                                       x.platform.area())
            if selected is not None:
                selected_wall = get_neighbouring_door(room, selected)
                if selected_wall is not None:
                    selected_wall.has_door = True
                    room.door_length = selected.door_length + 1
                elif will_visit:
                    if hasattr(selected, 'visited'):
                        if not selected.visited:
                            selected.visited = True
                            will_visit.append(selected)
                            room_ids.append(selected.id)
                    else:
                        setattr(selected, 'visited', False)
                        will_visit.append(selected)
                        room_ids.append(selected.id)

    def append(self, room):
        """
        Appends room to this floor

        @param room: Room polygon
        """
        room = Room(self, room)
        super(Floor, self).append(room)

    def set_rooms_neighbours(self):
        """
        According walls rooms set walls to rooms and set rooms' neighbours
        """
        for wall in self.walls:
            wall.can_entrance()
            if len(wall.rooms) == 1:
                room = wall.rooms[0]
                if wall not in room:
                    room.append_wall(wall)
            elif len(wall.rooms) == 2:
                room1 = wall.rooms[0]
                room2 = wall.rooms[1]
                if wall not in room1:
                    room1.append_wall(wall)
                if wall not in room2:
                    room2.append_wall(wall)
                room1.add_neighbour(room2)
                room2.add_neighbour(room1)
            else:
                LOGGER.error("Wall cannot divide three rooms!!!")

    def process_wall(self, wall):
        """
        Checks that wall is in walls list. If it's not, it's added. If it's in
        the list, wall parameter is replaced by existing wall

        @param wall: Wall to add and split in case of overlapping with some
                     other wall
        @return: List of walls to be added for room
        """
        def add_uniq_to_list(walls, wall):
            """
            Adds wall to walls only if it's not there. If it's already there,
            rooms are refreshed

            @param walls: List of walls
            @param wall: Wall to be added
            """
            if wall in walls:
                index = walls.index(wall)
                same_wall = walls[index]
                for room in wall.rooms:
                    same_wall.add_room(room)
            else:
                walls.append(wall)

        new_walls = list()
        if wall in self.walls:
            room = wall.rooms[0]
            index = self.walls.index(wall)
            wall = self.walls[index]
            wall.add_room(room)
            return [wall]
        else:
            for other_wall in self.walls[:]:
                splitted = wall.split_walls(other_wall)
                if splitted:
                    self.walls.remove(other_wall)
                    for spl_wall in splitted:
                        add_uniq_to_list(new_walls, spl_wall)
        ret_list = new_walls if new_walls else [wall]

        for wall in ret_list:
            if wall in self.walls:
                index = self.walls.index(wall)
                existing = self.walls.pop(index)
                for room in existing.rooms:
                    wall.add_room(room)
                self.walls.insert(index, wall)
            else:
                self.walls.append(wall)

    def generate_meshes(self):
        """
        Generates doors and windows in the walls
        """
        meshes = list()
        meshes.extend(self.generate_floors())
        for wall in self.walls:
            wall.fix_wall_orientation()
        for wall in self.walls:
            wall_meshes = wall.generate_mesh()
            meshes.extend(wall_meshes)
        return meshes

    def generate_floors(self):
        """
        Generates floor from polygon
        """
        meshes = list()
        for room in self:
            meshes.append(room.generate_floor())
            meshes.append(room.generate_ceiling())
        return meshes


class Room(list):
    """
    Room in the buildings
    """
    def __del__(self):
        print "Room destrcuted"

    def __init__(self, floor, room):
        """
        C'tor

        @param floor: Parent floor object
        @param room: Polygon of the room
        """
        super(Room, self).__init__()
        self.id = uuid.uuid4()
        self.windows_cnt = 0
        self.doors_cnt = 0
        self.platform = room
        self.neighbours = list()
        self.wall_texture = floor.textures['interior_texture']
        self.door_texture = Generator.textures.get_index('in_door')
        prev = self.platform[0][0]
        self.floor = floor
        for point in self.platform[0][1:]:
            self.append(prev, point)
            prev = point
        self.append(self.platform[0][-1], self.platform[0][0])
        self.door_length = -1

    def __eq__(self, other):
        """
        Compares walls of this and other room
        """
        result = False
        for wall in self:
            for wall_other in other:
                result &= wall == wall_other

    def __str__(self):
        """
        Prints center of the room
        """
        center = self.platform.center()
        return "(%f, %f -- %d)" % (center[0], center[1], len(self))

    __repr__ = __str__

    def add_neighbour(self, room):
        """
        Adds new room as a neighbour if it's already not

        @param room: Rooms to be added
        """
        if room not in self.neighbours and room is not self:
            self.neighbours.append(room)

    def generate_floor(self):
        """
        Generates floor from polygon
        """
        floor_mesh = generate_polygon_mesh(self.platform)
        floor_mesh.texture = Generator.textures.get_index('floor')
        for vertex in floor_mesh.vertices:
            vertex.y = self.floor.base_height + 0.01
        return floor_mesh

    def generate_ceiling(self):
        """
        Generates ceiling of the room
        """
        ceil_mesh = generate_polygon_mesh(self.platform, False)
        ceil_mesh.texture = Generator.textures.get_index('ceiling')
        height = self.floor.base_height + \
            float(self.floor.specs['wall_height'])
        for vertex in ceil_mesh.vertices:
            vertex.y = height - 0.01
        return ceil_mesh

    def append_wall(self, wall):
        """
        Adds wall to room without processing

        @param wall: Wall to be added
        """
        super(Room, self).append(wall)

    def append(self, first, second):
        """
        Creates a wall and adds it to this room

        @param first: First point of the wall
        """
        v1 = Vector3(first[0], self.floor.base_height, first[1])
        v2 = Vector3(second[0], self.floor.base_height, second[1])
        try:
            wall = Wall(v1, v2, self.floor)
            wall.add_room(self)
            self.floor.process_wall(wall)
        except WallException as wall_e:
            LOGGER.warning(wall_e)


class Wall(object):
    """
    Wall
    """
    MIN_LENGTH = 0.01

    def __init__(self, first, second, floor):
        """
        C'tor

        @param start: Starting vector of first point
        @param end: Ending vector of last point
        @param floor: Floor that this wall belongs to
        """
        d_vec = second - first
        self.length = sqrt(d_vec.x * d_vec.x + d_vec.y * d_vec.y +
                           d_vec.z * d_vec.z)
        if self.length < self.MIN_LENGTH:
            raise WallException("Short wall at <%s -- %s>", first, second)
        self.rooms = list()
        self.floor = floor
        self.juncs = list()
        self.is_entrance = False
        self.is_neighbouring = False
        self.has_door = False
        self.start = Vector3(first.x, first.y, first.z)
        self.start_clipped = False
        self.start_walls = set()
        self.end_walls = set()
        self.end = Vector3(second.x, second.y, second.z)
        self.end_clipped = False
        self.line = Line3(first, second)
        self.min = (min(first.x, second.x), min(first.y, second.y),
                    min(first.z, second.z))
        self.max = (max(first.x, second.x), max(first.y, second.y),
                    max(first.z, second.z))
        self.height = float(floor.specs['wall_height'])
        self.thickness = float(floor.specs['wall_thickness'])
        self.check_neighbouring()

    def __eq__(self, other):
        """
        Compares this wall with other

        @param other:
        """
        dir1 = self.start == other.start and self.end == other.end
        dir2 = self.end == other.start and self.start == other.end

        return dir1 or dir2

    @property
    def middle(self):
        """
        Gets middle of the wall
        """
        return (self.start + self.end) / 2.0

    @property
    def direction(self):
        """
        Direction of the wall
        """
        return (self.end - self.start).normalize()

    def __str__(self):
        """
        Converts this object to string
        """
        return "%s -- %s" % (self.start, self.end)

    def __repr__(self):
        return "<%s - %s>" % (self.start, self.end)

    def _is_between(self, point):
        """
        Checks that point is between first and second

        @param point: Vector3 point to check
        """
        dir_x = self.min[0] <= point.x <= self.max[0]
        dir_y = self.min[1] <= point.y <= self.max[1]
        dir_z = self.min[2] <= point.z <= self.max[2]

        return dir_x and dir_y and dir_z

    def contains(self, item):
        """
        Decides whether point or wall is part of this wall

        @param item: Vector3 or Wall instance that might be part of this wall
        @return: True if item is part of this wall
        """
        if isinstance(item, Vector3):
            return self.line.has_point(item) and self._is_between(item)
        elif isinstance(item, Wall):
            return self.contains(item.start) and self.contains(item.end)

    def overlaps(self, start, end):
        """
        if other wall defined by start and end overlaps self, returns two or
        three new walls

        @param start: Start of wall in direction of this wall
        @param end: End of wall in direction of this wall
        """
        if self.start == start and self.end == end:
            return {self}
        if self.contains(start) and start != self.end:
            if self.start == start:
                points = [end, self.end]
            elif self.end == end:
                points = [start, end]
            else:
                points = [self.end, start, end]
            dists = [(point, (self.start - point).length)
                     for point in points]
            dists.append((self.start, 0))
            dists.sort(key=lambda x: x[1])
        else:
            return

        walls = set()
        first = dists[0][0]
        for second, _ in dists[1:]:
            try:
                new = Wall(first, second, self.floor)
                walls.add(new)
            except WallException as wall_e:
                LOGGER.warning(wall_e)
            first = second
        return walls

    def split_walls(self, other):
        """
        Computes overlapping walls of this and other

        @param other: Other wall
        @return: None or set of new walls
        """
        if self.direction == other.direction.invert():
            start = other.end
            end = other.start
            s_start = self.end
            s_end = self.start
        elif self.direction == other.direction:
            start = other.start
            end = other.end
            s_start = self.start
            s_end = self.end
        else:
            return

        walls = self.overlaps(start, end)
        if walls is None:
            walls = other.overlaps(s_start, s_end)
        # Sets rooms
        if walls is not None:
            for wall in walls:
                if self.contains(wall):
                    pair_room_with_wall(self.rooms, wall, self)
                if other.contains(wall):
                    pair_room_with_wall(other.rooms, wall, other)
        return walls

    def add_room(self, room):
        """
        Adds room that contains this wall

        @param room: Foreign room
        """
        if room not in self.rooms:
            self.rooms.append(room)

    def can_entrance(self):
        """
        Sets to wall if it can be an entrance
        """
        if self.floor.entrance.contains(self) and self.can_have_entrance():
            self.floor.entrance_candidates.append(self)

    def shares_corner(self, other):
        """
        Returns true if walls have same corner

        @param other: Other wall
        """
        start = self.start == other.start or self.start == other.end
        end = self.end == other.start or self.end == other.end

        if start:
            return self.start
        elif end:
            return self.end
        else:
            return None

    def fix_wall_orientation(self):
        """
        Sets oriantation of this wall so z coordinate of start is lower than z
        coordinate of end
        """
        if len(self.rooms) > 1 and self.start.z > self.end.z:
            self.start, self.end = self.end, self.start

    def fix_wall_overlapping(self):
        """
        Fixes the wall so it doesn't overlap with other wall
        """
        THICKNESS_FIX_RATIO = 1
        for other_wall in self.floor.walls:
            if self.conts(other_wall) or \
                    other_wall.length < self.thickness / 2.0:
                continue
            corner = self.shares_corner(other_wall)
            if corner is None:
                continue
            if len(other_wall.rooms) == 1:
                if corner == self.start and not self.start_clipped:
                    LOGGER.debug("Fixing wall %s", self)
                    self.start += self.direction * other_wall.thickness * \
                        THICKNESS_FIX_RATIO
                    self.start_clipped = True
                elif corner == self.end and not self.end_clipped:
                    LOGGER.debug("Fixing wall %s", self)
                    self.end -= self.direction * other_wall.thickness * \
                        THICKNESS_FIX_RATIO
                    self.end_clipped = True
            else:
                if not is_non_left_turn(self.start, corner, other_wall.end)\
                        and self.start != corner and other_wall.end != corner\
                        and not self.end_clipped:
                    LOGGER.debug("Fixing wall %s", self)
                    self.end -= self.direction * other_wall.thickness * \
                        THICKNESS_FIX_RATIO
                    self.end_clipped = True
                if is_non_left_turn(self.end, corner, other_wall.start)\
                        and self.end != corner and other_wall.start != corner \
                        and not self.start_clipped:
                    LOGGER.debug("Fixing wall %s", self)
                    self.start += self.direction * other_wall.thickness * \
                        THICKNESS_FIX_RATIO
                    self.start_clipped = True

    def conts(self, other):
        """
        Whether other wall heads opposite or same directino as self
        """
        return self.direction == other.direction or \
            self.direction == other.direction.invert()

    def generate_mesh(self):
        """
        Splits wall into segments and generates segments
        """
        # Is entrance to building
        if self.is_entrance:
            return self.generate_entrance()
        # Is outside wall
        elif len(self.rooms) == 1 and not self.is_neighbouring:
            return self.generate_windows()
        # Is rooms walk-through
        elif self.has_door:
            self.fix_wall_overlapping()
            return self.generate_door(self.rooms[0].door_texture)
        # Is wall between two rooms
        else:
            self.fix_wall_overlapping()
            return self.generate_segment_wall()

    def generate_door(self, door_texture):
        """
        Generates wall containing one door
        """
        if not self.can_have_door():
            return self.generate_segment_wall()
        mesh = Block()
        meshes = [mesh]
        mesh.texture = self.floor.textures['interior_texture']
        half_door_step = (self.end - self.start).normalize() * \
            float(self.floor.specs['door_in_half'])
        self.generate_segment_door(
            self.start, self.end, self.floor.textures['door_in_texture'],
            half_door_step, float(self.floor.specs['door_in_height']),
            float(self.floor.specs['door_in_thickness']), self.length, meshes,
            0)
        front, back = get_block_vertices(self.start, self.end, self.thickness,
                                         self.height)
        mesh.outer_side_face(front, back)
        return meshes

    def generate_segment_wall(self):
        """
        Generates full wall
        """
        front_vertices, back_vertices = get_block_vertices(
            self.start, self.end, self.thickness, self.height)
        f_lu, f_lb, f_rb, f_ru = front_vertices
        b_lu, b_lb, b_rb, b_ru = back_vertices

        # Texture coordinates
        f_lu.t = self.floor.base_height + self.height
        f_lu.s = 0
        f_lb.t = self.floor.base_height
        f_lb.s = 0
        f_rb.s = self.length
        f_rb.t = self.floor.base_height
        f_ru.t = self.floor.base_height + self.height
        f_ru.s = self.length

        b_ru.t = self.floor.base_height + self.height
        b_ru.s = 0
        b_rb.t = self.floor.base_height 
        b_rb.s = 0
        b_lb.s = self.length
        b_lb.t = self.floor.base_height
        b_lu.t = self.floor.base_height + self.height
        b_lu.s = self.length

        meshes = list()
        wall = Block()
        wall.texture = self.floor.textures['interior_texture']
        meshes = [wall]

        if len(self.rooms) == 1:
            front_mesh = Block()
            front_mesh.texture = self.floor.textures['outside_texture']
            front_mesh.quad_face(f_lu, f_lb, f_rb, f_ru)
            front_mesh.outer_side_face(front_vertices, back_vertices)
            meshes.append(front_mesh)
        else:
            wall.quad_face(f_lu, f_lb, f_rb, f_ru)
            wall.outer_side_face(front_vertices, back_vertices)

        # Back face
        wall.quad_face(b_ru, b_rb, b_lb, b_lu)

        return meshes

    def generate_windows(self):
        """
        Generates windows in this wall
        """
        windows_cnt = int(self.length / float(self.floor.specs['win_needed']))
        if windows_cnt == 0:
            return self.generate_segment_wall()

        segment_length = self.length / windows_cnt
        front_mesh = Block()
        front_mesh.texture = self.floor.textures['outside_texture']

        front_outer, back_outer = get_block_vertices(self.start, self.end,
                                                     self.thickness,
                                                     self.height)

        #Textures
        front_outer[0].s = 0
        front_outer[0].t = self.floor.base_height + self.height
        front_outer[1].s = 0
        front_outer[1].t = self.floor.base_height 
        front_outer[2].s = self.length
        front_outer[2].t = self.floor.base_height 
        front_outer[3].s = self.length
        front_outer[3].t = self.floor.base_height + self.height

        back_outer[3].s = 0
        back_outer[3].t = self.floor.base_height + self.height
        back_outer[2].s = 0
        back_outer[2].t = self.floor.base_height 
        back_outer[1].s = self.length
        back_outer[1].t = self.floor.base_height 
        back_outer[0].s = self.length
        back_outer[0].t = self.floor.base_height + self.height

        front_mesh.outer_side_face(front_outer, back_outer)

        back_mesh = Block()
        back_mesh.texture = self.rooms[0].wall_texture

        meshes = [front_mesh, back_mesh]
        step = (self.end - self.start).normalize()
        prev = self.start
        half_win_step = step * float(self.floor.specs['win_half'])
        step = step * segment_length
        for num in range(windows_cnt):
            next = prev + step
            self.generate_segment_window(prev, next, half_win_step,
                                         segment_length, meshes,
                                         num)
            prev = next
        return meshes

    def generate_entrance(self):
        """
        Generates entrance door in the middle and if possible - windows
        """
        segment_cnt = int(self.length /\
            float(self.floor.specs['door_out_needed']))
        if segment_cnt == 0:
            raise FloorException("Cannot fit entrance in wall %s, this may "
                                 "never happen", self)

        segment_length = self.length / segment_cnt
        front_mesh = Block()
        front_mesh.texture = self.floor.textures['outside_texture']

        back_mesh = Block()
        back_mesh.texture = self.rooms[0].wall_texture

        meshes = [front_mesh, back_mesh]
        step_dir = (self.end - self.start).normalize()
        prev = self.start
        half_door_step = step_dir * float(self.floor.specs['door_out_half'])
        half_win_step = step_dir * float(self.floor.specs['win_half'])
        step = step_dir * segment_length
        door_segment = segment_cnt / 2

        front_wall_face, back_wall_face = get_block_vertices(
            self.start, self.end, self.thickness, self.height)

        for cnt in range(segment_cnt):
            next = prev + step
            if cnt == door_segment:
                self.generate_segment_door(
                    prev, next, self.floor.textures['door_out_texture'],
                    half_door_step, float(self.floor.specs['door_out_height']),
                    float(self.floor.specs['door_out_thickness']),
                    segment_length, meshes, cnt)
                self.make_stairs(prev, next,
                                 float(self.floor.specs['stair_height']),
                                 float(self.floor.specs['stair_depth']),
                                 self.floor.building)
            else:
                self.generate_segment_window(prev, next, half_win_step,
                                             segment_length, meshes, cnt)
            prev = next
        front_mesh.outer_side_face((front_wall_face), (back_wall_face))
        return meshes

    def make_door(self, start, end, thickness, height, texture):
        """
        Creates box that represents door

        @param start: Start position of door
        @param end: End position of door
        @param thickness: Door thickness
        @param height: Door height
        @param door texture
        """
        door = Block()
        door.texture = texture
        front, back = get_block_vertices(start, end, thickness, height)

        # Textures
        front[0].s = 0
        front[0].t = 0
        front[1].s = 0
        front[1].t = 1
        front[2].s = 1
        front[2].t = 1
        front[3].s = 1
        front[3].t = 0

        back[0].s = 0
        back[0].t = 0
        back[1].s = 0
        back[1].t = 1
        back[2].s = 1
        back[2].t = 1
        back[3].s = 1
        back[3].t = 0

        door.quad_face(front[0], front[1], front[2], front[3])
        door.quad_face(back[3], back[2], back[1], back[0])
        door.outer_side_face(front, back)

        return door

    def generate_segment_door(self, prev, next, door_texture,
                              half_door_step, door_height, door_thickness,
                              segment_length, meshes, num):
        """
        Creates whole wall that's splitted into segments. Each segments
        contain either nothing, window or door

        @param prev: Starting position of segment
        @param next: Ending position of segment
        @param texture_type: Type of texture for doors
        @param half_door_step: Half width of door (Vector3)
        @param door_height: Height of door
        @param door_thickness: Thickness of door
        @param segment_length: Lenght of this segment
        @param front_mesh: Where faces will be stored for front mesh
        @param back_mesh: Where faces will be stored for back_mesh
        @param num: Segment number in the wall
        """
        if len(meshes) == 1:
            front_mesh = back_mesh = meshes[0]
        elif len(meshes) == 2:
            front_mesh, back_mesh = meshes[:2]
        else:
            LOGGER.error("Segment door got %d meshes", len(meshes))
            front_mesh, back_mesh = meshes[:2]

        w_front, w_back = get_block_vertices(prev, next, self.thickness,
                                             self.height)
        middle = (next + prev) / 2.0
        middle.y = prev.y
        door_left = middle - half_door_step
        door_right = middle + half_door_step
        door_front, door_back = get_block_vertices(
            door_left, door_right, self.thickness, door_height)

        w_front = [Vertex(v.x, v.y, v.z) for v in w_front]
        door_front = [Vertex(v.x, v.y, v.z) for v in door_front]
        # Front outside face
        front_mesh.hole_face_no_bottom(w_front, door_front)

        # Inner wall
        back_mesh.hole_face_no_bottom(
            (w_back[3], w_back[2], w_back[1], w_back[0]),
            (door_back[3], door_back[2], door_back[1], door_back[0])
        )

        new_door_back = list()
        for vert in door_back:
            new_door_back.append(Vertex(vert.x, vert.y, vert.z))

        # Jambs
        front_mesh.quad_face(door_front[0], door_front[1], new_door_back[1],
                             new_door_back[0])
        front_mesh.quad_face(door_front[0], new_door_back[0], new_door_back[3],
                             door_front[3])
        front_mesh.quad_face(new_door_back[3], new_door_back[2], door_front[2],
                             door_front[3])

        # Textures
        w_front[0].s = segment_length * num
        w_front[0].t = self.floor.base_height + self.height
        w_front[1].s = w_front[0].s
        w_front[1].t = self.floor.base_height
        w_front[2].s = w_front[0].s + segment_length
        w_front[2].t = self.floor.base_height
        w_front[3].s = w_front[2].s
        w_front[3].t = self.floor.base_height + self.height

        w_back[3].s = w_front[3].s
        w_back[3].t = w_front[3].t
        w_back[2].s = w_front[2].s
        w_back[2].t = w_front[2].t
        w_back[1].s = w_front[1].s
        w_back[1].t = w_front[1].t
        w_back[0].s = w_front[0].s
        w_back[0].t = w_front[0].t

        # Door textures
        door_front[0].s = w_front[0].s + (door_left - prev).length
        door_front[0].t = self.floor.base_height + door_height
        door_front[1].s = door_front[0].s
        door_front[1].t = self.floor.base_height
        door_front[2].s = door_front[0].s + half_door_step.length * 2
        door_front[2].t = self.floor.base_height
        door_front[3].s = door_front[2].s
        door_front[3].t = door_front[0].t

        door_back[3].s = door_front[3].s
        door_back[3].t = door_front[3].t
        door_back[2].s = door_front[2].s
        door_back[2].t = door_front[2].t
        door_back[1].s = door_front[1].s
        door_back[1].t = door_front[1].t
        door_back[0].s = door_front[0].s
        door_back[0].t = door_front[0].t

        door_mesh = self.make_door(door_left, door_right, door_thickness,
                                   door_height, door_texture)
        meshes.append(door_mesh)

    def generate_segment_window(self, prev, next, half_win_step,
                                segment_length, meshes, num):
        """
        Generates window in the segment
        """
        front_mesh, back_mesh = meshes[:2]
        w_front, w_back = get_block_vertices(prev, next, self.thickness,
                                             self.height)
        middle = (next + prev) / 2.0
        middle.y += float(self.floor.specs['win_y_start'])
        win_left = middle - half_win_step
        win_right = middle + half_win_step
        win_front, win_back = get_block_vertices(
            win_left, win_right, self.thickness,
            float(self.floor.specs['win_height']))

        glass_front, glass_back = get_block_vertices(
            win_left, win_right,
            float(self.floor.specs['glass_thickness']),
            float(self.floor.specs['win_height']),
            self.thickness / 2.0)

        glass = GlassMesh()
        glass.texture = Generator.textures.get_index('glass')
        glass_front[0].s = 0
        glass_front[0].t = 1
        glass_front[1].s = 0
        glass_front[1].t = 0
        glass_front[2].s = 1
        glass_front[2].t = 0
        glass_front[3].s = 1
        glass_front[3].t = 1

        glass_back[0].s = 0
        glass_back[0].t = 1
        glass_back[1].s = 0
        glass_back[1].t = 0
        glass_back[2].s = 1
        glass_back[2].t = 0
        glass_back[3].s = 1
        glass_back[3].t = 1

        glass.quad_face(glass_front[0], glass_front[1], glass_front[2],
                        glass_front[3])
        glass.quad_face(glass_back[3], glass_back[2], glass_back[1],
                        glass_back[0])

        # Textures
        w_front[0].s = segment_length * num
        w_front[0].t = self.floor.base_height + self.height
        w_front[1].s = w_front[0].s
        w_front[1].t = self.floor.base_height 
        w_front[2].s = w_front[0].s + segment_length
        w_front[2].t = self.floor.base_height
        w_front[3].s = w_front[2].s
        w_front[3].t = self.floor.base_height + self.height

        w_back[0].s = w_front[0].s
        w_back[0].t = w_front[0].t
        w_back[1].s = w_front[1].s
        w_back[1].t = w_front[1].t
        w_back[2].s = w_front[2].s
        w_back[2].t = w_front[2].t
        w_back[3].s = w_front[3].s
        w_back[3].t = w_front[3].t

        win_left.y = prev.y
        win_right.y = prev.y
        # Window textures
        win_front[0].s = w_front[0].s + (win_left - prev).length
        win_front[0].t = self.floor.base_height + \
            float(self.floor.specs['win_height']) + \
            float(self.floor.specs['win_y_start'])
        win_front[1].s = win_front[0].s
        win_front[1].t = self.floor.base_height + \
            float(self.floor.specs['win_y_start'])
        win_front[2].s = w_front[0].s + (win_right - prev).length
        win_front[2].t = win_front[1].t
        win_front[3].s = win_front[2].s
        win_front[3].t = win_front[0].t

        win_back[3].s = win_front[3].s
        win_back[3].t = win_front[3].t
        win_back[2].s = win_front[2].s
        win_back[2].t = win_front[2].t
        win_back[1].s = win_front[1].s
        win_back[1].t = win_front[1].t
        win_back[0].s = win_front[0].s
        win_back[0].t = win_front[0].t

        # Front outside face
        front_mesh.hole_face(w_front, win_front)

        # Inner wall
        back_mesh.hole_face(
            (w_back[3], w_back[2], w_back[1], w_back[0]),
            (win_back[3], win_back[2], win_back[1], win_back[0])
        )
        front_mesh.inner_side_face(win_front, win_back)
        meshes.append(glass)

    def can_have_entrance(self):
        """
        Decides whether wall's length is enough for entranace
        """
        return self.length > float(self.floor.specs['door_out_needed']) and \
            len(self.rooms) == 1

    def check_neighbouring(self):
        """
        Checks that wall is not part of wall that is neighbouring with
        other building
        """
        for wall in self.floor.building.n_walls:
            if wall.contains(self):
                self.is_neighbouring = True
                break

    def can_have_door(self):
        """
        Decides whether wall's lenght is enough for interior door
        """
        return self.length > float(self.floor.specs['door_in_needed'])

    def make_stairs(self, start, end, step_height, step_depth,
                    building):
        """
        Generates stairs with texture sides of socle
        """
        start = Vector3(start.x, start.y, start.z)
        end = Vector3(end.x, end.y, end.z)
        height = building.socle[0]
        stair_cnt = int(ceil(height / step_height))
        socle_texture = building.socle[1]
        tiles_texture = Generator.textures.get_index('tiles')
        width = (start - end).length
        for stair_num in range(stair_cnt):
            depth = (stair_num + 1) * step_depth
            # front[1] is top and front[0] is bottom
            front, back = get_block_vertices(end, start, depth, -step_height)
            tiles = Block()
            tiles.texture = tiles_texture

            # Front face
            back[2].s = 0
            back[2].t = step_height
            back[3].s = 0
            back[3].t = 0
            back[0].s = width
            back[0].t = 0
            back[1].s = width
            back[1].t = step_height
            tiles.quad_face(back[2], back[3], back[0], back[1])

            # Top face
            front[2].s = 0
            front[2].t = 0
            back[2].s = 0
            back[2].t = depth
            back[1].s = width
            back[1].t = depth
            front[1].s = width
            front[1].t = 0
            tiles.quad_face(front[2], back[2], back[1], front[1])

            sides = Block()
            sides.texture = socle_texture
            # Left face
            front[2].s = 0
            front[2].t = height - step_height * stair_num
            front[3].s = 0
            front[3].t = front[2].t - step_height
            back[3].s = depth
            back[3].t = front[3].t
            back[2].s = depth
            back[2].t = front[2].t
            sides.quad_face(front[2], front[3], back[3], back[2])
            # Right face
            back[1].s = depth
            back[1].t = front[2].t
            back[0].s = depth
            back[0].t = front[3].t
            front[0].s = 0
            front[0].t = back[0].t
            front[1].s = 0
            front[1].t = back[1].t
            sides.quad_face(back[1], back[0], front[0], front[1])

            building.meshes.append(tiles)
            building.meshes.append(sides)
            start.y -= step_height
            end.y -= step_height


class TexturePack(dict):
    """
    Dictlike object for obtaining random texture from given set
    """

    def __init__(self, directory, *args, **kwargs):
        """
        C'tor
        """
        self.loaded_textures = 0
        self.colors = [
            (0, os.path.join(directory, "colors/c_bof.bmp")),
            (1, os.path.join(directory, "colors/c_old.bmp")),
            (2, os.path.join(directory, "colors/c_house.bmp")),
            (3, os.path.join(directory, "colors/c_other.bmp")),
            (4, os.path.join(directory, "colors/c_nothing.bmp")),
            (5, os.path.join(directory, "colors/c_selected.bmp")),
        ]
        for _, path in self.colors:
            if not os.path.exists(path):
                raise IOError("File %s not found" % path)

    def get_index(self, key):
        """
        Returns random texture index from list based on key

        @param key: Name of group of textures
        @return: Index to texture list in renderer or None if list is empty
        """
        textures = self.get(key, list())
        if not textures:
            LOGGER.warning("No textures with key %s", key)
            return -1
        index = int(uniform(0, len(textures)))
        LOGGER.debug("From %s choose texture from %s", key, textures[index][1])

        return textures[index][0]


class Generator(object):
    """
    Base class for building generator
    """
    textures = None

    @classmethod
    def load_textures_from_dir(cls, dir_path):
        """
        Loads all textures from given directory and its subdirectories

        @param dir_path: Path to directory containing textures
        """
        def add_relative_path_to_list(tex_d, dirname, names):
            """
            Adds texture files to list
            """
            if os.path.basename(dirname) == "colors":
                return
            textures = list()
            dir_name = os.path.basename(dirname)
            tex_d[dir_name] = textures
            for texture_file in names:
                if texture_file.endswith(".bmp"):
                    rel_path = os.path.join(dirname, texture_file)
                    LOGGER.debug("Loaded texture %s", rel_path)
                    textures.append((tex_d.loaded_textures, rel_path))
                    tex_d.loaded_textures += 1

        cls.textures = TexturePack(dir_path)
        os.path.walk(dir_path, add_relative_path_to_list, cls.textures)

    def __init__(self):
        """
        C'tor

        @param polygon: Polygon of building
        """

    def _generate_floor_numbers(self):
        """
        Generates how many floors will the building have
        """
        return int(random.uniform(1, 5))

    def _create_base(self, building, eps=0.3):
        """
        Creates base platform for buidling in a hill
        """
        height_func = lambda v: v.y
        min_height = min(building.nodes, key=height_func).y - eps
        max_height = max(building.nodes, key=height_func).y + eps
        s = 0
        t = max_height - min_height
        building.base_height = max_height
        socle = Mesh()
        socle.texture = Generator.textures.get_index('socle')
        building.socle = (t, socle.texture)
        for prev_node, node in zip(building.nodes,
                                   building.nodes[1:] + [building.nodes[0]]):
            lu = Vertex(prev_node.x, max_height, prev_node.z)
            lu.s = s
            lu.t = t
            lb = Vertex(lu.x, min_height, lu.z)
            lb.s = s
            lb.t = 0
            direction = (node - prev_node)
            dif_len = direction.length
            s += dif_len
            ru = Vertex(node.x, max_height, node.z)
            ru.s = s
            ru.t = t
            rb = Vertex(ru.x, min_height, ru.z)
            rb.s = s
            rb.t = 0
            socle.quad_face(lu, lb, rb, ru)
        building.meshes.append(socle)
        LOGGER.debug("Base created")

    def generate_floor(self, rooms, height, building, entrance,
                       outside_texture, is_base=False):
        """
        Generates floor for building

        @param rooms: List of polygons containing rooms
        @param height: Y-coordination of floor
        """
        floor = Floor(rooms, height, building, entrance, is_base,
                      outside_texture)
        floor_meshes = floor.generate_meshes()
        for wall in floor.walls:
            del wall
        for room in floor:
            del room
        del floor
        return floor_meshes

    def generate_roof(self, building, height, outside_texture):
        """
        Generates roof according building type

        @param building: Building entity
        @param height: Where roof should start
        """
        if building.building_class == BuildingClasses.HOUSE:
            Roof.get_skew_roof(building, height, 1)
        elif building.building_class == BuildingClasses.OLD:
            if random.random() > 0.75:
                Roof.get_skew_roof(building, height, 1)
            else:
                Roof.get_flat_roof(building, height, outside_texture)
        else:
            Roof.get_flat_roof(building, height, outside_texture)

    def generate(self, building):
        """
        Main function generating shape of building

        @param building: Building entity
        """
        specs = config_parser.get_building_specs(building.building_class)
        outside_texture = Generator.textures.get_index(specs['facade'])
        self._create_base(building, float(specs['socle_eps']))
        building.floors = self._generate_floor_numbers()
        sq_polygon = PolygonGen(building.polygon)
        rooms = sq_polygon.squarify(float(specs['wall_thickness']))
        height = building.base_height
        entrance = (Vector3(building.entrance[0].x, height,
                            building.entrance[0].y),
                    Vector3(building.entrance[1].x, height,
                            building.entrance[1].y))
        floor_meshes = self.generate_floor(rooms, height, building,
                                           entrance, outside_texture, True)
        building.meshes.extend(floor_meshes)
        for floor in range(1, building.floors):
            height = float(specs['wall_height']) * floor + building.base_height
            floor_meshes = self.generate_floor(rooms, height, building,
                                               entrance, outside_texture)
            building.meshes.extend(floor_meshes)
        self.generate_roof(building, height + float(specs['wall_height']),
                           outside_texture)


class BOFGenerator(Generator):
    """
    Generator for Block of flats
    """

    def _generate_floor_numbers(self):
        """
        Generates normal distribution for number of floors
        """
        floors = -1
        while floors < 3:
            floors = int(random.normal(loc=7.0, scale=3.0))
        return floors


class HouseGenerator(Generator):
    """
    Generator for house generating
    """

    def _generate_floor_numbers(self):
        """
        Normal, mostly one or two floor buildings
        """
        floors = abs(int(random.normal(loc=2.3, scale=0.5)))
        while not (0 < floors <= 4):
            floors = abs(int(random.normal(loc=2.3, scale=0.5)))
        return floors


class Rectangle(object):
    """
    Rectangle class
    """

    def __init__(self, x, y, width, height):
        """
        C'tor
        """
        self.height = height
        self.width = width
        self.x = x
        self.y = y
        self.subrectangle = None
        self.results = list()
        self.axis = 'x' if width < height else 'y'

    def __str__(self):
        """
        String representation
        """
        return "(%f, %f) - (%f, %f)" % (self.x, self.y, self.width,
                                        self.height)

    def smaller_subrectangle(self):
        """
        Returns smaller edge
        """
        self.axis = 'x' if self.subrectangle.width < self.subrectangle.height\
            else 'y'
        smaller = min(self.subrectangle.width, self.subrectangle.height)
        return smaller

    def subdivide(self, row, width):
        """
        Divides the subrectangle with given areas. Stores new rectangles

        @param row: List of areas in current process
        @param width: Width of axis
        """
        total_sum = sum(row)
        height = get_area_height(total_sum, width)
        new_x = self.subrectangle.x
        new_y = self.subrectangle.y
        if self.axis == "x":
            self.subrectangle.height -= height
            self.subrectangle.y += height
            for area in row:
                width = get_area_height(area, height)
                rect = poly.Polygon([(new_x, new_y),
                                     (width + new_x, new_y),
                                     (width + new_x, new_y + height),
                                     (new_x, height + new_y)])
                self.results.append(rect)
                new_x += width
        elif self.axis == "y":
            self.subrectangle.width -= height
            self.subrectangle.x += height
            for area in row:
                width = get_area_height(area, height)
                rect = poly.Polygon([(new_x, new_y),
                                     (height + new_x, new_y),
                                     (height + new_x, width + new_y),
                                     (new_x, width + new_y)])
                self.results.append(rect)
                new_y += width
        else:
            raise UnknownAxis("Unknown axis %s" % self.axis)

    def start_squarify(self, rooms):
        """
        Starts squarifying

        @params rooms: List of rooms' areas
        """
        self.subrectangle = Rectangle(self.x, self.y, self.width, self.height)
        width = self.smaller_subrectangle()
        self.squarify(rooms, [], width)

    def squarify(self, rooms_areas, row, width):
        """

        @param rooms_areas: Rooms' areas that weren't processed yet
        @param row: Current rooms in the row along one axis
        @param width: Length of current edge of subrectangle
        """
        if not rooms_areas:
            self.subdivide(row, width)
            return
        c = rooms_areas[0]
        if worst(row, width) > worst(row + [c], width):
            self.squarify(rooms_areas[1:], row + [c], width)
        else:
            self.subdivide(row, width)
            self.squarify(rooms_areas, [], self.smaller_subrectangle())


class PolygonGen(object):
    """
    Polygon capable of generating subpolygons
    """
    def __init__(self, *args, **kwargs):
        """
        C'tor
        """
        self.polygon = poly.Polygon(*args, **kwargs)
        self.bounding_box_center = get_bounding_box_center(self.polygon)
        self.original_polygon = poly.Polygon(*args, **kwargs)
        self.origin = self.polygon.center()
        self.angle = 0
        self.rectangle = None

    def _initialize(self):
        """
        Initialize angle and rectangle
        """
        self.angle = 0
        self.polygon = poly.Polygon(self.original_polygon)
        x0, x1, y0, y1 = self.polygon.boundingBox()
        self.rectangle = Rectangle(x0, y0, x1 - x0, y1 - y0)

    def adjust_polygon_rotation(self):
        """
        Rotates polygon to be aligned along ortogonal axis

        @param polygon: Polygon to adjust
        @return: Angle that was polygon rotated, center against it was rotated
        """
        point1, point2 = get_longest_side(self.polygon)
        angle = get_angle(point2, point1, (point1[0] + 1, point1[1]))
        self.angle = -angle if point2[1] > point1[1] else angle
        self.polygon.rotate(self.angle)
        x0, x1, y0, y1 = self.polygon.boundingBox()
        self.rectangle = Rectangle(x0, y0, x1 - x0, y1 - y0)

    def squarify(self, wall_thickness, attempt=30):
        """
        Squarifies bounding box of this polygon and makes a of generated
        rectangles

        @param wall_thickness: Thickness of the wall in this building
        @param attempt: Depth of recursion
        """
        def is_under_threshold(poly, wall_thickness, threshold=0.3):
            """
            Checks shorter edge of given polygon, whether it's smaller than
            threshold

            @param poly: Polygon for checking its bounding box
            @param wall_thickness: Thickness of walls in the building
            @param threshold: Threshold for comparing
            @return: True if it's under threshold
            """
            x0, x1, y0, y1 = poly.boundingBox()
            shorter = min(abs((x0 - x1)), abs(y0 - y1))
            LOGGER.debug("Comparing shorter of bounding box: %f", shorter)
            shortest = get_shortest_side_length(poly)

            return shorter < threshold or shortest < wall_thickness

        results = list()
        self.adjust_polygon_rotation()
        areas = self.generate_rooms_areas()
        LOGGER.debug("Generated rooms %s", areas)
        self.rectangle.start_squarify(areas)
        for rect in self.rectangle.results:
            result = self.polygon & rect
            if result:
                if is_under_threshold(result, wall_thickness) and attempt > 0:
                    self._initialize()
                    results = self.squarify(wall_thickness, attempt - 1)
                    break
                result.rotate(-self.angle, self.origin[0], self.origin[1])
                result = order_polygon(result)
                results.append(result)
        else:
            LOGGER.debug("Generated on %d. attempt", 31 - attempt)
        self.polygon.rotate(-self.angle)
        self.fix_polygon_pos(results)
        self._initialize()
        return results

    def fix_polygon_pos(self, polygons):
        """
        """
        broken_polygon = Polygon()
        for pol in polygons:
            broken_polygon |= pol
        bp_center = get_bounding_box_center(broken_polygon)
        diff = (self.bounding_box_center[0] - bp_center[0],
                self.bounding_box_center[1] - bp_center[1])
        for pol in polygons:
            pol.shift(diff[0], diff[1])

    def bounding_box_area(self):
        """
        Gets area of bounding box
        """
        x0, x1, y0, y1 = self.polygon.boundingBox()
        return (x1 - x0) * (y1 - y0)

    def generate_room_count(self):
        """
        Generates number of rooms in one floor

        @param area: Area of given bounding box
        """
        return int(ceil(pow(self.bounding_box_area(), ONE_THIRD) +
                        6 * random.random()))

    def generate_rooms_areas(self, list_type='int'):
        """
        Generates areas of particular rooms and sum of them is the area

        @param list_type: Type of values in retunred list ('int' or 'float')
        """
        area = self.bounding_box_area()
        num = self.generate_room_count()
        LOGGER.debug("Generated %d rooms", num)
        if list_type == 'int':
            opt_func = lambda x: ceil(x)
        elif list_type == 'float':
            opt_func = lambda x: x
        else:
            raise TypeError("Type '%s' is not 'int' neither 'float'"
                            % list_type)
        generated = [random.random() for _ in range(num)]
        generated_sum = sum(generated)
        floors = [opt_func(area*rand/generated_sum) for rand in generated]
        if list_type == 'int':
            floors.remove(max(floors))
            displacment = area - sum(floors)
            floors.append(displacment)
        floors.sort()

        return floors[::-1]


def divide_polygon(polygon, aspect, axis):
    """
    Divides polygon by given aspect and along given axis

    @param polygon: Polygon to divide
    @param aspect: Aspect of the new division
    @param axis: Along which axis will be division performed
    @return Two polygons created by division
    """
    bx0, bx1, by0, by1 = polygon.boundingBox()
    if axis == "x":
        bxn = (bx0 + bx1) * aspect
        rect = PolygonGen([(bx0, by0), (bxn, by0), (bxn, by1), (bx0, by1)])
    elif axis == "y":
        byn = (by0 + by1) * aspect
        rect = PolygonGen([(bx0, by0), (bx1, by0), (bx1, byn), (bx0, byn)])
    poly1 = polygon - rect
    poly2 = polygon - poly1

    return poly1, poly2


def worst(areas, w):
    """
    @param areas: list of areas
    """
    areas_sum_sq = sum(areas) ** 2
    w_sq = float(w * w)
    return max([max(w_sq * r / areas_sum_sq, areas_sum_sq / (w_sq * r))
                for r in areas]) if areas else float('inf')


def worst2(areas, w):
    areas_sum_sq = sum(areas) ** 2
    w_sq = float(w * w)
    max_r = max(areas)
    min_r = min(areas)
    return max(w_sq * max_r / areas_sum_sq, areas_sum_sq / (w_sq * min_r))\
        if areas else 0


def get_area_height(area, width):
    """
    Computes height of area when width is known

    @param area: Area of rectangle
    @param width: Width of rectangle
    @return: Height of rectangle
    """
    return float(area)/width


if __name__ == "__main__":
    from dp.entities import Building
    building = Building(1)
    building.add_node(Vector2(0, 0))
    building.add_node(Vector2(3, 0))
    building.add_node(Vector2(3, 6))
    building.add_node(Vector2(1, 6))
    building.add_node(Vector2(1, 4))
    building.add_node(Vector2(0, 4))
    building.create_polygon()
    Generator.load_textures_from_dir("dp/view/textures")
    rooms = [Polygon([(0, 0), (5, 0), (5, 3), (0, 3)]),
             Polygon([(5, 0), (9, 0), (9, 3), (5, 3)]),
             Polygon([(0, 3), (6, 3), (6, 5), (0, 5)]),
             Polygon([(6, 3), (9, 3), (9, 5), (6, 5)])]
    v1 = Vector3(0, 0, 0)
    v2 = Vector3(5, 0, 0)
    specs = config_parser.get_building_specs(3)
    class Fl(object):
        pass
    floor = Fl()
    building = Fl()
    room = Fl()
    setattr(room, 'wall_texture', 0)
    textures = { 'interior_texture': 0,
                 'outside_texture': 0,
                 'door_in_texture': 0}
    setattr(building, 'n_walls', list())
    setattr(floor, 'building', building)
    setattr(floor, 'specs', specs)
    setattr(floor, 'walls', list())
    setattr(floor, 'base_height', 0)
    setattr(floor, 'textures', textures)
    setattr(room, 'door_texture', 0)
    w1 = Wall(v1, v2, floor)
    w1.rooms.append(room)
    w1.rooms.append(1)
    w1.has_door = True
    w1.generate_mesh()
