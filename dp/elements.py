"""
Module for basic elements
"""
import logging
from numpy import linspace
from Polygon import Polygon

from dp.computations.ageometr import Vector3, Vector2
from dp.computations import geometrfunc
from dp.config import config_parser


LOGGER = logging.getLogger("dpLogger")
DENS_FAC = config_parser.getfloat('roof', 'density_factor')


class VertexNotFoundException(Exception):
    """
    Exception that vertex has not been found
    """


def scale(polygon, pos):
    """
    Scales polygon in direction of vertex normals

    @param polygon: Polygon that will be scaled
    @param pos: Step of which will be the polygon scaled
    """
    new_points = list()
    length = len(polygon[0])
    for index, point in enumerate(polygon[0]):
        prev = polygon[0][index - 1]
        next = polygon[0][(index + 1) % length]
        prev = Vector2(prev[0], prev[1])
        next = Vector2(next[0], next[1])
        point = Vector2(point[0], point[1])
        prev_norm = (point - prev).ort().normalize()
        next_norm = (next - point).ort().normalize()
        new_point = (prev_norm + next_norm).normalize() * pos + point
        new_point = (new_point.x, new_point.y)
        new_points.append(new_point)
    return Polygon(new_points)


def get_polygon_sides(polygon):
    """
    Gets tuples of all sides in polygon

    @param polygon: Polygon from which sides will be returend
    """
    for first, second in zip(polygon[0], polygon[0][1:] + [polygon[0][0]]):
        yield Vector2(first[0], first[1]), Vector2(second[0], second[1])


def sample(polygon):
    """
    Gets uniform samples from polygon in both axises

    @param polygon: Polygon that will be sampled
    """
    first, second = get_shortest_side(polygon)
    x0, x1, y0, y1 = polygon.boundingBox()
    sample_num_x = int((x1 - x0) * DENS_FAC)
    sample_num_y = int((y1 - y0) * DENS_FAC)
    xs = linspace(x0, x1, sample_num_x)
    ys = linspace(y0, y1, sample_num_y)
    step = min(xs[1] - xs[0], ys[1] - ys[0])

    x_list = list()
    y_list = list()

    for prev, node in zip(polygon[0], polygon[0][1:] + [polygon[0][0]]):
        node, prev = Vector2(node[0], node[1]), Vector2(prev[0], prev[1])
        x_list.append(prev.x)
        y_list.append(prev.y)
        direction = node - prev
        count = int(direction.length / step) + 1
        dir_step = direction.normalize() * step
        for i in range(1, count):
            new = prev + dir_step * i
            x_list.append(new.x)
            y_list.append(new.y)
        

    for y in ys:
        for x in xs:
            if polygon.isInside(x, y):
                x_list.append(x)
                y_list.append(y)
    return x_list, y_list


def get_bounding_box_center(polygon):
    """
    Creates polygon bounding box and retunrs its center

    @param polygon: Polygon from whcih bounding box center will be returned
    @return: Bounding box center
    """
    x0, x1, y0, y1 = polygon.boundingBox()
    bb = Polygon([(x0, y0), (x1, y0), (x1, y1), (x0, y1)])
    return bb.center()


def get_shortest_side_length(polygon):
    """
    Returns length of the shortest side of polygon side

    @param polygon: Polygon from which shortest side length will be returned
    @return: Shortest side length
    """
    first, second = get_shortest_side(polygon)
    return (Vector2(first[0], first[1]) - Vector2(second[0], second[1])).length


def get_shortest_side(polygon):
    """
    Returns shortes side of the polygon

    @param polygon: Polygon from which we want shortest side
    """
    min_length = float('inf')
    retval = None
    for first, second in zip(polygon[0], polygon[0][1:] + [polygon[0][0]]):
        dir = Vector2(first[0], first[1]) - Vector2(second[0], second[1])
        if dir.length < min_length:
            min_length = dir.length
            retval = (first, second)
    return retval


def get_longest_side(polygon):
    """
    Gets two points that represents longest side of polygon
    """
    max_length = -1
    first = polygon[0][0]
    retval = None
    for point in polygon[0][1:]:
        dir = Vector2(first[0], first[1]) - Vector2(point[0], point[1])
        if dir.length > max_length:
            max_length = dir.length
            retval = (first, point)
        first = point
    return retval


def generate_polygon_mesh(polygon, orientation=True):
    """
    Generates mesh that's same as polygon

    @param polygon: Polygon from which mesh will be created
    @param orientation: Orientation of triangles - True if ccw, False for cw
    """
    def check_triangle(v1, v2, v3):
        """
        Checks that vertices can make triangle

        @param v1:
        @param v2:
        @param v3:
        """
        return v1 == v2 or v1 == v3 or v2 == v3

    mesh = Mesh()
    for polygon in polygon.triStrip():
        vertex1 = Vertex(polygon[0][0], 0, polygon[0][1])
        vertex2 = Vertex(polygon[1][0], 0, polygon[1][1])
        vertex1.s = vertex1.x
        vertex1.t = vertex1.z
        vertex2.s = vertex2.x
        vertex2.t = vertex2.z
        for vertex in polygon[2:]:
            vertex3 = Vertex(vertex[0], 0, vertex[1])
            vertex3.s = vertex3.x
            vertex3.t = vertex3.z
            if check_triangle(vertex1, vertex2, vertex3):
                vertex1 = vertex2
                vertex2 = vertex3
                continue
            if geometrfunc.is_non_left_turn((vertex1.x, vertex1.z), 
                                            (vertex2.x, vertex2.z),
                                            (vertex3.x, vertex3.z)):
                if orientation:
                    mesh.add_face(Face((vertex1, vertex3, vertex2)))
                else:
                    mesh.add_face(Face((vertex1, vertex2, vertex3)))
            else:
                if orientation:
                    mesh.add_face(Face((vertex1, vertex2, vertex3)))
                else:
                    mesh.add_face(Face((vertex1, vertex3, vertex2)))

            vertex1 = vertex2
            vertex2 = vertex3
    return mesh


def shift_polygon_to_cener(polygon, center):
    """
    Fizes polygon bounding box center according center

    @param polygon: Polygon to fix
    @param center: Center the should be center of polygon
    """
    bp_center = get_bounding_box_center(polygon)
    diff = (center[0] - bp_center[0], center[1] - bp_center[1])
    polygon.shift(diff[0], diff[1])


def adjust_polygon_rotation(polygon):
    """
    Adjust polygon according longest side of polygon
    """
    point1, point2 = get_longest_side(polygon)
    angle = geometrfunc.get_angle(point2, point1,
                            (point1[0] + 1,
                             point1[1]))
    angle = -angle if point2[1] > point1[1] else angle
    polygon.rotate(angle)

    return angle


def order_polygon(polygon):
    """
    Order points of polygon therefore its points are ordered counter-clockwise

    @param polygon: Polygon to order contours
    @return: Polygon that is counter-clockwise
    """
    # Polygon is clockwise
    if polygon.orientation()[0] < 0:
        new_polygon = Polygon()
        for contour in polygon:
            contour.reverse()
            new_polygon.addContour(contour)
        return new_polygon
    return polygon


def get_nearest_point(polygon, point):
    """
    Gets the nearest point of polygon vertex

    @param polygon: Polygon (way polygon)
    @param point: Point of building vertex
    """
    height = point.y
    point = Vector2(point.x, point.z)
    distance = float('inf')
    nearest = None
    for contour in polygon:
        for vert in contour:
            v1 = Vector2(vert[0], vert[1])
            dist = (point - v1).length
            if dist < distance:
                distance = dist
                nearest = v1
    return Vector3(nearest.x, height, nearest.y)


def rotated_rectangle(p1, p2, width, shift=False):
    """
    Creates rotated rectangle polygon
    
    @param p1: First point of rectangle
    @param p2: Last point of rectangle
    @param width: Width of rectangle

    @return: Rotated rectangle polygon
    """
    ortogonal = p1.direction(p2).ort() * width
    ortogonal_opposite = ortogonal.invert()
    lu = p1 + ortogonal
    lb = p1 + ortogonal_opposite
    ru = p2 + ortogonal
    rb = p2 + ortogonal_opposite

    poly = Polygon([tuple(lu), tuple(lb), tuple(rb), tuple(ru)])
    if shift:
        poly.shift(ortogonal_opposite.x, ortogonal_opposite.y)
    return poly


class Vertex(object):
    """
    Vertex
    """
    eps = 0.0001

    def __init__(self, x, y, z, nx=0, ny=0, nz=0, s=0, t=0):
        """
        C'tor
        """
        self.x = x
        self.y = y
        self.z = z
        self._normal = Vector3(nz, ny, nz)
        self.prevlen = 0
        self.s = s
        self.t = t
        self.faces = set()
        self.index = -1

    def __eq__(self, other):
        """
        Compares position of vertex
        """
        return abs(self.x - other.x) < self.eps and\
                abs(self.y - other.y) < self.eps and\
                abs(self.z - other.z) < self.eps and\
                abs(self.s - other.s) < self.eps and\
                abs(self.t - other.t) < self.eps

    def __ne__(self, other):
        """
        """
        return not self.__eq__(other)

    @property
    def normal(self):
        """
        Computes average normal for this vertex according its faces
        """
        if len(self.faces) == self.prevlen:
            return self._normal
        self.prevlen = len(self.faces)
        for face in self.faces:
            self._normal += face.normal
        self._normal = self._normal.normalize()
        return self._normal

    def __repr__(self):
        """
        Object represantation, prints position only
        """
        return "(%f, %f, %f)" % (self.x, self.y, self.z)

    def __str__(self):
        """
        String representation
        """
        position = "Position: (%f, %f, %f)\n" % (self.x, self.y, self.z)
        normal = "Normal: (%f, %f, %f)\n" % (self._normal.x, self._normal.y,
                                             self._normal.z)
        texture = "Texture: (%f, %f)\n" % (self.s, self.t)
        return "%s%s%s\n" % (position, normal, texture)

    def __sub__(self, other):
        """
        Substracts two vertices and creates Vector3
        """
        x_diff = self.x - other.x
        y_diff = self.y - other.y
        z_diff = self.z - other.z

        return Vector3(x_diff, y_diff, z_diff)

    def __add__(self, other):
        """
        Adds two vertices and creates Vector3
        """
        x_diff = self.x + other.x
        y_diff = self.y + other.y
        z_diff = self.z + other.z

        return Vector3(x_diff, y_diff, z_diff)


class Face(tuple):
    """
    Face
    """

    def __init__(self, *args, **kwargs):
        """
        C'tor
        """
        super(Face, self).__init__(*args, **kwargs)
        if len(self) < 3:
            LOGGER.warning("Cannot compute normal for face, countains only "
                           "%d vertices", len(self))
            return
        for vertex in self:
            vertex.faces.add(self)
        vector1 = Vector3(self[1].x, self[1].y, self[1].z) - \
            Vector3(self[0].x, self[0].y, self[0].z)
        vector2 = Vector3(self[2].x, self[2].y, self[2].z) - \
            Vector3(self[1].x, self[1].y, self[1].z)
        self.normal = (vector2 * vector1).normalize()


class Vertices(list):
    """
    List of vertices, each vertex remembers its position and is uniq in the
    list
    """

    def append(self, vertex):
        """
        Add new vertex to the list, set its index
        """
#        vertex = copy(vertex)
        vertex.index = self.__len__()
        super(Vertices, self).append(vertex)

    def extend(self, vertices):
        """
        Extends current list with list of vertices
        """
        for vertex in vertices:
            self.append(vertex)

    def append_exclusive(self, vertex):
        """
        Appends vertex if it's not in the list but if it is, reference to
        existing vertex is returned

        @param vertex: New vertex
        @return: Added or existing vertex
        """
        if vertex in self:
            index = self.index(vertex)
            vertex = self[index]
        else:
            self.append(vertex)
        return vertex

    def get_vertex(self, vertex, default=None):
        """
        Gets the vertex with same coordinations

        @param: vertex that has same coordination as vertex in the list
        """
        for v in self:
            if v == vertex:
                ret_vert = v
                break;
        else:
            if default is None:
                raise VertexNotFoundException("Vertex %s not found in mesh" %
                                              vertex)
            else:
                ret_vert = default
        return ret_vert


class Mesh(object):
    """
    Complete 3D mesh
    """
    def __init__(self, mesh=None):
        """
        C'tor

        @attribute texture: Index of texture
        @attribute vertice: Vertices object (list of vertices)
        @attribute faces: List of faces
        """
        self.texture = -1
        self.vertices = Vertices()
        self.faces = list()
        if mesh is not None:
            self.extend_mesh(mesh)

    def extend_mesh(self, other):
        """
        Extends this mesh with another mesh
        """
        for face in other.faces:
            self.add_face(face)

    def add_face(self, face):
        """
        Adds face to list and missing vertices adds to list of vertices
        """
        self.faces.append(face)
        for vertex in face:
#            if vertex not in self.vertices:
            self.vertices.append(vertex)
#            else:
#                similar = self.vertices.get_vertex(vertex)
#                vertex.index = similar.index

    @property
    def tri_indices(self):
        """
        Generates tuple of three indexes that makes triangles
        """
        for face in self.faces:
            yield (face[0].index, face[1].index, face[2].index)

    @property
    def indices(self):
        """
        Generates indexes for faces of mesh
        """
        for face in self.faces:
            for vert in face:
                yield vert.index

    def quad_face(self, lu, lb, rb, ru, copy=True):
        """
        Generates two faces that creates rectangle

        @param lu: Left upper corner of quad
        @param lb: Left bottom corner of quad
        @param rb: Right bottom corner of quad
        @param ru: Right upper corner of quad
        @param copy: Create new vertices
        """
        if copy:
            lu = Vertex(lu.x, lu.y, lu.z, s=lu.s, t=lu.t)
            lb = Vertex(lb.x, lb.y, lb.z, s=lb.s, t=lb.t)
            ru = Vertex(ru.x, ru.y, ru.z, s=ru.s, t=ru.t)
            rb = Vertex(rb.x, rb.y, rb.z, s=rb.s, t=rb.t)
        self.add_face(Face((lu, lb, ru)))
        self.add_face(Face((lb, rb, ru)))


class Block(Mesh):
    """
    Block mesh
    """

    def outer_side_face(self, front, back):
        """
        Creates outer faces of box (i.e. not front and back faces - only top,
        sides and bottom

        @param front: vertices of front face (tuple of 4)
        @param back: vertices of back face (tuple of 4)
        """
        # Left
        self.quad_face(back[0], back[1], front[1], front[0], True)
        # Right
        self.quad_face(front[3], front[2], back[2], back[3], True)
        # Top
        self.quad_face(back[0], front[0], front[3], back[3], True)
        # Bottom
        self.quad_face(front[1], back[1], back[2], front[2], True)

    def inner_side_face(self, front, back):
        """
        Creates inner faces of box (i.e. not front and back faces - only top,
        sides and bottom

        @param front: vertices of front face (tuple of 4)
        @param back: vertices of back face (tuple of 4)
        """
        # Copy front vertices
        new_back = list()
        for vert in back:
            new_back.append(Vertex(vert.x, vert.y, vert.z))
        # Left
        self.quad_face(front[0], front[1], new_back[1], new_back[0])
        # Right
        self.quad_face(new_back[3], new_back[2], front[2], front[3])
        # Top
        self.quad_face(front[0], new_back[0], new_back[3], front[3])
        # Bottom
        self.quad_face(new_back[1], front[1], front[2], new_back[2])

    def hole_face(self, front, hole):
        """
        Creates face with hole withing

        @param front: vertices of front face (tuple of 4)
        @param hole: tuple of 4 vertices making hole in the face
        """
        front = [Vertex(vert.x, vert.y, vert.z, s=vert.s, t=vert.t) for vert in
                 front]
        hole = [Vertex(vert.x, vert.y, vert.z, s=vert.s, t=vert.t) for vert in
                 hole]
        self.add_face(Face((front[0], front[1], hole[0])))
        self.add_face(Face((hole[0], front[1], hole[1])))
        self.add_face(Face((front[1], front[2], hole[1])))
        self.add_face(Face((front[2], hole[2], hole[1])))
        self.add_face(Face((front[2], front[3], hole[2])))
        self.add_face(Face((front[3], hole[3], hole[2])))
        self.add_face(Face((front[3], front[0], hole[3])))
        self.add_face(Face((front[0], hole[0], hole[3])))

    def hole_face_no_bottom(self, front, hole):
        """
        Generates mesh that represents door hole in a wall

        @param: front - vertices of the wall (tuple of 4 - lu, lb, rb, ru)
        @param: hole - vertices of a hole (tuple of 4 - lu, lb, rb, ru)
        """
        self.add_face(Face((front[0], front[1], hole[0])))
        self.add_face(Face((front[1], hole[1], hole[0])))
        self.add_face(Face((front[0], hole[0], front[3])))
        self.add_face(Face((front[3], hole[0], hole[3])))
        self.add_face(Face((front[3], hole[3], front[2])))
        self.add_face(Face((hole[3], hole[2], front[2])))


class GlassMesh(Block):
    """
    Different type for distinguishing whether start blending
    """


if __name__ == "__main__":
    from Polygon.IO import writeSVG
    poly = Polygon([(12.154558015000003, 12.244670513000003),
                    (11.70183934122247, 12.225845697730561),
                    (11.825553099362878, 9.170290265311504),
                    (13.549002783226463, 9.240069538531186),
                    (14.542061444000003, 10.24108384099999),
                    (13.601532954000001, 10.241084864000001),
                    (13.456831536000003, 11.131567948999997)])
    writeSVG('platform.svg', poly)
    writeSVG('tristrip.svg', poly.triStrip())
    triangles = list()
    mesh = generate_polygon_mesh(poly)
    for tri in mesh.tri_indices:
        vert1 = mesh.vertices[tri[0]]
        vert2 = mesh.vertices[tri[1]]
        vert3 = mesh.vertices[tri[2]]
        triangles.append(Polygon([(vert1.x, vert1.z), (vert2.x, vert2.z),
                                 (vert3.x, vert3.z)]))
    writeSVG('tri.svg', triangles)
