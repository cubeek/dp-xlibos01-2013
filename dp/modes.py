"""
Module provides modes classes, the main objects
"""
import cPickle
import logging
import time

from dp import common
from dp.config import config_parser
from dp.classifier.classifier import BuildingClassifier
from dp.controller import Controller
from dp import downloader
from dp.osmparser import Parser
from dp.projector import Projector


LOGGER = logging.getLogger("dpLogger")


class Mode(object):
    """
    Base class for modes
    """
    controls_help = ""

    def __init__(self):
        """
        C'tor of base mode
        """
        self.controller = None
        self.osm_server = None
        self.bounds = None
        self.ambient_color = tuple(config_parser.get_json('light', 'ambient'))
        self.diffuse_color = tuple(config_parser.get_json('light', 'diffuse'))
        self.spec_color = tuple(config_parser.get_json('light', 'specular'))
        self.background_color = None

    def preprocess(self):
        """
        Data preprocessing before rendering starts
        """
        pass

    def postprocess(self):
        """
        Data postrprocess after program terminated
        """
        pass

    def _download_and_parse_map(self):
        """
        Downloads map from OSM, creates parser and returnes created objects
        list

        @return List of entities created by Parser
        """
        entities = list()
        with downloader.osm.OsmApi(self.osm_server) as osm:
            xmlmap = osm.get_xml_map(self.bounds)
        osm_parser = Parser(xmlmap)
        LOGGER.info("Parsing entities from XML")
        for entity in osm_parser:
            entities.append(entity)
        LOGGER.debug("All entities parsed")
        bounds = osm_parser.bounds
        self.controller.world.bounds = bounds
        self.controller.world.projector = Projector(bounds[0], bounds[2],
                                                    bounds[1], bounds[3])
        del osm_parser
        return entities

    def set_renderer_view(self, width, height):
        """
        Sets renderer specifications
        """
        middle_x = width / 2.0
        middle_z = height / 2.0
        if self.controller.world.heightmap is None:
            middle_y = 400
        else:
            middle_plain = self.controller.world.heightmap.get_plain(middle_x,
                                                                     middle_z)
            middle_y = middle_plain.get_y(middle_x, middle_z) + 100
        self.controller.renderer.camera.pos.x = -middle_x
        self.controller.renderer.camera.pos.y = middle_y
        self.controller.renderer.camera.pos.z = middle_z
        self.controller.renderer.camera.pitch = 90
        LOGGER.info("Camera se to (%f, %f, %f)", -middle_x, middle_y, middle_z)

        # Set sun position
        if self.controller.renderer.mode == self.controller.lib.VIEW:
            self.controller.renderer.light_position = (-middle_x,
                                                       middle_y,
                                                       middle_z, 1.0)
            LOGGER.info("Light set to (%f, %f, %f)", -middle_x, middle_y + 300,
                        middle_z)
            self.controller.renderer.ambient_color = self.ambient_color
            self.controller.renderer.diffuse_color = self.diffuse_color
            self.controller.renderer.spec_color = self.spec_color

        # Set background color
        self.controller.renderer.clear_color = self.background_color

    def start(self, **kwargs):
        """
        Starts mode

        Sets attributes for mode given by start function
        """
        self.input = kwargs.get('input', None)
        self.output = kwargs.get('output', None)


class CollectMode(Mode):
    """
    Mode for collecting data for classifier training
    """
    controls_help = """
                    COLLECT MODE
                    ============

                    Use mouse to look around

                    Movement
                    --------
                        W - move forward
                        S - move backward
                        D - move right
                        A - move left
                        = - Increase speed
                        - - Decrease speed

                    Setting buildings' classes
                    --------------------------
                        1 - Set class to Block of flats
                        2 - Set class to Old city house
                        3 - Set class to Family house
                        4 - Set class to Other
                        0 - Set class to Nothing (will not be part of training,
                            testing data)
                        N - Select next building
                        B - Select previous building
                    """

    def __init__(self, osm_server, rect, input, output):
        """
        C'tor, sets inter variables
        """
        super(CollectMode, self).__init__()
        self.controller = Controller(800, 600, "OSM building collector - "
                                               "COLLECT MODE")
        self.osm_server = osm_server
        self.rect = rect
        self.xml_file = input
        self.output = output
        self.background_color = (0.0, 0.0, 0.0, 1.0)

    def preprocess(self):
        """
        Parses XML from file, creates entities
        """
        if self.xml_file is not None:
            LOGGER.info("Loading entities from %s", self.xml_file)
            with open(self.xml_file, 'r') as xml_h:
                xmlstring = xml_h.read()
        elif self.rect is not None:
            with downloader.osm.OsmApi(self.osm_server) as osm:
                xmlstring = osm.get_xml_map(self.bounds)
        else:
            raise Exception("This will never happen")

        osm_parser = Parser(xmlstring)
        bounds = osm_parser.bounds
        self.controller.vertex_shader_path = common.VERTEX_SHADER_PATH
        self.controller.fragment_shader_path = \
            common.NOLIGHT_FRAGMENT_SHADER_PATH
        self.controller.init_renderer(self.controller.lib.COLLECT)
        projector = Projector(bounds[0], bounds[2], bounds[1], bounds[3])
        self.controller.world.projector = projector
        for entity in osm_parser:
            self.controller.world.append(entity)
        self.controller.world.ways.create_way()
        self.controller.world.set_buildings_neighbours()
        self.controller.world.extend_buildings_features()
        self.set_renderer_view(projector.width, projector.height)

    def postprocess(self):
        """
        Sets classes for building types and creates data file for classifier
        training
        """
        self.controller.set_building_classes()
        self.controller.world.save_features_to_file(self.output)
        self.controller.lib.cleanup(self.controller.lp_renderer)

    def start(self, **kwargs):
        """
        Starts program in training mode
        """
        self.preprocess()
        self.controller.copy_entities_to_gpu()
        print self.controls_help
        self.controller.window.run()
        self.postprocess()


class BuildMode(Mode):
    """
    Main build mode, downloads map
    """
    controls_help = """
                    BUILD MODE
                    ============

                    Use mouse to look around

                    Movement
                    --------
                        W - move forward
                        S - move backward
                        D - move right
                        A - move left
                        = - Increase speed
                        - - Decrease speed

                    Debug:
                        O - switch between TRIANGLES, LINES and POINTS
                        P - witch between polygon modes (filled or lines)
                        G - switch whether buildings should be rendered
                        H - switch whether heightmap should be rendered
                        J - switch whether ways should be rendered
                        R - reset camera pitch and yaw
                    """

    def __init__(self, bounds, osm_server, gg_server, classifier_paths):
        """
        C'tor
        """
        super(BuildMode, self).__init__()
        self.controller = Controller(800, 600, "OSM building generator - "
                                               "BUILD MODE")
        self.bounds = bounds
        self.osm_server = osm_server
        self.gg_server = gg_server
        self.classifier_path = classifier_paths[0]
        self.classifier_data_path = classifier_paths[1]
        self.background_color = (0.3137254901960784, 0.7843137254901961,
                                 0.9019607843137255, 1.0)

    def preprocess(self):
        """
        Downloads all data (entities, heightmap), transforms, creates meshes
        """
        self.controller.vertex_shader_path = common.VERTEX_SHADER_PATH
        self.controller.fragment_shader_path = common.FRAGMENT_SHADER_PATH
        self.controller.init_renderer(self.controller.lib.VIEW)
        entities = self._download_and_parse_map()
        LOGGER.info("Downloads from OSM data completed")
        self.controller.load_height_map(self.gg_server)
        LOGGER.info("World now contains heightmap")
        start_time = time.time()
        self.controller.world.process_entities(entities, True)
        LOGGER.info("There are %d buildings, %d ways and %d unknown entities",
                    len(self.controller.world.buildings),
                    len(self.controller.world.ways),
                    len(self.controller.world.entities))
        self.controller.world.set_buildings_neighbours()
        self.controller.world.extend_buildings_features()
        self.controller.load_classifier(self.classifier_path,
                                        self.classifier_data_path)
        self.controller.process_buildings(True)
        projector = self.controller.world.projector
        self.set_renderer_view(projector.width, projector.height)
        duration = time.time() - start_time
        LOGGER.info("The whole scene created in %f seconds", duration)

    def start(self, **kwargs):
        """
        Starts mode
        """
        super(BuildMode, self).start(**kwargs)
        self.preprocess()
        self.controller.copy_entities_to_gpu(self.output)
        print self.controls_help
        self.controller.window.run()
        self.postprocess()

    def postprocess(self):
        """
        Frees memory, stores world if needed
        """
        self.controller.lib.cleanup(self.controller.lp_renderer)


class ViewMode(Mode):
    """
    View already generated world
    """
    def __init__(self, filename):
        """
        Constructor for view mode

        @params filename: Path to file containing world
        """
        super(ViewMode, self).__init__()
        self.controller = Controller(800, 600, "OSM building viewer - "
                                               "VIEW MODE")
        self.background_color = (0.3137254901960784, 0.7843137254901961,
                                 0.9019607843137255, 1.0)
        with open(filename,'r') as world_file:
            self.world_list = cPickle.load(world_file)

    def preprocess(self):
        """
        Initializes renderer, loads world
        """
        width, height = self.world_list.pop(0)
        self.controller.vertex_shader_path = common.VERTEX_SHADER_PATH
        self.controller.fragment_shader_path = common.FRAGMENT_SHADER_PATH
        self.controller.init_renderer(self.controller.lib.VIEW)
        self.set_renderer_view(width, height)

    def start(self, **kwargs):
        """
        Starts view mode
        """
        self.preprocess()
        self.controller.copy_pickable_to_gpu(self.world_list)
        self.controller.window.run()


class TrainMode(object):
    """
    Mode for training classifier
    """
    def __init__(self, output, training_dataset_file, testing_dataset_file,
                 C, kernel, gamma, show_roc):
        """
        Creates classifier and sets C and kernel parameter
        """

        self.classifier = BuildingClassifier(output, training_dataset_file,
                                             testing_dataset_file, show_roc)
        self.C = C
        self.gamma = gamma
        self.kernel = kernel

    def start(self, output, **kwargs):
        """
        Starts classifier training
        """
        self.classifier.create_classifier(self.C, self.kernel, self.gamma)
        self.classifier.save()
