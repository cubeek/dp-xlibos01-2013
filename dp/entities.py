"""
This module contains class for one building extracted from XML file
"""
import logging
from Polygon import Shapes

from dp.classifier.classifier import BuildingClasses
from dp.computations import geometrfunc
from dp.computations.convexhull import graham_scan
from dp.config import config_parser
from dp.elements import Vertex, rotated_rectangle, Polygon, order_polygon, \
    generate_polygon_mesh
from dp.generator import Generator


LOGGER = logging.getLogger("dpLogger")


def instancecheck(f):
    def wrapper(self, other):
        if not isinstance(other, self.__class__):
            raise TypeError
        return f(self, other)
    return wrapper


class Entity(object):
    """
    This is class for basic entity in the scene.
    """
    GPS_TYPE = "gps"
    CARTESIAN_TYPE = "cartesian"

    def __init__(self, id=None, tags=dict()):
        """
        Constructor
        """
        self.id = id
        self.osm_nodes = list()
        self.nodes = list()
        self.meshes = list()
        self.altitude = 0
        self._tags = tags
        self.features = dict()
        self._position = None
        self.polygon = None
        self.render = True
        self.coordinates_type = self.GPS_TYPE
        self.base_height = None

    def __iter__(self):
        """
        Iterates over nodes
        """
        for node in self.nodes:
            yield node

    def __del__(self):
        """
        Descructor
        """
        LOGGER.debug("Entity deleted")

    @instancecheck
    def __lt__(self, other):
        return self.position < other.position

    @instancecheck
    def __le__(self, other):
        return self.position <= other.position

    @instancecheck
    def __gt__(self, other):
        return self.position > other.position

    @instancecheck
    def __ge__(self, other):
        return self.position >= other.position

    @instancecheck
    def __eq__(self, other):
        return self.position == other.position

    @instancecheck
    def __ne__(self, other):
        return self.position != other.position

    @instancecheck
    def __cmp__(self, other):
        if self > other:
            return 1
        if self == other:
            return 0
        if self < other:
            return -1

    def __setitem__(self, key, value):
        """
        Stores tag for entity object

        @param key: Key of tag
        @param value: Value to store
        """
        self._tags[key] = value

    def __getitem__(self, key):
        """
        Gets entity's tag value

        @param key: Key for value to get
        """
        return self._tags[key]

    def add_node(self, node):
        """
        Adds node to the entity
        @param node: Tupple of (x, y)
        """
        if not node in self.nodes:
            self.nodes.append(node)

    @property
    def position(self):
        """
        Computes smallest node in entity in case it's not computed yet.
        This smallest node is returned.
        @return: Smallest node in entity
        """
        if self._position is None:
            self._position = min(self.nodes)

        return self._position

    def _has_same_vertex_start_end(self):
        """
        Removes last vertex from if it's the same vertex as the first one
        """
        return self.nodes[0] == self.nodes[-1]

    def create_polygon(self):
        """
        Creates polygon from nodes
        """
        polygon = Polygon([(point.x, point.y)
                           for point in self.nodes])
        self.polygon = order_polygon(polygon)

    def transform_nodes_to_vertices(self):
        """
        Transforms nodes from Vector2 object to Vertex objects
        """
        new_nodes = list()
        if self.polygon is None:
            self.create_polygon()
        if len(self.polygon) != 1:
            # Polygon should have only one contour
            LOGGER.warn("Polygon has more than one contour")
        for point in self.polygon[0]:
            new_nodes.append(Vertex(point[0], 0.0, point[1]))
        self.nodes = new_nodes


class Building(Entity):
    """
    Class for Building types
    """

    def __init__(self, id, tags=dict()):
        super(Building, self).__init__(id, tags)
        self.building_class = BuildingClasses.NOTHING
        self.has_features = False
        self.neighbours = set()
        self.n_walls = list()
        self.entrance = None
        self.generated_in = -1

    def create_features(self):
        """
        Creates feature vector if the entity is a building
        """
        if 'building' not in self._tags:
            LOGGER.warn("Entity on position %sis not a building (or doesn't "
                        "have the tag at least), skipping...", self.position)
            return
        self.has_features = True
        if self.polygon is None:
            self.create_polygon()
        self.features["verts"] = len(self.nodes)
        self.features["area"] = self.polygon.area()

        perimeter, max_angle, min_angle, avg_angle, median_side, max_side = \
            geometrfunc.get_perimeter_and_angles(self.nodes)

        self.features["aproxim_area"] = \
            geometrfunc.get_area(graham_scan(self.nodes))
        self.features["perimeter"] = perimeter
        self.features["max_angle"] = max_angle
#        self.features["min_angle"] = min_angle
        self.features["avg_angle"] = avg_angle
        self.features["median_side"] = median_side
        self.features["max_side"] = max_side

    def feature_string(self):
        """
        Creates string containing feature of given building
        """
        keys = self.features.keys()
        keys.sort()
        features = "1:%f" % self.features[keys[0]]
        for num, key in enumerate(keys[1:]):
            features += " %d:%f" % (num + 2, self.features[key])

        return features

    def feature_list(self):
        """
        Creates list containing each feauter on its position
        """
        keys = self.features.keys()
        keys.sort()
        f_list = list()
        for key in keys:
            f_list.append(self.features[key])

        return f_list

    def platform_mesh(self):
        """
        Generates platform mesh with blue color
        """
        platform = generate_polygon_mesh(self.polygon)
        self.meshes.append(platform)


class MetaWay(type):
    """
    Metaclass that loads class attributes for way from config file
    """
    def __new__(meta, name, bases, dct):
        """
        Loads settings for way from config file
        """
        cls = type.__new__(meta, name, bases, dct)
        cls._default_width = 0
        cls._widths = dict()
        for key, value in config_parser.items('ways'):
            cls._widths[key] = float(value)
        return cls


class Way(Entity):
    """
    Class of way
    """
    __metaclass__ = MetaWay

    def __init__(self, id_, tags=dict()):
        """
        Constructor
        """
        super(Way, self).__init__(id_, tags=tags)
        self.width = self._widths.get(self._tags['highway'],
                                      self._default_width)
        self.render = (self.width != 0)
        self.way_rectangles = list()

    def create_polygon(self):
        """
        Creates polygon
        """
        last_node = self.nodes[0]
        circle = Shapes.Circle(radius=self.width, center=(last_node.x,
                                                          last_node.y))
        self.polygon = circle
        for node in self.nodes[1:]:
            polygon = rotated_rectangle(last_node, node, self.width)
            self.way_rectangles.append(polygon)
            last_node = node
            circle = Shapes.Circle(radius=self.width, center=(node.x, node.y))
            self.polygon += circle + polygon


class Ways(Entity):
    """
    Collection of Way objects, computes junctions
    """
    def __init__(self, *args, **kwargs):
        """
        Initializes list and huge polygon
        """
        super(Ways, self).__init__(*args, **kwargs)
        self.ways = list()
        self.polygon = Polygon()

    def append(self, item):
        """
        Appends item to ways list
        """
        self.ways.append(item)

    def __len__(self):
        """
        Returns length of ways list
        """
        return self.ways.__len__()

    def create_way(self):
        """
        Creates triangles of all polygons
        """
        road_texture = Generator.textures.get_index('road')
        LOGGER.info("Creating roads")
        for way in self.ways:
            if way.polygon is None:
                way.create_polygon()
            self.polygon += way.polygon
        for way in self.ways:
            for rectangle in way.way_rectangles:
                self.polygon -= rectangle
                mesh = generate_polygon_mesh(rectangle)
                mesh.texture = road_texture
                self.meshes.append(mesh)
        if self.polygon:
            way_mesh = generate_polygon_mesh(self.polygon)
            way_mesh.texture = road_texture
            self.meshes.append(way_mesh)
        LOGGER.info("Roads created")

    def __getitem__(self, index):
        """
        Gets way on given index

        @param index: Index of way
        """
        return self.ways[index]

    def __iter__(self):
        """
        Yields ways
        """
        for way in self.ways:
            yield way
