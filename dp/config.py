"""
Reads and provides configuration
"""
from ConfigParser import ConfigParser
import json
import logging
import os

from dp.classifier.classifier import BuildingClasses

CONFIG_PATH = os.path.join(os.path.dirname(__file__), "conf/settings.ini")
LOGGER = logging.getLogger("dpLogger")


class JsonConfigParser(ConfigParser):
    """
    Extended config parser with json parsing capability
    """
    def get_json(self, section, option, *args, **kwargs):
        """
        Returns JSON parsed value of section

        @param section: Section name in config
        @param option: Key of desired value
        """
        return json.loads(self.get(section, option, *args, **kwargs))

    def get_building_specs(self, building_class):
        """
        Returns dictionary with configuration for given building class

        @param building_class: Class of the building
        """
        ret_dict = dict()
        section = BuildingClasses.CLASS_TO_STRING[building_class]
        for key in self.options(section):
            ret_dict[key] = self.get(section, key)
        return ret_dict

config_parser = JsonConfigParser()
config_parser.read(CONFIG_PATH)
LOGGER.info("Settings read from %s", CONFIG_PATH)
