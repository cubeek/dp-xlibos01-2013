"""
This module parses OSM XML data and returns them in more
workable format.
"""

from lxml import etree
import logging
import re

import dp.common as common
from dp.entities import Building, Entity, Way


LOGGER = logging.getLogger("dpLogger")


class NotVisibleException(Exception):
    pass


class Parser(object):
    """
    Parser class

    Based on XML in string provides functionality for obtaining objects from
    this document. Objects can be ways and buildings
    """
    root = None
    NODE_TAG = 'node'
    ND_TAG = 'nd'
    WAY_TAG = 'way'
    TAG_TAG = 'tag'
    RELATION_TAG = 'relation'
    MEMBER_TAG = 'member'
    
    def __init__(self, xmlstring):
        """
        Sets the root node and boundries

        @param xmlstring: The XML file in huge string
        """
        self._get_root_from_string(xmlstring)

        bounds_node = self.root.xpath('/osm/bounds')[0]

        try:
            self.minlat = float(bounds_node.get('minlat'))
            self.maxlat = float(bounds_node.get('maxlat'))
            self.minlon = float(bounds_node.get('minlon'))
            self.maxlon = float(bounds_node.get('maxlon'))
        except ValueError as value_error:
            LOGGER.fatal("Can't get correct type: %s", value_error)
            raise value_error
        LOGGER.debug("Parser created")

    @property
    def bounds(self):
        """
        Property returning tuple containing bounds returned in XML (may vary
        from user's input!)
        """
        return (self.minlon, self.minlat, self.maxlon, self.maxlat)

    def __iter__(self):
        """
        Generator over all ways in the document
        """
        way_tags = self.root.xpath('/osm/way[@visible="true"]')
        for way in way_tags:
            entity = self._create_element(way)
            yield entity

    def _validate_document(self, xsd_doc_path=common.API_XSD_PATH):
        """
        Validates XML document against XSD

        @param xsd_doc_path: path to XSD document
        """
        LOGGER.debug("Validating parser against XSD in %s",
                     xsd_doc_path)
        xsd_root = etree.parse(xsd_doc_path)
        return self.root.getroottree().xmlschema(xsd_root)

    def _get_root_from_string(self, xmlstring):
        """
        Creates root DOM object from string, stores to self.root

        @param xmlstring: String containing the whole XML document
        """
        reenc = re.compile(r"encoding=\"(?P<enc>[^\"]*)\"")
        encoding_line = xmlstring.split('\n', 1)[0]
        match = reenc.search(encoding_line)
        if match is None:
            LOGGER.warning("Cannot find encoding in the XML file, using "
                           "UTF-8 as default")
            encoding = "UTF-8"
        else:
            encoding = match.group('enc')

        parser = etree.XMLParser(encoding=encoding)
        LOGGER.debug("Creating dom from xml string")
        self.root = etree.fromstring(xmlstring, parser)
        if not self._validate_document():
            LOGGER.warning("XML document is not valid against %s",
                           common.API_XSD_PATH)

    def _create_element(self, way):
        """
        Creates element from way tag

        @params way: <way> tag
        """
        tags = self._get_tags_dict(way)
        ent_id = int(way.get('id'))
        if 'highway' in tags:
            ent = Way(ent_id, tags)
        elif 'building' in tags:
            ent = Building(ent_id, tags)
        else:
            ent = Entity(ent_id, tags)
            return ent
        for node_tag in way.iter(tag=self.ND_TAG):
            try:
                node = self._get_node(node_tag.get('ref'))
                ent.add_node(node)
            except NotVisibleException as not_visible_ex:
                LOGGER.info(not_visible_ex)
        return ent

    def _get_tags_dict(self, way):
        """
        Examines the type of the way
        """
        tags = dict()
        for tag in way.iter(tag=self.TAG_TAG):
            tags[tag.get('k')] = tag.get('v')
        return tags

    def _get_node(self, node_id):
        """
        Returns node of given id

        @param node_id: ID of the node to obtain
        @return (latitude, longitude)
        """
        nodes = self.root.xpath('/osm/node[@id="%s"]' % node_id)
        if len(nodes) > 1:
            LOGGER.warning("Multiple occurence of node %s in document",
                           node_id)
            for node in nodes:
                LOGGER.warning(etree.tostring(node, pretty_print=True))
        node = nodes[0]
        if node.get('visible') != 'true':
            raise NotVisibleException('Node %d is not visible, skipping' %
                                      node_id)
        lat = float(node.get('lat'))
        lon = float(node.get('lon'))
        if lat < self.minlat:
            self.minlat = lat
        if lat > self.maxlat:
            self.maxlat = lat
        if lon < self.minlon:
            self.minlon = lon
        if lon > self.maxlon:
            self.maxlon = lon
        return (lat, lon)

    def get_xml_gps_position(self):
        """
        Returns GPS coordinates of bounding box
        """
        bounds_node = self.root.xpath('/osm/bounds')[0]
        left = float(bounds_node.get('minlon'))
        bottom = float(bounds_node.get('minlat'))
        right = float(bounds_node.get('maxlon'))
        top = float(bounds_node.get('maxlat'))

        return (left, bottom, right, top)
