"""
Provides geometric functions
"""

import logging
import math

from dp import common
from dp.computations.ageometr import Vector2

LOGGER = logging.getLogger('dpLogger')


def get_center(first, second):
    """
    @param left: First point
    @param right: Second point
    @return: Point in the middle of left and right
    """
    return (float(first) + float(second))/2.0


def swap(point):
    """
    Swaps coordination of point p

    @param point: Tuple (x, y) representing point
    """
    return (point[1], point[0])


def get_min_x(points):
    """
    Return point with minimal Y coordinate, removes it from the list

    @param points: List of points (tuples (x, y))
    """
    return min(points)


def get_max_x(points):
    """
    Return point with minimal Y coordinate, removes it from the list

    @param points: List of points (tuples (x, y))
    """
    return max(points)


def get_min_y(points):
    """
    Return point with minimal Y coordinate, removes it from the list

    @param points: List of points (tuples (x, y))
    """
    return min(points, key=lambda x: (x[1], x[0]))


def get_max_y(points):
    """
    Return point with minimal Y coordinate, removes it from the list

    @param points: List of points (tuples (x, y))
    """
    return max(points, key=lambda x: (x[1], x[0]))


def get_triangle_left(a, b, c):
    """
    Returns ordered points in order that creates triangle with right direction

    @param a: Point A
    @param b: Point B
    @param c: Point C
    """
    if is_non_left_turn(a, b, c):
        return a, b, c
    return a, c, b


def get_triangle_left(a, b, c):
    """
    Returns ordered points in order that creates triangle with left direction

    @param a: Point A
    @param b: Point B
    @param c: Point C
    """
    if is_non_left_turn(a, b, c):
        return a, c, b
    return a, b, c


def is_non_left_turn(a, b, c):
    """
    Determines whether a, b, c creates a right turn

    @param a: Point A
    @param b: Point B
    @param c: Point C
    @return: True if turn is straight or right
    """
    return (b[0] - a[0]) * (c[1] - a[1]) - (b[1] - a[1]) * (c[0] - a[0]) <= 0


def get_angle(a, b, c):
    """
    Returns angle in radians of vectors BA and BC

    @param a: Point A
    @param b: Point B
    @param c: Point C
    @return: Angle at point B between points A, B and C
    """
    # Vector BA
    vec_a = (a[0] - b[0], a[1] - b[1])

    #Vector BC
    vec_b = (c[0] - b[0], c[1] - b[1])

    argument =  (vec_a[0] * vec_b[0] + vec_a[1] * vec_b[1]) / (math.sqrt(
                    vec_a[0] * vec_a[0] + vec_a[1] * vec_a[1]) * math.sqrt(
                        vec_b[0] * vec_b[0] + vec_b[1] * vec_b[1]
                        )
                    )
    if argument > 1.0:
        argument = 1.0
    elif argument < -1.0:
        argument = -1.0
    # Compute angle between vec_a and vec_b
    angle = math.acos(argument)
    return angle


def get_distance(a, b):
    """
    Computes distance between two points

    @param a: Tuple (x,y) of first point
    @param b: Tuple (x,y) of second point
    @return: distance between points A and B
    """
    if isinstance(a, Vector2) and isinstance(b, Vector2):
        return math.sqrt((a.x - b.x)**2 + (a.y - b.y)**2)
    else:
        return math.sqrt((a[0]-b[0])**2+(a[1]-b[1])**2)


def sphere_distance(first, second, in_degrees=False):
    """
    Computes distance between two points on the sphere

    @param first: Point A (tuple)
    @param second: Point B (tuple)
    @param in_degrees: Specifies whether points are given in degrees 
                      (True) or in radians (False)
    @return: Distance between points A and B
    """
    a_old = None
    b_old = None

    if (in_degrees):
        a_old = first
        b_old = second
        first = tuple([math.radians(point) for point in a_old])
        second = tuple([math.radians(point) for point in b_old])

    return math.acos(math.sin(first[0])*math.sin(second[0])+
                     math.cos(first[0])*math.cos(second[0])*
                     math.cos(second[1]-first[1]))*common.EARTH_R


def get_perimeter_and_angles(points):
    """
    Computes perimeter of the given points

    @param points: List of Vector2 objects
    @return: Perimeter
    """
    perimeter = 0.0
    points = [(point.x, point.y) for point in points[:-1]]
    distances = []
    max_angle = 0.0
    min_angle = full_angle = 2 * math.pi
    anglesum = 0.0
    length = len(points)

    for i in range(length):
        point = points[i]
        prev = points[(i - 1) % length]
        next_ = points[(i + 1) % length]
        distance = get_distance(prev, point)
        distances.append(distance)
        perimeter += distance
        if is_non_left_turn(prev, point, next_):
            angle = full_angle - get_angle(prev, point, next_)
        else:
            angle = get_angle(prev, point, next_)

        if angle > max_angle:
            max_angle = angle
        if angle < min_angle:
            min_angle = angle
        anglesum += angle

    distances.sort()
    median = distances[len(distances) / 2]

    return (perimeter, max_angle, min_angle, anglesum/length, median,
            distances[-1])


def get_area(points):
    """
    Computes area of convex polygon

    @param points: List of points of convex polygon. Point is tuple (x,y)
    @return: Area of convex polygon
    """
    sum_ = 0.0
    point_num = len(points)
    for i in xrange(point_num):
        next_ = i + 1
        if next_ == point_num:
            next_ = 0
        sum_ += \
            points[i][0] * points[next_][1] - points[next_][0] * points[i][1]
    return sum_/2.0


def get_standard_deviation(near_entities, feature_keys):
    """
    Computes standard deviation of feature vector of near entities of entity.

    @param near_entities: List of entities
    @param feature_keys: List of feautre keys
    @return: Dictionary representing feature vector with standard deviation
             for each key
    """
    avg_features = {}
    s_deviation = {}
    length = len(near_entities)

    for key in feature_keys:
        #eliminating nearby features
        if not key.startswith("nearby_"):
            avg_features[key] = 0.0
    for entity in near_entities:
        for key in avg_features:
            avg_features[key] += entity.features[key]

    for key in avg_features:
        sum_ = 0.0
        avg_features[key] = avg_features[key] / length if length != 0 else 0
        for e in near_entities:
            sum_ += (e.features[key]-avg_features[key]) ** 2
        s_deviation["nearby_" + key] = math.sqrt(sum_ / length)\
                                       if length != 0 else 0

    return s_deviation


def minimum_area_rectangle(points):
    """
    Creates a minimal area rectangle of given CONVEX polygon

    @param points: List of points - [ (x,y), ... ]
    @return Tupple of 4 points creating minimal area rectangle ( (x1, y1), (x2, y2), (x3, y3), (x4, y4) )
    """
    x_min = get_min_x(points)
    x_max = get_max_x(points)
    y_min = get_min_y(points)
    y_max = get_max_y(points)

    return (x_min, y_min, x_max, y_max)


def orientation(p, q, r):
    '''Return positive if p-q-r are clockwise, neg if ccw, zero if colinear.'''
    return (q[1]-p[1])*(r[0]-p[0]) - (q[0]-p[0])*(r[1]-p[1])


def hulls(points):
    '''
    Graham scan to find upper and lower convex hulls of a set of 2d points.
    '''
    upper = []
    lower = []
    points.sort()
    for point in points:
        while len(upper) > 1 and orientation(upper[-2], upper[-1], point) <= 0:
            upper.pop()
        while len(lower) > 1 and orientation(lower[-2], lower[-1], point) >= 0:
            lower.pop()
        upper.append(point)
        lower.append(point)
    return upper, lower


def rotating_calipers(points):
    '''Given a list of 2d points, finds all ways of sandwiching the points
between two parallel lines that touch one point each, and yields the sequence
of pairs of points touched by each pair of lines.'''
    upper, lower = hulls(points)
    i = 0
    j = len(lower) - 1
    while i < len(upper)-1 or j > 0:
        yield upper[i], lower[j]

        # if all the way through one side of hull, advance the other side
        if i == len(upper)-1:
            j -= 1
        elif j == 0:
            i += 1

        # still points left on both lists, compare slopes of next hull edges
        # being careful to avoid divide-by-zero in slope calculation
        elif (upper[i+1][1]-upper[i][1])*(lower[j][0]-lower[j-1][0]) > \
                (lower[j][1]-lower[j-1][1])*(upper[i+1][0]-upper[i][0]):
            i += 1
        else: j -= 1

if __name__ == "__main__":
    a_list = [(2, 9), (-1, 8), (-2, 6), (2, 5), (6, 7), (3, 0), (-1, 4),
              (7, 1), (8, 4), (4, 8), (-1, 1), (1, 7), (1, 2)]
#    print a
#    convexhull = graham_scan(a)
#    print get_area(convexhull)
    b_list = [(0, 0), (3, 0), (3, 3), (0, 3), (1, 1), (1, 2), (2, 2), (2, 1),
              (3, 2), (3, 1), (1, 3)]
#    print b
#    convexhull = graham_scan(b)
#    print get_area(convexhull)
    print a_list
    calips = rotating_calipers(a_list)
    for calip in calips:
        print calip



