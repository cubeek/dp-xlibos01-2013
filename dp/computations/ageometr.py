"""
Module implementing vector class
"""

import logging
import numpy
import math
from Polygon import Polygon
from Polygon.IO import writeSVG

LOGGER = logging.getLogger("dpLogger")
EPS = 0.001


class Plane(object):
    """
    Plain in 3D
    """

    def __init__(self, first, second, third):
        """
        Constructor of plain in 3D given by 3 points

        @param first: First point
        @param second: Second point
        @param third: Third point
        """
        self.normal = ((second - first) * (third - first)).normalize()
        self.offset = self.normal.x * first.x + self.normal.y * first.y + \
            self.normal.z * first.z

    def get_y(self, x, z):
        """
        Returns Y coordinate in the plane
        """
        left = [list(self), list(PLANE_YZ), list(PLANE_XY)]
        right = [self.offset, x, z]
        return numpy.linalg.solve(left, right)[1]

    def __repr__(self):
        """
        String representation
        """
        return "%.3f*x + %.3f*y + %.4f*z + %.3f = 0" % (self.normal.x,
                                                        self.normal.y,
                                                        self.normal.z,
                                                        self.offset)

    def __iter__(self):
        """
        """
        for coord in self.normal:
            yield coord


class Line(object):
    """
    Class representing line in analytic geometry
    """

    def __init__(self, origin, direction):
        """
        Creates basic line equation for analytic geometry

        @param origin: Vector to origin of edge (First point)
        @param direction: Vector of edge direction (Second point)
        """
        dir_vec = direction - origin
        self.normal = dir_vec.ort().normalize()
        self.offset = self.normal.x * origin.x + self.normal.y * origin.y

    def get_intersect(self, other, offset=None):
        """
        Computes intersect of two lines
        """
        if offset is None:
            offset = self.offset
        left = [list(self), list(other)]
        right = [offset, other.offset]
        try:
            intersect = (numpy.linalg.solve(left, right))
        except numpy.linalg.LinAlgError as liae:
            LOGGER.warn("Cannot solve equations:\n\t%s\t%s\nGot error: "
                        "%s\nOffset of first equation was %f" %
                        (self, other, liae, offset))
            return None
        return Vector2(intersect[0], intersect[1])

    def __iter__(self):
        """
        Iterates over normal
        """
        for coord in self.normal:
            yield coord

    def __repr__(self):
        """
        Prints equation
        """
        return "%.3f*x + %.3f*y + %.3f = 0" % (self.normal.x, self.normal.y,
                                               self.offset)


    __str__ = __repr__


    def has_point(self, point):
        """
        Return true if point lays on this line
        """
        return self.normal.x * point.x + self.normal.y * point.y \
                - self.offset == 0.0


class Line3(object):
    """
    Representing line in 3D
    """

    def __init__(self, origin, direction):
        """
        Creates basic line equation for analytic geometry

        @param origin: Vector to origin of edge (First point)
        @param direction: Vector of edge direction (Second point)
        """
        self.origin = origin
        self.direction = direction

    def has_point(self, point):
        """
        Decides whether point is on this line
        """
        if point == self.origin or point == self.direction:
            return True
        vec1 = (point - self.origin).normalize()
        vec2 = (point - self.direction).normalize()
        return vec1 == vec2 or vec1.invert() == vec2

    def __repr__(self):
        """
        Prints two points
        """
        return "%s   ---   %s" %(self.origin, self.direction)

    __str__ = __repr__


class Vector3(object):
    """
    3D vector
    """

    def __init__(self, x=0.0, y=0.0, z=0.0):
        """
        Constructor
        """
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self):
        """
        Representation
        """
        output = "(%f, %f, %f)" % (self.x, self.y, self.z)
        return output

    __str__ = __repr__

    def __add__(self, other):
        """
        Vector addition
        """
        new_vec = Vector3()
        new_vec.x = self.x + other.x
        new_vec.y = self.y + other.y
        new_vec.z = self.z + other.z
        return new_vec

    def __sub__(self, other):
        """
        Vector substraction
        """
        new_vec = Vector3()
        new_vec.x = self.x - other.x
        new_vec.y = self.y - other.y
        new_vec.z = self.z - other.z
        return new_vec

    def __ne__(self, other):
        """
        True if self != other

        @param other: Other Vector3 object
        """
        return not (self == other)

    def __eq__(self, other):
        """
        If has equal coordinates

        @param other: Other Vector3 object
        """
        x_diff = abs(self.x - other.x)
        y_diff = abs(self.y - other.y)
        z_diff = abs(self.z - other.z)
        return x_diff < EPS and y_diff < EPS and z_diff < EPS

    def __mul__(self, other):
        """
        Vector or scalar product
        """
        new_vec = Vector3()
        if isinstance(other, Vector3):
            new_vec.x = self.y * other.z - self.z * other.y
            new_vec.y = self.z * other.x - self.x * other.z
            new_vec.z = self.x * other.y - self.y * other.x
        else:
            new_vec.x = self.x * other
            new_vec.y = self.y * other
            new_vec.z = self.z * other
        return new_vec

    def __div__(self, scalar):
        """
        Divide each coordination of vector by scalar
        """
        scalar = float(scalar)
        new_vec = Vector3()
        new_vec.x = self.x / scalar
        new_vec.y = self.y / scalar
        new_vec.z = self.z / scalar

        return new_vec

    def __getitem__(self, index):
        """
        Hack for compatibility with tuple computing in 2D
        """
        if index == 0:
            return self.x
        elif index == 1:
            return self.z
        else:
            raise IndexError("Out of range: %d", index)

    def invert(self):
        """
        Makes vector with opposite direction
        """
        return self * (-1)

    def normalize(self):
        """
        Normalize vector
        """
        norm = self.length
        return Vector3(self.x / norm, self.y / norm, self.z / norm)

    def direction(self, vector, normalize=True):
        """
        Computes direction vector from this vector to another vector

        @param vector: Another vector
        @return: Vector that's directing from this vector to another
        """
        vec = vector - self
        if normalize:
            vec = vec.normalize()
        return vec

    def __iter__(self):
        """
        Iterates over coordinations
        """
        yield self.x
        yield self.y
        yield self.z

    @property
    def length(self):
        """
        Computes lenght of this vector
        """
        return math.sqrt(self.x * self.x + self.y * self.y + self.z * self.z)


class Vector2(object):
    """
    2D vector
    """

    def __init__(self, x=0.0, y=0.0):
        """
        Constructor
        """
        self.x = x
        self.y = y

    def __repr__(self):
        """
        Representation
        """
        output = "(%f, %f)" % (self.x, self.y)
        return output

    __str__ = __repr__

    def __add__(self, other):
        """
        Vector addition
        """
        new_vec = Vector2()
        new_vec.x = self.x + other.x
        new_vec.y = self.y + other.y
        return new_vec

    def __sub__(self, other):
        """
        Vector substraction
        """
        new_vec = Vector2()
        new_vec.x = self.x - other.x
        new_vec.y = self.y - other.y
        return new_vec

    def __mul__(self, other):
        """
        Scalar product
        """
        if isinstance(other, Vector2):
            return self.x * other.x + self.y * other.y
        else:
            new_vec = Vector2()
            new_vec.x = self.x * other
            new_vec.y = self.y * other
            return new_vec

    def __eq__(self, other):
        """
        If has equal coordinates
        """
        x_diff = abs(self.x - other.x)
        y_diff = abs(self.y - other.y)
        return x_diff < EPS and y_diff < EPS

    def __getitem__(self, key):
        """
        Index supporting, for compatibility with tuples
        """
        if key == 0:
            return self.x
        if key == 1:
            return self.y
        raise IndexError("Vector2 has only 2 items, item %d is unknown", key)

    def normalize(self):
        """
        Normalize vector
        """
        norm = math.sqrt(self.x ** 2 + self.y ** 2)
        return Vector2(self.x / norm, self.y / norm)

    def ort(self):
        """
        Computes ortogonal vector
        """
        return Vector2(self.y, -1 * self.x)

    def invert(self):
        """
        Computes opposite vector
        """
        return Vector2(-1 * self.x, -1 * self.y)

    def direction(self, node2, normalize=True):
        """
        Computes direction vector between last point in way and point node

        @param node2: Point (Vector2) towards will be direction computed
        @return direction vector (Vector2)
        """
        vec = node2 - self
        if normalize:
            vec = vec.normalize()
        return vec

    @property
    def length(self):
        """
        Computes lenght of this vector
        """
        return math.sqrt(self.x * self.x + self.y * self.y)

    def angle(self, other):
        """
        Gets angle between self and other vector
        """
        arg = self * other / (self.length * other.length)
        if arg > 1.0:
            arg = 1.0
        elif arg < -1.0:
            arg = -1.0
        return math.acos(arg)

    def __iter__(self):
        """
        Iterates over coordinations
        """
        yield self.x
        yield self.y


PLANE_XY = Plane(Vector3(0, 0, 0), Vector3(1, 0, 0), Vector3(0, 1, 0))
PLANE_YZ = Plane(Vector3(0, 0, 0), Vector3(0, 1, 0), Vector3(0, 0, 1))
PLANE_XZ = Plane(Vector3(0, 0, 0), Vector3(1, 0, 0), Vector3(0, 0, -1))


def sign(vec1, vec2, vec3):
    """
    Algorithm for decision whether
http://www.gamedev.net/topic/295943-is-this-a-better-point-in-triangle-test-2d/


    @param vec1: Point of triangle
    @param vec2: Point of triangle
    @param vec3: Point of triangle
    """

    if isinstance(vec1, Vector3):
        return (vec1.x - vec3.x) * (vec2.z - vec3.z) - \
               (vec2.x - vec3.x) * (vec1.z - vec3.z)
    elif isinstance(vec1, Vector2):
        return (vec1.x - vec3.x) * (vec2.y - vec3.y) - \
               (vec2.x - vec3.x) * (vec1.y - vec3.y)


def contains_point(poi, vec1, vec2, vec3):
    """
    Decides whether point poi is between vectors vec1, vec2 and vec3, which
    make triangle
    """
    b1 = sign(poi, vec1, vec2) < 0.0
    b2 = sign(poi, vec2, vec3) < 0.0
    b3 = sign(poi, vec3, vec1) < 0.0

    return ((b1 == b2) and (b2 == b3))


def fits_interval(vec, start, end):
    """
    Checks, if vector vec fits into interval given by start and end

    @param vec: Vector pointing to the point we want to check
    @param start: Vector pointing to the start of interval
    @param end: VEctor pointing to the end of interval
    """
    return (start.x < vec.x < end.x) and (start.y < vec.y < end.y)


class RectangleLine(object):
    """
    Testing purposes, draws line
    """
    width = 0.01

    def __init__(self, start, end):
        """
        start end are Vector2
        """
        direction = (end - start).normalize()
        ortogonal = direction.ort() * self.width
        ortogonal_opposite = ortogonal.invert()
        lu = end + ortogonal
        lb = end + ortogonal_opposite
        ru = start + ortogonal
        rb = start + ortogonal_opposite
        self.polygon = Polygon([tuple(lu), tuple(lb), tuple(rb), tuple(ru)])

    def draw(self, filename):
        writeSVG("%s.svg" % filename, self.polygon)


def get_distance(a, b, c):
    """
    Computes distance of point C from stroke A, B

    @param a: Point A of stroke AB
    @param b: Point B of stroke AB
    @param c: Point C
    """
    pi_h = math.pi / 2

    vec_ab = b - a
    vec_ba = a - b
    vec_bc = c - b
    vec_ac = c - a
    if vec_ba.angle(vec_bc) >= pi_h:
        return vec_bc.length
    angle_ab_ac = vec_ab.angle(vec_ac)
    length = vec_ac.length
    if angle_ab_ac >= pi_h:
        return vec_ac.length
    return math.sin(angle_ab_ac) * length


def get_x_stroke_coordinate(stroke, y):
    """
    Gets X coordinate on given stroke

    @param stroke: Tuple of (start, end, line), where
                        - start is starting coordinate of the stroke (Vector2)
                        - end is the end coordinate of the stroke (Vector2)
                        - stroke is Line object
    @param y: Y-coordinate
    """
    start, end, line = stroke
    if start.y > end.y:
        start, end = end, start
    if start.y > y or end.y < y or line.normal.x == 0:
        return None
    return (-1) * (line.normal.y * y + line.offset) / line.normal.x


def get_y_stroke_coordinate(stroke, x):
    """
    Gets Y coordinate on given stroke

    @param stroke: Tuple of (start, end, line), where
                        - start is starting coordinate of the stroke (Vector2)
                        - end is the end coordinate of the stroke (Vector2)
                        - stroke is Line object
    @param y: X-coordinate
    """
    start, end, line = stroke
    if start.x > end.x:
        start, end = end, start
    if start.x > x or end.x < x or line.normal.y == 0:
        return None
    return (-1) * (line.normal.x * x + line.offset) / line.normal.y


def draw_polygon(vertices, indices, filename):
    """
    Helping function for drawing polygons to files
    """
    poly = Polygon()
    for i in range(0, len(indices), 3):
        i1 = indices[i]
        i2 = indices[i+1]
        i3 = indices[i+2]
        poly.addContour([vertices[i1], vertices[i2], vertices[i3]])
    if poly.nPoints() > 0:
        writeSVG("%s.svg" % filename, poly)


if __name__ == "__main__":
    start = Vector2(0, 0)
    end = Vector2(2, 2)
    direction = (start - end).normalize()
    stroke = (end, start, Line(end, direction))
    assert get_x_stroke_coordinate(stroke, 0) == 0
    assert get_y_stroke_coordinate(stroke, 0) == 0
    assert get_x_stroke_coordinate(stroke, 3) == None
    assert get_y_stroke_coordinate(stroke, 3) == None
    assert get_x_stroke_coordinate(stroke, 1) == 1
    assert get_y_stroke_coordinate(stroke, 1) == 1
