"""
Module for computing convex hull of polygon
"""
from dp.computations.geometrfunc import get_angle, get_area,  get_min_y,\
                                        is_non_left_turn


def graham_scan(points):
    """
    Computes the graham scan of polygon given by points

    @param points: List of points
    """
    list_cpy = points[:]
    p0 = get_min_y(list_cpy)
    list_cpy.remove(p0)
    other_points = _sort_angle(p0, list_cpy)

    stack = [p0, other_points[0], other_points[1]]
    for p in other_points[2:]:
        while (is_non_left_turn(stack[-2], stack[-1], p)):
            stack.pop()
        stack.append(p)
    return stack


def _sort_angle(point, points):
    """
    Sorts points according its' angles against point `point'

    @params point: Reference point for angle measuring
    @params points: Points to sort
    """
    aux_point = (point[0] + 1.0, point[1])

    def qsort(list_):
        """
        Quick-sort algorithm

        @param list_: List of points (tuples (x, y))
        """
        if list_ == []:
            return []
        else:
            mid = list_[len(list_) / 2]
            list_.remove(mid)
            mid_angle = get_angle(aux_point, point, mid)
            less = [x_point for x_point in list_
                    if get_angle(aux_point, point, x_point) < mid_angle]
            greater = [x_point for x_point in list_
                       if get_angle(aux_point, point, x_point) >= mid_angle]
            return qsort(less) + [mid] + qsort(greater)
    return qsort(points)


if __name__ == "__main__":
    a = [(2, 9), (-1, 8), (-2, 6), (2, 5), (6, 7), (3, 0), (-1, 4), (7, 1),
         (8, 4), (4, 8), (-1, 1), (1, 7), (1, 2)]
    print "Obtained a: ", a
    convexhull = graham_scan(a)
    print "Area of A: ", get_area(convexhull)
    b = [(0, 0), (3, 0), (3, 3), (0, 3), (1, 1), (1, 2), (2, 2), (2, 1),
         (3, 2), (3, 1), (1, 3)]
    print "Obtained B: ", b
    convexhull = graham_scan(b)
    print "Area of B: ", get_area(convexhull)
