/***************************
 * Renderer
 *
 * Module contains stuctures for copying vertexes to graphic card
 *
 ***************************/

#ifndef __DP_RENDERER_H__
#define __DP_RENDERER_H__

#include <SDL.h>
#include <SDL_image.h>>
#ifdef __APPLE__
    #include <OpenGL/gl3.h>
#else
    #define GL_GLEXT_PROTOTYPES
    #include <GL/gl.h>
    #include <GL/glext.h>
    #include <math.h>
#endif
#define BUFFER_OFFSET(i) ((GLvoid*)(sizeof(GLuint)*i))
#define TRUE 1
#define FALSE 0

#define VERTEX_SHADER 0
#define FRAGMENT_SHADER 1

#define TRANSL_CHANGE 5.0

//#define DEBUG

/* ERROR CODES */
typedef enum { OK, E_SETATTR, E_SETVIDEO, E_GETVIDEO, E_INIT, E_OGLCONTEXT,
               E_MEM, E_FILE } RETERRORS;

/* MOVEMENT TYPES */
typedef enum { FORWARD, BACKWARD, LEFT, RIGHT } movement_type;

/* Serves also as indexes for color textures */
typedef enum { BOF, OLD, HOUSE, OTHER, NOTHING } TBuildingClass;
#define SELECTED 5

/* RENDERER MODES */
typedef enum { COLLECT, VIEW } TRendererMode;

/**************** VERTEX ******************/

/**
 * Structure of one vertex
 */
typedef struct _vertexStruct {
    // Vertex position
    GLfloat x, y, z;
    // Vertex normals
    GLfloat nx, ny, nz;
    // Texture coordinates
    GLfloat s0, t0;
} Vertex;

/**
 * Vector structure
 */
struct _vec {
    GLfloat x;
    GLfloat y;
    GLfloat z;
};

/***************** END VERTEX *****************/

/**************** OFFSETS STRUCTURES *********/

struct entity {
    long int offset;
    long int length;
    GLuint texture;
};

/**
 * Structure keeping offsets in VBO
 */
typedef struct entitiesstruct {
    struct entity *ent;
    long int entity_cnt;
    long int entity_allocated;
    long int last_entity;
} Entities;

/*************** END OFFSETS STRUCTURES *******/


/********************* CAMERA *****************/

/**
 * Camera structure
 */
typedef struct _camera {
    GLfloat pitch;
    GLfloat yaw;
    struct _vec pos;
} Camera;

/**
 * Assures pitch and yaw are in relevant values (-90;90 and 0;360)
 */
void lock_camera(Camera *);

/**
 *
 */
void move_camera(Camera *, float, movement_type);



/******************** END CAMERA ****************/


/************* RENDERER *********************/

typedef struct renderstruct {
    // Program
    GLuint program;
    // Block of shaders
    GLuint *shaders;

    // Array of vertices
    Vertex *vertices;
    // Number of vertices in the array above
    unsigned int vertex_cnt;
    // Allocated memory for vertices array
    unsigned int vertex_allocated;
    // Offset to last vertex that belongs to some entity
    GLuint vertex_offset;

    // Array of indices
    GLuint *indices;
    // Number of indices in the array above
    long int index_cnt;
    // Allocated memory for indices array
    long int index_allocated;
    // Array of building entities
    Entities buildings;
    // Classes of building for calssifier training
    int *buildings_classes;

    // Currently selected building
    long int selected_building;

    // Color of last selected object
    GLuint last_texture_color;

    // Array of ways
    Entities ways;

    // Heightmap and indexing of triangles to IBO
    Entities heightmap;

    // Glass entities
    Entities glasses;
    
    // Pointer to shader for model-view-projection matrix
    GLuint pMvpMatUniform;

    // Pointer to model-view matrix
    GLuint pMvMatUniform;

    // Pointer to normal matrix
    GLuint pMnMatUniform;

    // Pointer to light position
    GLuint pLightPositionUniform;

    // Pointer to unifrom ambient color in fragment shader
    GLuint pAmbientColorUniform;

    // Pointer to unifrom diffues color in fragment shader
    GLuint pDiffuseColorUniform;

    // Pointer to unifrom specular color in fragment shader
    GLuint pSpecularColorUniform;

    // Pointer to texture uniform
    GLuint pTexCoordsUniform;

    GLuint vao;
    GLuint vbo;
    GLuint ibo;
    GLint view_width;
    GLint view_height;
    float world_width;
    float world_height;

    GLenum polygon_mode;
    GLenum draw_mode;

    TRendererMode mode;
    // Position of the light in scene space
    GLfloat light_position[4];
    // Light colors
    GLfloat ambient_color[3];
    GLfloat diffuse_color[3];
    GLfloat spec_color[3];

    // Camera settings
    Camera camera;

    // Loaded textures
    GLuint *textures;
    // Number of loaded textures
    int texture_cnt;

    // Color textures
    GLuint c_textures[6];

    // Background color
    GLfloat clear_color[4];
} Renderer;


/**
 * Adds vertex to the entity
 * @param r: Pointer to entity
 * @param x: x coordination of vertex
 * @param y: y coordination of vertex
 * @param z: z coordination of vertex
 * @param nx: Vertex normal in x axis
 * @param ny: Vertex normal in y axis
 * @param nz: Vertex normal in z axis
 * @param s: Texture s coordinate
 * @param t: Texture t coordinate
 * @return: Position of new vertex in renderer, -1 on failure
 */
long add_vertex(Renderer *r, GLfloat x, GLfloat y, GLfloat z,
                GLfloat nx, GLfloat ny, GLfloat nz,
                GLfloat s, GLfloat t);


/**
 * Adds index to the entity
 * @param index: Index in vertices in entity
 * @return: Position of new index
 */
long add_index(Renderer *r, GLuint index);


/**
 * Allocates new renderer in the memory
 */
void create_renderer(Renderer **r);


/**
 * Copies data from renderer to VBO
 *
 * @param r: Pointer to renderer
 */
int copy_data_to_buffers(Renderer *r);

/**
 * Draws content of renderer using OpenGL
 *
 * @param r: Renderer structure
 */
void draw(Renderer *r);

/**
 * Initialize OpenGL Attributes in Renderer
 *
 * @param r: Renderer structure
 */
int init_renderer(Renderer *, char *, char *);

/**
 * Frees renderer from memory
 *
 * @param r: Pointer to renderer
 */
void cleanup(Renderer *r);

/**
 * Clears opengl settings and clears renderer
 */
void clear_renderer(Renderer *r);

/**
 * Allocates memory for texture array
 * @param r: Renderer
 * @param num: Number of textures that are gonna be loaded
 */
int allocate_texture_memory(Renderer *r, int num);

/**
 * Creates texture from file and puts it to its index in renderer
 * @param target: Pointer to first item of texture memory
 * @param texture_file: File that contains the texture
 * @param index: Index that texture will be placed to
 * @return: error code
 */
int load_texture(GLuint *target, const char *texture_file, int index);



/****************** END RENDERER **********/



/****************** OFFSETS FUNCTIONS *********************/

/**
 * adds new offset to offset structure
 *
 * @param Offsets *: Pointer to offset structure
 * @param index_cnt: Offset value in the VBO
 * @param offset: 
 */
int add_entity(Entities *entities, long int index_cnt, long int offset);

/**
 * Initialize offset structure
 *
 * @param o: Offset structure pointer
 */
void init_entities(Entities *entities);


/**
 * Clears offset structure from memory
 */
void clear_entities(Entities *entities);

/**
 * Adds building offset to renderer
 *
 * @param r: Renderer pointer
 * @param index_cnt: how many indexes will be added for given building
 * @param texture_index: Index of created texture in renderer that should be used for this element
 */
void add_building(Renderer *r, long int index_cnt, int texture_index);

/**
 * Adds glass offset to renderer
 *
 * @param r: Renderer pointer
 * @param index_cnt: how many indexes will be added for given building
 * @param texture_index: Index of created texture in renderer that should be used for this element
 */
void add_glass(Renderer *r, long int index_cnt, int texture_index);

/**
 * Adds building offset to renderer
 *
 * @param r: Renderer pointer
 * @param index_cnt: how many indexes will be added for given way
 */
void add_way(Renderer *r, long int index_cnt, int texture_index);

/**
 * Adds building offset to renderer
 *
 * @param r: Renderer pointer
 * @param index_cnt: how many indexes will be added for given heightmap
 * @param texture_index: Index of created texture in renderer that should be used for this element
 */
void add_heightmap(Renderer *r, long int index_cnt, int texture_index);

/****************** END OFFSETS *****************/



/************** SDL ********************/

/**
 * Event handler serves when window size is changed
 *
 * @param width: new width of window
 * @param height: new height of window
 */
void on_window_resized(int, int);

/**
 * Redraws scene and window
 */
void on_window_redraw();

/**
 * Setups SDL and creates window
 *
 * @param width: Width of the window in pixels
 * @param hegiht: Height of the window in pixels
 * @param window_title: Title of the window
 */
int sdl_setup(int width, int height, const char * window_title);

/**
 * Infinite loop for event handling
 */
void sdl_loop();

/**
 * Returns global renderer
 */
Renderer *get_renderer();

/**
 * Set mouse sensitivity
 *
 * @param sensitivity: New sensitivity of mouse
 */
void set_sensitivity(float);

/**
 * Set movement speed
 *
 * @param speed: New speed of movement
 */
void set_speed(float);


/********************* END SDL ******************/

#endif
