#version 330
precision highp float;

in vec3 vNormal;
in vec2 v_texcoord;
in vec3 vLightDir;

uniform vec4 ambientColor;
uniform vec4 diffuseColor;
uniform vec4 specularColor;
uniform sampler2D vTexCoords;

out vec4 FragColor;


void main(void) {
    float diff = max(0.0, dot(normalize(vNormal),
                              normalize(vLightDir)));

    vec4 lightning = diff * diffuseColor;

    // Add in ambient light 
    lightning += ambientColor;

    // Specular Light
    vec3 vReflection = normalize(reflect(-normalize(vLightDir),
                                          normalize(vNormal)));

    float spec = max(0.0, dot(normalize(vNormal), vReflection));

    // If the diffuse light is zero, don’t even bother with the pow function
    if(diff != 0) {
        float fSpec = pow(spec, 128.0);
        lightning.rgb += vec3(fSpec, fSpec, fSpec);
    }

    FragColor = texture(vTexCoords, v_texcoord) * lightning;
}
