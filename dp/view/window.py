#!/usr/bin/python
"""
Module containing class for main view
"""
import logging


LOGGER = logging.getLogger("dpLogger")
logging.basicConfig()

# Constant for OpenGL
GL_FILL = 6914


class SDLException(Exception):
    """
    Class for raising exception from SDL
    """
    pass


class StreamHandler(object):
    """
    Handles stdout and stderr and puts it into the logger
    """
    def __init__(self, logger, level):
        """
        Constructor
        """
        self.logger = logger
        self.level = level

    def write(self, message):
        """
        Implements write method for stream
        """
        self.logger.log(self.level, message)


class SDLWindow(object):
    """
    Main window
    """

    def __init__(self, lib, width, height, title):
        """
        Constructor, stores dynamic library handlers
        sets stdout to logger debug level
        sets stderr to logger error level
        """
        super(SDLWindow, self).__init__()
#        sys.stdout = StreamHandler(LOGGER, logging.DEBUG)
        self.lib = lib
        self.error_messages = {
            self.lib.E_SETATTR: "Failed to set SDL GL attribute",
            self.lib.E_SETVIDEO: "Failed to set SDL GL video",
            self.lib.E_GETVIDEO: "Couldn't get video information",
            self.lib.E_INIT: "Failed to initialize SDL video.",
            self.lib.E_OGLCONTEXT: "Failed to create OpenGL context",
            self.lib.E_MEM: "Not enought memory",
            self.lib.E_FILE: "Failed to open file",
            }
        ecode = self.lib.sdl_setup(width, height, title)
        if ecode != self.lib.OK:
            raise SDLException("sdl_setup failed: %s " % 
                    self.error_messages[ecode])
        else:
            LOGGER.info("SDL initialized")

    def run(self):
        """
        Runs the main window loop
        """
        LOGGER.debug("Running main loop")
        self.lib.sdl_loop()
