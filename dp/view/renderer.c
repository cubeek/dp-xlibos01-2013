/**
 * Module implementing functions above renderer structur and entityes.
 *
 * Renderer structure serves as holder of vertexes and objects in scene. It 
 * also copies vertexes to the GPU
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "renderer.h"
#include "oglutils/oglfunctions.h"
#include "oglutils/matrixUtil.h"

#define ALLOCBUNCH 32

Renderer *renderer = NULL;
SDL_WindowID window; 
int mouse_in = FALSE;
//int fullscreen = FALSE;
int b_draw_ways = TRUE;
int b_draw_hm = TRUE;
int b_draw_buildings = TRUE;
float sensitivity = 1.0;
float speed = 1.0;


Renderer *get_renderer()
{
    if (renderer == NULL)
    {
        create_renderer(&renderer);
    }
    return renderer;
}


void set_sensitivity(float val)
{
    sensitivity = val;
}


void increase_speed()
{
    if (speed < 2.0)
    {
        speed *= 10.0;
    }
    else
    {
        speed += 1.0;
    }
    printf("Increasing speed to %f\n", speed);
}


void decrease_speed()
{
    if (speed < 2.0)
    {
        speed *= 0.1;
    }
    else
    {
        speed -= 1.0;
    }
    printf("Decreasing speed to %f\n", speed);
}


GLuint create_texture_from_file(const char *filename) 
{
    GLuint texture;
    SDL_Surface *image = SDL_LoadBMP(filename);
    if (!image)
    {
        fprintf(stderr, "Failed to load image %s: %s\n", filename,
                SDL_GetError());
        return -1;
    }

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, image->w, 
                 image->h, 0, GL_BGR, GL_UNSIGNED_BYTE, 
                 image->pixels);

    glGenerateMipmap(GL_TEXTURE_2D);

    SDL_FreeSurface(image);
    return texture;
}


/******************* RENDERER FUNCTIONS *****************/

Renderer *alloc_renderer()
{
    Renderer *r = NULL;
    r = (Renderer *)malloc(sizeof(struct renderstruct));
    return r;
}

void create_renderer(Renderer **r)
{
    *r = alloc_renderer();
    (*r)->shaders = NULL;

    (*r)->view_width = 0.0;
    (*r)->view_height = 0.0;
    (*r)->world_width = 0.0;
    (*r)->world_height = 0.0;

    (*r)->camera.pos.x = 0.0;
    (*r)->camera.pos.y = 0.0;
    (*r)->camera.pos.z = 0.0;
    (*r)->camera.pitch = 0.0;
    (*r)->camera.yaw = 0.0;

    (*r)->vertices = NULL;
    (*r)->vertex_cnt = 0;
    (*r)->vertex_allocated = 0;
    (*r)->vertex_offset = 0;

    (*r)->indices = NULL;
    (*r)->index_cnt = 0;
    (*r)->index_allocated = 0;

    init_entities(&((*r)->buildings));
    (*r)->selected_building = -1;
    (*r)->buildings_classes = NULL;
    init_entities(&((*r)->ways));
    init_entities(&((*r)->heightmap));
    init_entities(&((*r)->glasses));

    (*r)->polygon_mode = GL_FILL;
    (*r)->draw_mode = GL_TRIANGLES;

    (*r)->textures = NULL;
    (*r)->texture_cnt = 0;
}

int init_renderer(Renderer *r, char *vertex_shader_path,
                   char *fragment_shader_path)
{
    clear_renderer(r);
    r->shaders = (GLuint *)malloc(2*sizeof(GLuint));
    r->program = glCreateProgram();
    GLchar *vertexsource = NULL;
    GLchar *fragmentsource = NULL;
    int ecode = E_OK;
    if ((ecode = load_shader_program_code(vertex_shader_path, &vertexsource)) != E_OK)
    {
        process_error(ecode, "");
        return ecode;
    }
    if ((ecode = load_shader_program_code(fragment_shader_path, &fragmentsource)) != E_OK)
    {
        process_error(ecode, "");
        return ecode;
    }

    glGenVertexArrays(1, &r->vao);
    glBindVertexArray(r->vao);
    
    glGenBuffers(1, &r->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, r->vbo);
    
    glGenBuffers(1, &r->ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, r->ibo);

    if ((ecode = create_and_compile_shader(vertexsource, GL_VERTEX_SHADER, 
                              &(r->shaders[VERTEX_SHADER]))) != E_OK)
    {
        process_error(ecode, "");
        return ecode;
    }
    if ((ecode = create_and_compile_shader(fragmentsource, GL_FRAGMENT_SHADER,
                              &(r->shaders[FRAGMENT_SHADER]))) != E_OK)
    {
        process_error(ecode, "");
        return ecode;
    }

    glAttachShader(r->program, r->shaders[VERTEX_SHADER]);
    glAttachShader(r->program, r->shaders[FRAGMENT_SHADER]);

    glBindAttribLocation(r->program, 0, "in_vPosition");
    glBindAttribLocation(r->program, 1, "in_vNormal");
    glBindAttribLocation(r->program, 2, "in_vTexture");

    glBindFragDataLocation(r->program, 0, "FragColor");

    glLinkProgram(r->program);

    r->pMvpMatUniform = glGetUniformLocation(r->program, "mvpMatrix");
    r->pMvMatUniform = glGetUniformLocation(r->program, "mvMatrix");
    r->pMnMatUniform = glGetUniformLocation(r->program, "normalMatrix");
    if (r->mode == VIEW)
    {
        r->pLightPositionUniform = glGetUniformLocation(r->program,
                                                    "vLightPosition");
        r->pAmbientColorUniform = glGetUniformLocation(r->program, "ambientColor");
        r->pDiffuseColorUniform = glGetUniformLocation(r->program, "diffuseColor");
        r->pSpecularColorUniform = glGetUniformLocation(r->program, "specularColor");
    }
    r->pTexCoordsUniform = glGetUniformLocation(r->program, "vTexCoords");

    // Vertex position
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          (void*)offsetof(Vertex, x));
    glEnableVertexAttribArray(0);

    // Vertex normal
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
                          sizeof(Vertex),
                          (void*)offsetof(Vertex, nx));
    glEnableVertexAttribArray(1);


    // Texture coordination
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE,
                          sizeof(Vertex),
                          (void*)offsetof(Vertex, s0));
    glEnableVertexAttribArray(2);

    printf("GL_VERSION = %s\n"
           "GL_SHADING_LANGUAGE_VERSION = %s\n"
           "GL_RENDERER = %s\n", glGetString(GL_VERSION), 
           glGetString(GL_SHADING_LANGUAGE_VERSION), 
           glGetString(GL_RENDERER));
    glBindVertexArray(0);

    return ecode;
}


/**
 * Frees vertices and indexes and sets counters to 0
 */
void clear_renderer(Renderer *r)
{
    if (r == NULL)
        return;
    
    if (r->vertices != NULL)
    {
        free(r->vertices);
        r->vertices = NULL;
    }
    r->vertex_cnt = 0;
    r->vertex_allocated = 0;
    r->vertex_offset = 0;
    
    if (r->indices != NULL)
    {
        free(r->indices);
        r->indices = NULL;
    }
    r->index_cnt = 0;
    r->index_allocated = 0;
    
    r->view_height = 0;
    r->view_width = 0;
    r->world_height = 0;
    r->world_width = 0;

    clear_entities(&(r->buildings));
    r->selected_building = -1;
    if (r->buildings_classes != NULL)
    {
        free(r->buildings_classes);
        r->buildings_classes = NULL;
    }
    clear_entities(&(r->ways));
    clear_entities(&(r->heightmap));
    clear_entities(&(r->glasses));

    if (r->textures != NULL)
        free(r->textures);
    r->texture_cnt = 0;

    printf("Renderer cleared\n");
}


int copy_data_to_buffers(Renderer *r)
{
    int size;
#ifdef __APPLE__
    int init_class = NOTHING;
#endif
    if (r->buildings_classes == NULL)
    {
        size = sizeof(int) * r->buildings.entity_cnt;
        r->buildings_classes = (int *)malloc(size);
        if (r->buildings_classes == NULL)
            return E_MEM;
#ifdef __APPLE__
        memset_pattern4(r->buildings_classes, &init_class, size);
#else
        for (int i=0; i<r->buildings.entity_cnt; i++)
            r->buildings_classes[i] = NOTHING;
#endif
    }
    glBindVertexArray(r->vao);
    glBindBuffer(GL_ARRAY_BUFFER, r->vbo);
    glBufferData(GL_ARRAY_BUFFER, r->vertex_cnt * sizeof(Vertex),
                 r->vertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, r->ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, r->index_cnt * sizeof(GLuint),
                 r->indices, GL_STATIC_DRAW);
    glBindVertexArray(0);
    return E_OK;
}


void draw_ways(Renderer *r)
{
    long int cur_offset = 0;
    long int count = 0;

    glPolygonOffset(-2, -2);
    glEnable(GL_POLYGON_OFFSET_FILL);
    for (int i=0; i < r->ways.entity_cnt; i++)
    {
        cur_offset = r->ways.ent[i].offset;
        count = r->ways.ent[i].length;
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, r->ways.ent[i].texture);
        glDrawElements(r->draw_mode, count, GL_UNSIGNED_INT, 
                       BUFFER_OFFSET(cur_offset));
    }
    glDisable(GL_POLYGON_OFFSET_FILL);
}


void draw_heightmap(Renderer *r)
{
    long int cur_offset = 0;
    long int count = 0;

    for (int i=0; i < r->heightmap.entity_cnt; i++)
    {
        cur_offset = r->heightmap.ent[i].offset;
        count = r->heightmap.ent[i].length;
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, r->heightmap.ent[i].texture);
        glDrawElements(r->draw_mode, count, GL_UNSIGNED_INT, 
                       BUFFER_OFFSET(cur_offset));
    }
}


void draw_glasses(Renderer *r)
{
    long int cur_offset = 0;
    long int count = 0;

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    for (int i=0; i < r->glasses.entity_cnt; i++)
    {
        cur_offset = r->glasses.ent[i].offset;
        count = r->glasses.ent[i].length;
        glBindTexture(GL_TEXTURE_2D, r->glasses.ent[i].texture);
        glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_INT, 
                       BUFFER_OFFSET(cur_offset));
    }
    glDisable(GL_BLEND);
}


void draw_buildings(Renderer *r)
{
    long int cur_offset = 0;
    long int count = 0;

    for (int i=0; i < r->buildings.entity_cnt; i++)
    {
        cur_offset = r->buildings.ent[i].offset;
        count = r->buildings.ent[i].length;
        glBindTexture(GL_TEXTURE_2D, r->buildings.ent[i].texture);
        glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_INT, 
                       BUFFER_OFFSET(cur_offset));
    }
    draw_glasses(r);
}


void print_matrix(float *mat, int size)
{
    for (int i=0; i<size; i++)
    {
        for (int j=0; j<size; j++)
        {
            printf("%f ", mat[i*size + j]);
        }
        printf("\n");
    }
}


void draw(Renderer *r)
{
    GLfloat projection[16];
    GLfloat modelView[16];
    GLfloat normalMatrix[9];
    GLfloat tmpMatrix[9];
    GLfloat light_position[4];
    GLfloat mvp[16];
    GLenum e;

    // Calculate the projection matrix
    mtxLoadPerspective(projection, 45, 
                       (float)r->view_width / (float)r->view_height,
                       0.1, 100000.0f);

    mtxLoadIdentity(modelView);
    mtxRotateApply(modelView, r->camera.pitch, 1.0, 0.0, 0.0);
    mtxRotateApply(modelView, -r->camera.yaw, 0.0, 1.0, 0.0);

    // Rotations for normal matrix
    mtx3x3FromTopLeftOf4x4(tmpMatrix, modelView);
    mtx3x3Transpose(normalMatrix, tmpMatrix);

    // Translation of points
    mtxTranslateApply(modelView, -r->camera.pos.x, -r->camera.pos.y,
                      -r->camera.pos.z);

    // Multiply the modelview and projection matrix and set it in the shader
    mtxMultiply(mvp, projection, modelView);
    if (r->mode == VIEW)
    {
        mult_mat4_vec4(mvp, r->light_position, light_position);
    }
    glUseProgram(r->program);
    
    glClearColor(r->clear_color[0], r->clear_color[1], r->clear_color[2],
                 r->clear_color[3]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);

    if (r->mode == VIEW)
    {
        glUniform4fv(r->pAmbientColorUniform, 1, r->ambient_color);
        glUniform4fv(r->pDiffuseColorUniform, 1, r->diffuse_color);
        glUniform4fv(r->pSpecularColorUniform, 1, r->spec_color);
        glUniform3fv(r->pLightPositionUniform, 1, light_position);
    }
    glUniformMatrix4fv(r->pMvpMatUniform, 1, GL_FALSE, mvp);
    glUniformMatrix4fv(r->pMvMatUniform, 1, GL_FALSE, modelView);
    glUniformMatrix3fv(r->pMnMatUniform, 1, GL_FALSE, normalMatrix);

    glUniform1i(r->pTexCoordsUniform, 0);

    glBindVertexArray(r->vao);
    glPolygonMode(GL_FRONT_AND_BACK, r->polygon_mode);
    if (b_draw_ways)
        draw_ways(r);
    if (b_draw_hm)
        draw_heightmap(r);
    if (b_draw_buildings)
        draw_buildings(r);
    glBindVertexArray(0);

    glUseProgram(0);
    SDL_GL_SwapWindow(window);
    while ((e = glGetError()) != GL_NO_ERROR)
    {
        fprintf(stderr, "ERROR!: ");
        print_gl_error(e);
    }
}


long add_vertex(Renderer *r, GLfloat x, GLfloat y, GLfloat z,
                GLfloat nx, GLfloat ny, GLfloat nz, 
                GLfloat s, GLfloat t)
{
    long pos;
    
    if (r->vertices == NULL)
    {
        r->vertex_allocated = ALLOCBUNCH;
        r->vertices = (Vertex *)malloc(r->vertex_allocated*sizeof(Vertex));
        if (r->vertices == NULL)
        {
            return E_MEM;
        }
    }

    r->vertex_cnt++;
    if (r->vertex_cnt > r->vertex_allocated)
    {
        r->vertex_allocated += ALLOCBUNCH;
        r->vertices = (Vertex *)realloc(r->vertices, 
                                        sizeof(Vertex)*r->vertex_allocated);
        if (r->vertices == NULL)
        {
            return E_MEM;
        }
        
    }

    pos = r->vertex_cnt - 1;

    // Scene was mirrored
    r->vertices[pos].x = -1*x;
    r->vertices[pos].y = y;
    r->vertices[pos].z = z;
    r->vertices[pos].nx = nx;
    r->vertices[pos].ny = ny;
    r->vertices[pos].nz = nz;
    r->vertices[pos].s0 = s;
    r->vertices[pos].t0 = t;

    return pos;
}


long add_index(Renderer *r, GLuint index)
{
    long pos;
    
    if (r->indices == NULL)
    {
        r->index_allocated = ALLOCBUNCH;
        r->indices = (GLuint *)malloc(r->index_allocated*sizeof(GLuint));
        if (r->indices == NULL)
        {
            return E_MEM;
        }
    }
    
    r->index_cnt++;
    if (r->index_cnt > r->index_allocated)
    {
        r->index_allocated += ALLOCBUNCH;
        r->indices = (GLuint *)realloc(r->indices, sizeof(GLuint)*
                                       r->index_allocated);
        if (r->indices == NULL)
        {
            return -1;
        }
        
    }
    
    pos = r->index_cnt - 1;
    r->indices[pos] = index + r->vertex_offset;

    return pos;
}


void add_building(Renderer *r, long int index_cnt, int texture_index)
{
    long int offset = r->index_cnt;
    int texture;

    r->vertex_offset = r->vertex_cnt;
    add_entity(&(r->buildings), index_cnt, offset);
    if (texture_index < 0)
        texture = r->c_textures[NOTHING];
    else
        texture = r->textures[texture_index];
    r->buildings.ent[r->buildings.entity_cnt-1].texture = texture;
}


void add_glass(Renderer *r, long int index_cnt, int texture_index)
{
    long int offset = r->index_cnt;

    r->vertex_offset = r->vertex_cnt;
    add_entity(&(r->glasses), index_cnt, offset);
    r->glasses.ent[r->glasses.entity_cnt-1].texture = 
                    r->textures[texture_index];
}


void add_way(Renderer *r, long int index_cnt, int texture_index)
{
    long int offset = r->index_cnt;

    r->vertex_offset = r->vertex_cnt;
    add_entity(&(r->ways), index_cnt, offset);
    r->ways.ent[r->ways.entity_cnt-1].texture = r->textures[texture_index];
}


void add_heightmap(Renderer *r, long int index_cnt, int texture_index)
{
    long int offset = r->index_cnt;

    r->vertex_offset = r->vertex_cnt;
    add_entity(&(r->heightmap), index_cnt, offset);
    r->heightmap.ent[r->heightmap.entity_cnt-1].texture = r->textures[texture_index];
}


void set_next_building(Renderer *r)
{
    if (r->selected_building >= r->buildings.entity_cnt-1)
        return;
    if (r->selected_building > -1)
    {
        r->buildings.ent[r->selected_building].texture = r->last_texture_color;
    }
    r->selected_building++;
    r->last_texture_color = r->buildings.ent[r->selected_building].texture;
    r->buildings.ent[r->selected_building].texture = r->c_textures[SELECTED];
}


void set_prev_building(Renderer *r)
{
    if (r->selected_building <= 0)
        return;
    r->buildings.ent[r->selected_building].texture = r->last_texture_color;
    r->selected_building--;
    r->last_texture_color = r->buildings.ent[r->selected_building].texture;
    r->buildings.ent[r->selected_building].texture = r->c_textures[SELECTED];
}


int allocate_texture_memory(Renderer *r, int num)
{
    r->textures = (GLuint *)malloc(num*sizeof(GLuint));
    if (r->textures == NULL)
        return E_MEM;
    r->texture_cnt = num;
    return E_OK;
}


int load_texture(GLuint *target, const char *texture_file, int index)
{
    GLuint texture = create_texture_from_file(texture_file);
    if (texture == -1)
        return E_FILE;
    target[index] = texture;
    return E_OK;
}


/**
 * Cleans up the opengl structures
 */
void cleanup(Renderer *r)
{
    printf("Cleaning up memory\n");
    glUseProgram(0);
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDetachShader(r->program, r->shaders[VERTEX_SHADER]);
    glDetachShader(r->program, r->shaders[FRAGMENT_SHADER]);
    glDeleteProgram(r->program);
    glDeleteShader(r->shaders[VERTEX_SHADER]);
    glDeleteShader(r->shaders[FRAGMENT_SHADER]);
    glDeleteBuffers(1, &r->vbo);
    glDeleteBuffers(1, &r->ibo);
    glDeleteVertexArrays(1, &r->vao);
    printf("GL cleared\n");
    clear_renderer(r);
}

/******************* END RENDERER ****************/



/********************** Entities *****************/

int add_entity(Entities *entities, long int index_cnt, long int offset)
{
    long pos;
    
    if (entities->ent == NULL)
    {
        entities->entity_allocated = ALLOCBUNCH;
        entities->ent = (struct entity *)malloc(entities->entity_allocated*
                                        sizeof(struct entity));
        if (entities->ent == NULL)
        {
            return E_MEM;
        }
    }
    
    entities->entity_cnt++;
    if (entities->entity_cnt > entities->entity_allocated)
    {
        entities->entity_allocated += ALLOCBUNCH;
        entities->ent = (struct entity *)realloc(entities->ent, 
                      sizeof(struct entity)*entities->entity_allocated);
        if (entities->ent == NULL)
        {
            return -1;
        }
        
    }
    
    pos = entities->entity_cnt - 1;
    entities->ent[pos].offset = offset;
    entities->ent[pos].length = index_cnt;

    entities->last_entity += index_cnt;

    return pos;
}

void init_entities(Entities *entities)
{
    entities->ent = NULL;
    entities->entity_cnt = 0;
    entities->entity_allocated = 0;
    entities->last_entity = 0;
}

void clear_entities(Entities *entities)
{
    if (entities == NULL)
    {
        return;
    }

    if (entities->ent != NULL)
    {
        free(entities->ent);
        entities->ent = NULL;
    }
    entities->entity_cnt = 0;
    entities->entity_allocated = 0;
    entities->last_entity = 0;
}


/***************** END ENTITIES ***************/



/****************** SDL **************************/


/**
 * Event handler serves when window size is changed
 *
 * @param width: new width of window
 * @param height: new height of window
 */
void on_window_resized(int width, int height)
{
    glViewport(0, 0, width, height);
    renderer->view_width = width;
    renderer->view_height = height;
    on_window_redraw();
}


// Setup SDL and OpenGL
int sdl_setup(int width, int height, const char *window_title)
{
    const SDL_VideoInfo* vidinfo;
    SDL_GLContext OpenGLContext; 
    SDL_version compiled;

    SDL_VERSION(&compiled);
    printf("Compiled against SDL version %d.%d.%d ...\n",
           compiled.major, compiled.minor, compiled.patch);

    // init video system 
    if( SDL_Init(SDL_INIT_VIDEO) < 0 )
    {
        fprintf(stderr, "%s\n", SDL_GetError());
        return E_INIT;
    }
    // tell system which funciton to process when exit() call is made
    atexit(SDL_Quit);

    // Request core-profile context
    if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3) != 0)
    {
        fprintf(stderr, "SDL_CONTEXT_MAJOR_VERSION - 3\n");
        fprintf(stderr, "%s\n", SDL_GetError());
        return E_SETATTR;
    }
    if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2) != 0)
    {
        fprintf(stderr, "SDL_GL_CONTEXT_MINOR_VERSION - 2\n");
        fprintf(stderr, "%s\n", SDL_GetError());
        return E_SETATTR;
    }

    // get optimal video settings
    vidinfo = SDL_GetVideoInfo();
    if(!vidinfo)
    {
        fprintf(stderr, "%s\n", SDL_GetError());
        return E_GETVIDEO;
    }

    // set opengl attributes
    if (SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5) < 0)
    {
        fprintf(stderr, "SDL_GL_RED_SIZE - 5\n");
        fprintf(stderr, "%s\n", SDL_GetError());
        return E_SETATTR;
    }
    if (SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5) < 0)
    {
        fprintf(stderr, "SDL_GL_GREEN_SIZE - 5\n");
        fprintf(stderr, "%s\n", SDL_GetError());
        return E_SETATTR;
    }
    if (SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5) < 0)
    {
        fprintf(stderr, "SDL_GL_BLUE_SIZE - 5\n");
        fprintf(stderr, "%s\n", SDL_GetError());
        return E_SETATTR;
    }
#ifdef __APPLE__
    if (SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 32) < 0)
    {
        fprintf(stderr, "SDL_GL_DEPTH_SIZE - 32\n");
        fprintf(stderr, "%s\n", SDL_GetError());
        return E_SETATTR;
    }
#else
    if (SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16) < 0)
    {
        fprintf(stderr, "SDL_GL_DEPTH_SIZE - 16\n");
        fprintf(stderr, "%s\n", SDL_GetError());
        return E_SETATTR;
    }
#endif
    if (SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1) < 0)
    {
        fprintf(stderr, "SDL_GL_DOUBLEBUFFER - 1\n");
        fprintf(stderr, "%s\n", SDL_GetError());
        return E_SETATTR;
    }

    window = SDL_CreateWindow(window_title, SDL_WINDOWPOS_CENTERED, 
                              SDL_WINDOWPOS_CENTERED, width, height,
                              SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
    OpenGLContext = SDL_GL_CreateContext(window); 
    if (OpenGLContext == NULL)
    {
        fprintf(stderr, "%s\n", SDL_GetError());
        return E_OGLCONTEXT;
    }

    return OK;
}


void set_building_class(Renderer *r, TBuildingClass class)
{
    if (r->mode == COLLECT && r->selected_building >= 0)
    {
        r->buildings_classes[r->selected_building] = class;
        r->last_texture_color = r->c_textures[class];
        set_next_building(r);
        copy_data_to_buffers(r);
    }
}


/**
 * Event handler serves when key is pressed down
 *
 * @param key: Pressed key
 * @param mode: mode
 */
void on_key_down(SDLKey key, Uint16 mode, Uint32 window)
{
    switch (key)
    {
        case SDLK_w:
            // Move up
            move_camera(&(renderer->camera), speed, FORWARD);
            break;
        case SDLK_s:
            // Move down
            move_camera(&(renderer->camera), speed, BACKWARD);
            break;
        case SDLK_a:
            // Move left
            move_camera(&(renderer->camera), speed, LEFT);
            break;
        case SDLK_d:
            // Move right
            move_camera(&(renderer->camera), speed, RIGHT);
            break;
        case SDLK_EQUALS:
            increase_speed();
            break;
        case SDLK_MINUS:
            decrease_speed();
            break;
        case SDLK_o:
            if (renderer->draw_mode == GL_TRIANGLES)
                renderer->draw_mode = GL_LINES;
            else if (renderer->draw_mode == GL_LINES)
                renderer->draw_mode = GL_POINTS;
            else
                renderer->draw_mode = GL_TRIANGLES;
            break;
        case SDLK_p:
            if (renderer->polygon_mode == GL_LINE)
                renderer->polygon_mode = GL_FILL;
            else if (renderer->polygon_mode == GL_FILL)
                renderer->polygon_mode = GL_LINE;
            break;
        case SDLK_1:
            set_building_class(renderer, BOF);
            break;
        case SDLK_2:
            set_building_class(renderer, OLD);
            break;
        case SDLK_3:
            set_building_class(renderer, HOUSE);
            break;
        case SDLK_4:
            set_building_class(renderer, OTHER);
            break;
        case SDLK_0:
            set_building_class(renderer, NOTHING);
            break;
        case SDLK_n:
            if (renderer->mode == COLLECT)
            {
                set_next_building(renderer);
                copy_data_to_buffers(renderer);
            }
            break;
        case SDLK_b:
            if (renderer->mode == COLLECT)
            {
                set_prev_building(renderer);
                copy_data_to_buffers(renderer);
            }
            break;
        case SDLK_ESCAPE:
                SDL_ShowCursor(SDL_ENABLE);
                mouse_in = FALSE;
            break;
        case SDLK_g:
            b_draw_buildings = !b_draw_buildings;
            break;
        case SDLK_h:
            b_draw_hm = !b_draw_hm;
            break;
        case SDLK_j:
            b_draw_ways = !b_draw_ways;
            break;
        case SDLK_r:
            renderer->camera.pitch = 0.0;
            renderer->camera.yaw = 0.0;
            break;

        case SDLK_RIGHT:
            renderer->light_position[0] += speed;
            break;
        case SDLK_LEFT:
            renderer->light_position[0] -= speed;
            break;
        case SDLK_UP:
            renderer->light_position[2] += speed;
            break;
        case SDLK_DOWN:
            renderer->light_position[2] -= speed;
            break;
        case SDLK_LEFTBRACKET:
            renderer->light_position[1] += speed;
            break;
        case SDLK_RIGHTBRACKET:
            renderer->light_position[1] -= speed;
            break;
        default:
            break;
    }
}


/**
 * Event handler serves when key is released
 *
 * @param key: Pressed key
 * @param mode: mode
 */
void on_key_up(SDLKey key, Uint16 mode)
{
    return;
}

/**
 * Event handler serves mouse movement
 */
void on_mouse_move(unsigned x, unsigned y, int xrel, int yrel, Uint8 buttons)
{
    if (mouse_in == TRUE)
    {
        renderer->camera.yaw += xrel*sensitivity;
        renderer->camera.pitch += yrel*sensitivity;
        lock_camera(&(renderer->camera));
        on_window_redraw();
    }
}


/**
 * Event handler serves when mouse button is pushed
 */
void on_mouse_down(Uint8 button, unsigned x, unsigned y)
{
    if (mouse_in == FALSE && button & SDL_BUTTON_LMASK)
    {
        mouse_in = TRUE;
        SDL_ShowCursor(SDL_DISABLE);
    }
}


/**
 * Event handler serves when mouse button is released
 */
void on_mouse_up(Uint8 button, unsigned x, unsigned y)
{
    return;
}


/**
 * Copies data from renderer to GPU and redraws the scene
 *
 */
void on_window_redraw()
{
    draw(renderer);
/*
#ifndef __APPLE__
    if (mouse_in)
        SDL_WarpMouse(renderer->view_width / 2, renderer->view_height / 2);
#endif
*/
}


void sdl_loop()
{
    SDL_Event event;
    int active = TRUE;
    int redraw = FALSE;
    on_window_redraw();
    mouse_in = FALSE;
    while(1)
    {
        if (SDL_WaitEvent(&event) == 0)
        {
            fprintf(stderr, "Error occured in SDL_WaitEvent\n");
            return;
        }
        // Handle events
        do
        {
            switch(event.type)
            {
                case SDL_MOUSEMOTION:
                    on_mouse_move(event.motion.x, event.motion.y,
                                event.motion.xrel, event.motion.yrel,
                                event.motion.state);
                    break;
                case SDL_ACTIVEEVENT:// Stop redraw when minimized
                    if(event.active.state == SDL_APPACTIVE)
                        printf("Setting active to %d\n", event.active.gain);
                        active = event.active.gain;
                    break;
                case SDL_KEYDOWN:
                    on_key_down(event.key.keysym.sym, event.key.keysym.mod, event.key.windowID);
                    on_window_redraw();
                    break;
                case SDL_KEYUP:
                    on_key_up(event.key.keysym.sym, event.key.keysym.mod);
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    on_mouse_down(event.button.button, event.button.x,
                                  event.button.y);
                    break;
                case SDL_MOUSEBUTTONUP:
                    on_mouse_up(event.button.button, event.button.x,
                                event.button.y);
                    break;
                case SDL_QUIT:
                    return;// End main loop
                case SDL_WINDOWEVENT:
                    if (event.window.event == SDL_WINDOWEVENT_RESIZED)
                        on_window_resized(event.window.data1,
                                          event.window.data2);
                    break;
                case SDL_VIDEORESIZE:
                    on_window_resized(event.resize.w, event.resize.h);
                    break;
                case SDL_VIDEOEXPOSE:
                    redraw = TRUE;
                    break;
                default :// Do nothing
                    break;
            }
        } while(SDL_PollEvent(&event) == 1);

        if (active && redraw)
        {
            redraw = FALSE;
            on_window_redraw();
        }
    }
}

/******************* END SDL ********************/



/******************* CAMERA *********************/

void lock_camera(Camera *camera)
{
    if (camera->pitch > 90.0)
    {
        camera->pitch = 90.0;
    }
    else if (camera->pitch < -90.0)
    {
        camera->pitch = -90.0;
    }
    if (camera->yaw < 0.0)
    {
        camera->yaw += 360.0;
    }
    else if (camera->yaw > 360.0)
    {
        camera->yaw -= 360.0;
    }
}

float to_rad(float degree)
{
    return degree*M_PI/180.0;
}

void move_camera(Camera *camera, float distance, movement_type m_type)
{
    float pitch, yaw;
    float tmp;

    switch (m_type)
    {
        case FORWARD:
            pitch = to_rad(camera->pitch*(-1.0));
            yaw = to_rad(camera->yaw - 90.0);
            break;
        case BACKWARD:
            pitch = to_rad(camera->pitch);
            yaw = to_rad(camera->yaw + 90.0);
            break;
        case LEFT:
            yaw = to_rad(camera->yaw + 180.0);
            pitch = 0.0;
            break;
        case RIGHT:
            pitch = 0.0;
            yaw = to_rad(camera->yaw);
            break;
        default:
            return;
    }
    tmp = distance * cos(pitch);
    camera->pos.x += tmp*cos(yaw);
    camera->pos.y += distance*sin(pitch);
    camera->pos.z += tmp*sin(yaw);
}

/******************* END CAMERA *****************/
