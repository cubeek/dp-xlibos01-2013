//
//  ogl_functions.c
//  Master thesis
//
//  Created by Jakub Libosvar on 1/24/12.
//  Copyright (c) 2012 jakub.libosvar@gmail.com. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef __APPLE__
#include <OpenGL/gl3.h>
#elif defined(__MACOS__)
#include <gl.h>
#include <glu.h>
#else
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glext.h>
#endif
#include "oglfunctions.h"


// Messages based on error codes
const char *error_msgs[] = {
    "Everything works, why do you even call this method right now?\n",
    "Cannot open file %s\n",
    "Cannot compile shader\n",
    "Cannot create program\n",
    "Canont link program\n",
};


void process_error(int ecode, char *message) {
    switch (ecode) {
        case E_OK : return;
            break;
        case E_OPENFILE:
            fprintf(stderr, error_msgs[ecode], message);
            break;
        default:
            fprintf(stderr, error_msgs[ecode], NULL);
            break;
    }
}

void print_gl_error(GLenum ecode)
{
    const char *str;
    switch(ecode)
    {
        case GL_NO_ERROR:
            str = "GL_NO_ERROR";
            break;
        case GL_INVALID_ENUM:
            str = "GL_INVALID_ENUM";
            break;
        case GL_INVALID_VALUE:
            str = "GL_INVALID_VALUE";
            break;
        case GL_INVALID_OPERATION:
            str = "GL_INVALID_OPERATION";
            break;        
        case GL_OUT_OF_MEMORY:
            str = "GL_OUT_OF_MEMORY";
            break;
        case GL_INVALID_FRAMEBUFFER_OPERATION:
            str = "GL_INVALID_FRAMEBUFFER_OPERATION";
            break;
        default:
            str = "(ERROR: Unknown Error Enum)";
            break;
    }
    fprintf(stderr, "glError Enum: %s\n", str);
}


void mult_mat4_vec4(float *mat4, float *vec4, float *dest) {
    dest[0] = dest[1] = dest[2] = 0.0;
    dest[3] = 0.0;
    for (int i=0; i<4; i++)
    {
        for (int j=0; j<4; j++)
        {
            dest[i] += vec4[j] * mat4[i * 4 + j];
        }
    }
}


char *vertex_shader_source_code = "#version 150\n\
// in_Position was bound to attribute index 0 and in_Color was bound to attribute index 1\n\
in  vec2 in_Position;\n\
in  vec3 in_Color;\n\
\n\
out vec3 ex_Color;\n\
void main(void) {\n\
gl_Position = vec4(in_Position, 0.0, 1.0);\n\
\n\
ex_Color = in_Color;\n\
}";

char *fragment_shader_source_code = "#version 150\n\
precision highp float;\n\
\n\
in  vec3 ex_Color;\n\
out vec4 FragColor;\n\
\n\
void main(void) {\n\
    // Pass through our original color with full opacity.\n\
    FragColor = vec4(ex_Color,1.0);\n\
}";


int create_program_and_attach_shaders(GLuint *shaders, int count,
                                      GLuint *program)
{
    GLint program_status;
    
    *program = glCreateProgram();
    
    if (*program == 0) {
        return E_CREATINGPROGRAM;
    }
    
    for (int i=0; i<count; i++) {
        glAttachShader(*program, shaders[i]);
    }
    
    glLinkProgram(*program);
    
    glGetProgramiv(*program, GL_LINK_STATUS, &program_status);
    if (program_status == GL_FALSE) {
        return E_LINKPROGRAM;
    }
    
    return E_OK;
}


int create_and_compile_shader(const char *source_code, GLint type,
                              GLuint *shader)
{
    GLint length = (GLint)strlen(source_code);
    GLint compile_status;
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, (const GLchar **)&source_code, &length);
    glCompileShader(*shader);
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &compile_status);
    if (compile_status == GL_FALSE) {
        int maxLength;
        char *vertexInfoLog = NULL;
        vertexInfoLog = (char *)malloc(maxLength);
        
        glGetShaderInfoLog(*shader, maxLength, &maxLength, vertexInfoLog);
        
        if (type == GL_VERTEX_SHADER) {
            printf("Vertex info:");
        } else if (type == GL_FRAGMENT_SHADER) {
            printf("Fragment info:");
        }
        printf(" %s\n", vertexInfoLog);
        
        free(vertexInfoLog);
        return E_SHADERCOMPILE;
    }
    
    return E_OK;
}


int load_shader_program_code(char *shader_filename, char **source_code) {
    FILE *shader_file;
    long size = 0;
    int code = 0;
    
    shader_file = fopen(shader_filename, "r");
    
    if (shader_file == NULL) {
        *source_code = NULL;
        return E_OPENFILE;
    }
    
    code = fseek(shader_file, 0, SEEK_END);
    size = ftell(shader_file);
    *source_code = (char *)malloc(size+1);
    fseek(shader_file, 0, SEEK_SET);
    fread(*source_code, size, 1, shader_file);
    (*source_code)[size] = 0;
    fclose(shader_file);

    return E_OK;
}

int load_shaders(GLuint *program) {
    char *source_code;
    int ecode;
    GLuint shaders[2];
    
#ifndef LOAD_FROM_MEM
    ecode = load_shader_program_code(VERTEX_SHADER_FILE, &source_code);
    if (ecode == E_OPENFILE) {
        process_error(ecode, VERTEX_SHADER_FILE);
        return ecode;
    }
    if (ecode != E_OK)
        return ecode;
#else
    source_code = vertex_shader_source_code;
#endif
    
    ecode = create_and_compile_shader(source_code, GL_VERTEX_SHADER,
                                      &shaders[0]);
    if (ecode != E_OK)
        return ecode;
    
#ifndef LOAD_FROM_MEM
    ecode = load_shader_program_code(FRAGMENT_SHADER_FILE, &source_code);
    if (ecode != E_OK)
        return ecode;
#else
    source_code = fragment_shader_source_code;
#endif
    
    ecode = create_and_compile_shader(source_code, GL_FRAGMENT_SHADER,
                                      &shaders[1]);
    if (ecode != E_OK)
        return ecode;
    
    ecode = create_program_and_attach_shaders(shaders, 2, program);
    
    return ecode;
}



