//
//  ogl_functions.h
//
//  Created by Jakub Libosvar on 1/24/12.
//  Copyright (c) 2012 jakub.libosvar@gmail.com. All rights reserved.
//

#ifndef ogl_functions_h
#define ogl_functions_h

// File containing vertex shader code
#define VERTEX_SHADER_FILE "vertex_shader.glsl"

// File containing fragment shader code
#define FRAGMENT_SHADER_FILE "fragment_shader.glsl"

// Error codes
typedef enum ERROR_CODES { E_OK, E_OPENFILE, E_SHADERCOMPILE,
                           E_CREATINGPROGRAM, E_LINKPROGRAM } TCodes;

/**
 * Processes error code and prints relevant message
 */
void process_error(int /* ecode */, char * /* message */);

/**
 * Prints gl enum
 */
void print_gl_error(GLenum ecode);


/**
 * Multiplies 4D vector with matrix 4x4
 *
 * @param mat4: Matrix that will be multiplied
 * @param vec4: Vector that will be multiplied
 * @param dest: Where result will be stored
 */
void mult_mat4_vec4(float *mat4, float *vec4, float *dest);


/**
 *
 * @params char * shader_filename: 
 * @params char ** source_code:
 */
int create_program_and_attach_shaders(GLuint * /* shaders */, int /* count */,
                                      GLuint * /* program */);


/**
 * Loads shader source code from file
 *
 * @params char * shader_filename: 
 * @params char ** source_code:
 * @return: error code
 */
int load_shader_program_code(char * /* shader_filename */, 
                             char ** /* source_code */);


/**
 * Creates and compiles the shader from it's source code
 * @params char * shader_filename: 
 * @params char ** source_code:
 */
int create_and_compile_shader(const char * /* source_code */,
                              GLint /* type */, GLuint * /* shader */);


/**
 * 
 * @params char * shader_filename: 
 * @params char ** source_code:
 * @return: error code
 */
int load_shaders(GLuint * /* program */);


#endif
