#version 330
precision highp float;

in vec4 vPosition;
in vec4 vNormal;
in vec2 v_texcoord;

uniform sampler2D vTexCoords;

out vec4 FragColor;


void main(void) {
    FragColor = texture(vTexCoords, v_texcoord);
}
