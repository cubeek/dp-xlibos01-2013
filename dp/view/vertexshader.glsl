#version 330
in vec3 in_vPosition;
in vec3 in_vNormal;
in vec2 in_vTexture;

uniform mat4 mvpMatrix;
uniform mat4 mvMatrix;
uniform mat3 normalMatrix;
uniform vec3 vLightPosition;

out vec3 vNormal;
out vec3 vLightDir;
out vec2 v_texcoord;


void main(void)
{
    vec4 vPosition = mvMatrix * vec4(in_vPosition, 1.0);
    vec3 vPosition3 = vPosition.xyz / vPosition.w;
    vNormal = normalMatrix * in_vNormal;

    vLightDir = normalize(vLightPosition - vPosition3);

    gl_Position = mvpMatrix * vec4(in_vPosition, 1.0);

    // Texture coordinates
    v_texcoord = in_vTexture;
}
