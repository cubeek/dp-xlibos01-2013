"""
Module containing constants for all modules
"""
import os
from multiprocessing import cpu_count

from dp.config import config_parser

LOGGER_CONF = "dp/conf/logger.conf"
LOGGER_NAME = "dpLogger"

#Exit codes
E_OK = 0
E_PARAMS = 1        #Wrong input parameters
E_FILE = 2          #Cannot open file
E_IMPORT = 3        #Import failed
E_CONNECTION = 5    #Connection failed
E_DIMENSION = 10    #Dimension error
E_MODULES = 15      #Necessary modules not found

# Count of locations in one query
GAPI_LOCS = 64

# Value to round the float number
ROUND_VAL = 9

# Threshold for precision of altitude in height map
ALT_THRES = 1.0623
ALT_RAND = ALT_THRES/2.0
ALT_DEPTH = 10

#
EARTH_R = 6378000


#Entity types
ENT_UNKNOWN = -1
ENT_FHOUSE = 0
ENT_FLATS = 1
ENT_APARTMENTS = 2

# Ways settings
WAY_WIDTH = 15

# api XSD scheme file path
API_XSD_PATH = "dp/conf/osm_api_scheme.xsd"

# Workers for parallel threading
MAX_DOWNLOAD_WORKERS = cpu_count()
MAX_WORKERS = config_parser.getint('common', 'max_workers')
THREAD_WAIT = 5

# Classifier settings
CLASSIFIER_PATH=os.path.join(os.path.dirname(__file__),
                             'classifier/classifier.svm')
CLASSIFIER_DATA_PATH=os.path.join(os.path.dirname(__file__),
                                  'classifier/training.data')


# Entities type
BUILDING = 'building'
WAY = 'highway'

TEXTURE_DIR = "dp/view/textures"
VERTEX_SHADER_PATH = "dp/view/vertexshader.glsl"
FRAGMENT_SHADER_PATH = "dp/view/fragmentshader.glsl"
NOLIGHT_FRAGMENT_SHADER_PATH = "dp/view/nolight-fragmentshader.glsl"
