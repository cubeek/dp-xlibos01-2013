#!/usr/bin/python

"""
Main module
"""

import argparse
import logging
import logging.config
import os
import sys

from dp import common
from dp.downloader.osm import OsmApi
from dp import modes


DESC = "Here comes the description."
LOG_DIR = os.path.join(os.path.dirname(__file__), '../log')


def init_loggers(args):
    """
    Initialize logger according args. If no arguments for logger are
    given, conf/logger.conf is used
    """
    logger_levels = {
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warn': logging.WARN,
        'warning': logging.WARNING
    }

    log_file = None
    if args.log:
        log_dir = os.path.dirname(args.log) or '.'
        log_file = os.path.basename(args.log)
    else:
        log_dir = LOG_DIR
    if not os.path.isdir(LOG_DIR):
        os.mkdir(LOG_DIR)
    if not os.path.isdir(log_dir):
        os.mkdir(log_dir)
    logging.config.fileConfig(common.LOGGER_CONF)
    logger = logging.getLogger(common.LOGGER_NAME)
    default_formater = logger.handlers[0].formatter
    if log_file is not None:
        if log_file == '-':
            new_handler = logging.StreamHandler(sys.stdout)
        else:
            new_handler = logging.FileHandler(log_file)
        new_handler.setFormatter(default_formater)
        logger.addHandler(new_handler)
    if args.debug:
        logger.level = logger_levels[args.debug.lower()]
    logger.debug("Logger initialized")


def parse_args(args):
    """
    Parses arguments of command line and returns tuples

    @param args: Command line arguments
    """
    parser = argparse.ArgumentParser(prog="dp", description=DESC)
    parser.add_argument('mode', action='store', type=str,
                        choices=['build', 'view', 'download', 'train',
                                 'collect'],
                        help="Mode that program will run in.")
    parser.add_argument('-s', "--osm-server", dest='osm_server',
                        help="Openstreetmap API server URL",
                        default='www.openstreetmap.org')
    parser.add_argument('-g', "--gg-server", dest='gg_server',
                        help="Google Elevation API server URL",
                        default='maps.googleapis.com')
    parser.add_argument('-i', "--input", dest='input', metavar="FILE",
                        help="Input file, depends on mode")
    parser.add_argument('-c', "--classifier", dest='classifier', nargs=2,
                        metavar="INPUT", help="Two files: First contains "
                        "trained classifier, second file is dataset for "
                        "training", default=[common.CLASSIFIER_PATH,
                                             common.CLASSIFIER_DATA_PATH])
    parser.add_argument('-t', '--train', dest='trainfiles', metavar='FILE',
                        nargs=2,
                        help="First file for training, second file for "
                             "testing. Mode must be set train.")
    parser.add_argument('--show-roc', dest='show_roc', action='store_true',
                        help="Shows ROC curve during training")
    parser.add_argument('-d', '--download', dest='download', metavar='FILE',
                        help="Filename to which world will be stored")
    parser.add_argument('-o', "--output", dest='output', metavar="OUTPUT",
                        help="Output file, content depends on the mode")
    parser.add_argument('-w', '--world', dest='world', metavar='FILE',
                        help='File containing described world')
    parser.add_argument('-l', "--log", action='store', dest='log',
                        metavar="FILE",
                        help="File for logging. If - is used, logging to "
                             "stdout")
    parser.add_argument('-e', "--debug", action='store', dest='debug',
                        metavar="LEVEL",
                        help="Debug level for logging messages")
    parser.add_argument('-r', '--rect', action='store', dest='rect', nargs=4,
                        metavar="FLOAT", type=float,
                        help="Coordinates for world rectangle: left bottom "
                             "right top")
    parser.add_argument('-k', '--kernel', action='store', dest='kernel',
                        metavar="STRING", type=str,
                        choices=['linear', 'polynomial', 'gaussian'],
                        default='gaussian', help="Kernel for SVM training")
    parser.add_argument('-p', '--penalty', action='store', dest='penalty',
                        metavar="FLOAT", type=float, default=10.0,
                        help="C parameter (penalty) for SVM training")
    parser.add_argument('-a', '--gamma', action='store', dest='gamma',
                        metavar="FLOAT", type=float, default=None,
                        help="Gamma or degree of polynom for SVM kernel "
                             "training")
    parsed_args = parser.parse_args(args)
    if parsed_args.mode == 'build':
        if parsed_args.rect is None:
            parser.error("Rectangle (-r, --rect) parameter required for"
                         " build mode")
    elif parsed_args.mode == 'view':
        if parsed_args.world is None:
            parser.error("World (-w, --world) parameter is required for"
                         " view mode")
    elif parsed_args.mode == 'download':
        if parsed_args.rect is None:
            parser.error("Rectangle (-r, --rect) parameter required for"
                         " download mode")
        if parsed_args.download is None:
            parser.error("File into map rectangle will be downloaded is "
                         "requiered in download mode.")
    elif parsed_args.mode == 'collect':
        if parsed_args.rect is None and parsed_args.input is None:
            parser.error("Either input file (-i, --input) containing XML "
                         "or rectangle (-r, --rect) must be given for "
                         "collect mode")
    elif parsed_args.mode == 'train':
        if parsed_args.trainfiles is None:
            parser.error("Trainfile (-t, --train) parameter is required "
                         "for train mode giving files containing data "
                         "for training and testing.")
    return parsed_args


def download(borders, filename, server):
    """
    Downloads the map and saves to file

    @param borders: Tuple with borders coordinations (left, bottom, right, top)
    @param filename: Name of file where XML will be stored
    @param server: OpenStreetMap server
    """
    with OsmApi(server) as osm:
        xmlmap = osm.get_xml_map(borders)
        with open(filename, 'w') as target_file:
            target_file.write(xmlmap)


def run_mode(args):
    """
    Runs the program in mode according args

    @params args: Parsed cmd line arguments
    """
    logger = logging.getLogger(common.LOGGER_NAME)
    if args.mode == 'collect':
        mode = modes.CollectMode(args.osm_server, args.rect, args.input,
                                 args.output)
    elif args.mode == 'download':
        logger.info("Running in download mode, downloading "
                    "coordinates %.2f %.2f %.2f %.2f to file %s",
                    args.rect[0], args.rect[1], args.rect[2], args.rect[3],
                    args.download)
        download(tuple(args.rect),
                 args.download, args.osm_server)
        return
    elif args.mode == 'build':
        logger.info("Running in build mode, coordinates are  %.2f %.2f %.2f "
                    "%.2f", args.rect[0], args.rect[1], args.rect[2],
                    args.rect[3])
        mode = modes.BuildMode(tuple(args.rect), args.osm_server,
                               args.gg_server, args.classifier)
    elif args.mode == 'view':
        mode = modes.ViewMode(args.world)
    elif args.mode == 'train':
        mode = modes.TrainMode(args.output, args.trainfiles[0],
                               args.trainfiles[1], args.penalty, args.kernel,
                               args.gamma, args.show_roc)

    mode.start(input=args.input, output=args.output)


def main():
    """
    Main function.

    Parses cmd line arguments, decides what to do based on that.
    """
    args = parse_args(sys.argv[1:])
    init_loggers(args)
    run_mode(args)

if __name__ == "__main__":
    main()
