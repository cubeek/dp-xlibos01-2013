"""
Module implements World class that contains all of entities in the scene
"""
from concurrent.futures import ThreadPoolExecutor
import cPickle
import logging
import os
import sys

from dp import common
from dp.classifier.classifier import BuildingClasses
from dp.computations.geometrfunc import get_distance, get_standard_deviation
from dp.computations.ageometr import Line, Vector2
from dp.config import config_parser
from dp.elements import GlassMesh
from dp.entities import Building, Entity, Way, Ways
from dp.heightmap import PlaneNotFound

LOGGER = logging.getLogger("dpLogger")


class NeighbouringWall(object):
    """
    Wall that is part of another building as well
    """

    def __init__(self, start, end):
        """
        @param start: Start point of this wall
        @param end: End point of this wall
        """
        self.start = start
        self.end = end
        self.x_min = min(self.start.x, self.end.x)
        self.x_max = max(self.start.x, self.end.x)
        self.y_min = min(self.start.y, self.end.y)
        self.y_max = max(self.start.y, self.end.y)
        self.line = Line(start, end)

    def contains(self, wall):
        """
        Returns true if wall is part of this wall

        @param wall: Foreign wall to test
        """
        if self.x_min <= wall.start.x <= self.x_max and \
                self.y_min <= wall.start.z <= self.y_max and \
                self.x_min <= wall.end.x <= self.x_max and \
                self.y_min <= wall.end.z <= self.y_max:
            start = Vector2(wall.start.x, wall.start.z)
            end = Vector2(wall.end.x, wall.end.z)
            return self.line.has_point(start) and self.line.has_point(end)
        return False


class PickableWorld(object):
    """
    This class holds neccesarry information about world hence it can be 
    saved
    """

    def __init__(self, world):
        """
        Prepares structure for pickle

        @param world: World object
        """
        self.meshes = [(world.projector.width, world.projector.height)]
        glasses = list()
        if world.heightmap is not None:
            indices = list()
            vertices = [(
                vertex.x, vertex.y, vertex.z,
                vertex.normal.x, vertex.normal.y, vertex.normal.z,
                vertex.s, vertex.t)
                for vertex in world.heightmap.vertices]
            for face1, face2, face3 in world.heightmap.faces:
                indices.append(face1.index)
                indices.append(face2.index)
                indices.append(face3.index)
            texture = world.heightmap.texture
            self.meshes.append((vertices, indices, texture, 'hm'))
        for mesh in world.ways.meshes:
            self._store_mesh(mesh, 'way')
        for building in world.buildings:
            for mesh in building.meshes:
                if isinstance(mesh, GlassMesh):
                    glasses.append(mesh)
                else:
                    self._store_mesh(mesh, 'building')
        for glass in glasses:
            self._store_mesh(glass, 'building')

    def _store_mesh(self, mesh, mesh_type):
        """
        Stores one mesh

        @param mesh: Mesh to store
        """
        vertices = [(
            vertex.x, vertex.y, vertex.z,
            vertex.normal.x, vertex.normal.y, vertex.normal.z,
            vertex.s, vertex.t) for vertex in mesh.vertices]
        indices = [index for index in mesh.indices]
        texture = mesh.texture
        self.meshes.append((vertices, indices, texture, mesh_type))

    def save(self, filename):
        """
        Saves world to file

        @param filename: File in which world will be saved
        """
        with open(filename, 'w') as world_file:
            cPickle.dump(self.meshes, world_file)


def draw_intersections(heightmap, intersections, w1, w2, filename):
    from Polygon.IO import writeSVG
    from Polygon.Shapes import Circle
    from dp.elements import rotated_rectangle

    polygons = list()
    axis_cnt = len(heightmap._heights[0])
    for i, x_p in enumerate(heightmap._heights[0]):
        next = i + 1
        x_last = heightmap._heights[-1][i]
        first = Vector2(x_p['x'], x_p['z'])
        last = Vector2(x_last['x'], x_last['z'])
        polygons.append(rotated_rectangle(first, last, 1))
        if next < axis_cnt:
            x_last = heightmap._heights[-next][-1]
            last = Vector2(x_last['x'], x_last['z'])
            polygons.append(rotated_rectangle(first, last, 1))

    for next, y_p in enumerate(heightmap._heights, 1):
        first = Vector2(y_p[0]['x'], y_p[0]['z'])
        last = Vector2(y_p[-1]['x'], y_p[-1]['z'])
        polygons.append(rotated_rectangle(first, last, 1))
        if 1 < next < axis_cnt:
            x_last = heightmap._heights[-1][-next]
            last = Vector2(x_last['x'], x_last['z'])
            polygons.append(rotated_rectangle(first, last, 1))

    for inter in intersections:
        polygons.append(Circle(3, (inter.x, inter.y), 8))

    polygons.append(rotated_rectangle(w1, w2, 1))
    writeSVG("%s.svg" % filename, polygons)


class NoProjectorException(Exception):
    pass


class World(object):
    """
    This class represents world and encapsulates height map and entities
    in the map
    """

    def __init__(self):
        """
        C'tor of the world
        """
        LOGGER.debug("New world created")
        self.buildings = list()
        self.ways = Ways()
        self.entities = list()
        self.size = (0, 0)
        self.heightmap = None
        self.bounds = None
        self.projector = None

    def transform_to_cartesian(self, entity):
        """
        Transforms gps coordinates to cartesian
        """
        if self.projector is None:
            raise NoProjectorException("Projector is not created!")
        if entity.coordinates_type == Entity.CARTESIAN_TYPE:
            LOGGER.warning("Transforming entity that is already transformed")
            return
        new_nodes = list()
        while entity.nodes:
            node = entity.nodes.pop(0)
            x, y = self.projector.get_point(node, in_degrees=True)
            new_nodes.append(Vector2(x, y))
        entity.nodes = new_nodes
        entity.coordinates_type = Entity.CARTESIAN_TYPE

    def append(self, entity):
        """
        Adds entity to world. It's saved to list according it's type and
        triangularized
        """
        self.transform_to_cartesian(entity)
        if isinstance(entity, Building):
            self._process_building(entity, True)
        elif isinstance(entity, Way):
            self.ways.append(entity)
        else:
            self.entities.append(entity)

    def process_entities(self, entities, parallel=True):
        """
        Transforms GPS coordinates to cartesian, prepares data for GPU

        @param entities: List of entities from OSM parser
        """
        LOGGER.info("Pre-processing entities")
        if parallel:
            with ThreadPoolExecutor(max_workers=common.MAX_WORKERS) \
                    as executor:
                results = executor.map(self.process_entity, entities)
                for res in results:
                    pass
        else:
            for entity in entities:
                self.process_entity(entity)
        self.ways.create_way()
        LOGGER.info("Setting heights for ways")
        way_eps = config_parser.getfloat('heightmap', 'way_height_eps')
        if parallel:
            results = list()
            with ThreadPoolExecutor(max_workers=common.MAX_WORKERS) \
                    as executor:
                for mesh in self.ways.meshes:
                    results.append(executor.submit(self.set_heights,
                        mesh.vertices, way_eps))
                for res in results:
                    if res.exception():
                        raise res.exception()
        else:
            for mesh in self.ways.meshes:
                self.set_heights(mesh.vertices, way_eps)
        LOGGER.info("Heights for ways has been set")

    def process_entity(self, entity):
        """
        Transforms entity to cartesian coordinations and adds to lists for
        given entity
        """
        self.transform_to_cartesian(entity)
        if isinstance(entity, Building):
            self._process_building(entity)
        elif isinstance(entity, Way):
            self._process_way(entity)
        else:
            self.entities.append(entity)

    def _process_way(self, way):
        """
        Processes one way, computes heights, collisions, triangles

        @param way: Way entity
        """
        self.set_hm_entity_intersections(way)
        self.ways.append(way)

    def _process_building(self, building, platform=False):
        """
        Creates features of building and adds to list

        @param building: Building object
        @param platform: If True, generates platform mesh
        """
        self.buildings.append(building)
        building.create_features()
        if platform:
            building.platform_mesh()

    def extend_buildings_features(self, parallel=True):
        """
        Extend buildings features with standard deviation of surrounding
        buildings
        """
        LOGGER.info("Extending buildings' features")

        def extend_features(building):
            """
            Computes near entities and extents building's features with
            standard deviation

            @param building: Building entity
            """
            near_b = self.get_near_buildings(building, parallel=not parallel)
            feature_keys = building.features.keys()
            building.features.update(get_standard_deviation(near_b,
                                                            feature_keys))
        if parallel:
            with ThreadPoolExecutor(max_workers=common.MAX_WORKERS) as \
                    executor:
                results = executor.map(extend_features, self.buildings)
                for res in results:
                    pass
        else:
            for building in self.buildings:
                extend_features(building)

    def set_entrance(self, building):
        """
        Set entrance to building according nearest way

        @param building: Buildig to set etnrance for
        """
        try:
            nearest_node = self.ways[0].nodes[0]
        except (KeyError, IndexError):
            building.entrance = (building.nodes[0], building.nodes[1])
            return
        min_distance = float('inf')
        for way in self.ways:
            for node in way:
                if get_distance(node, building.position) < min_distance:
                    nearest_node = node
        for first, second in zip(building.nodes, building.nodes[1:] +
                                 [building.nodes[0]]):
                distance = get_distance(first, nearest_node) + \
                    get_distance(second, nearest_node)
                if distance < min_distance:
                    building.entrance = (first, second)
                    min_distance = distance
        LOGGER.debug("Building entrance set to %s", building.entrance)

    def get_near_buildings(self, building, radius=200, parallel=True):
        """
        Computes list of entities from entities within world instance that
        are within the radius with center specified by entity

        @param entity: Entity defining center of the radius
        @param radius: Radius in meters
        """

        def less_than(building, other, radius):
            distance = get_distance(building.position, other.position)
            return distance < radius

        if parallel:
            near_buildings = list()

            def get_near_buildings_part(ref_building, other, radius):
                """
                For parallel manner, adds building to list if it's near

                @param ref_building: Reference building
                @param other: Potentional near building
                @param radius: How far can building be to be considered as near
                @param near_buildings: Result list where near building will be
                                       put
                """
                if ref_building != other and less_than(ref_building, other,
                                                       radius):
                    return other
                return None

            with ThreadPoolExecutor(max_workers=common.MAX_WORKERS) as \
                    executor:
                results = list()
                for other in self.buildings:
                    results.append(executor.submit(get_near_buildings_part,
                                                   building, other, radius))
                    for res in results:
                        if res.exception():
                            raise res.exception()
                        elif res.result() is not None:
                            near_buildings.append(res.result())

        else:
            near_buildings = [b for b in self.buildings
                              if b is not building and
                              less_than(building, b, radius)]

        return near_buildings

    def set_buildings_neighbours(self, parallel=True):
        """
        Sets neighbours for all buildings in the world
        """
        LOGGER.info("Setting buildings' neighbours")
        if parallel:
            with ThreadPoolExecutor(max_workers=common.MAX_WORKERS) as\
                    executor:
                result = executor.map(self.set_building_neighbours,
                                      self.buildings)
                for res in result:
                    pass
        else:
            for building in self.buildings:
                self.set_building_neighbours(building)

    def set_building_neighbours(self, building):
        """
        Sets neighbours building for given building

        @param building: Buidling we want neighbours of
        """
        near_buildings = self.get_near_buildings(building, parallel=False)
        for node_i, node in enumerate(building.nodes):
            for other in near_buildings:
                for on_i, other_node in enumerate(other.nodes):
                    if other_node == node:
                        building.neighbours.add(other)
                        o_next = other.nodes[(on_i + 1) % len(other.nodes)]
                        o_prev = other.nodes[on_i - 1]
                        node_next = building.nodes[(node_i + 1) %
                                                   len(building.nodes)]
                        node_prev = building.nodes[node_i - 1]
                        if o_prev == node_prev or o_next == node_prev:
                            building.n_walls.append(
                                NeighbouringWall(node_prev, node))
                        elif o_prev == node_next or o_next == node_next:
                            building.n_walls.append(
                                NeighbouringWall(node, node_next))

        building.features['neighbours'] = len(building.neighbours)

    def save_features_to_file(self, filename):
        """
        Saves the features in format for training SVM

        @param filename: File to save features to
        """
        def features_to_string(e, feature_keys):
            if e.building_class == BuildingClasses.NOTHING:
                return ""
            feature_line = "%d %s\n" % (e.building_class, e.feature_string())

            return feature_line

        if not self.buildings:
            return
        feature_keys = self.buildings[0].features.keys()
        feature_keys.sort()
        header = "# class  "
        for num, key in enumerate(feature_keys):
            header += " %d:%s " % (num+1, key)
        header += "\n"

        feature_lines = [features_to_string(building, feature_keys)
                         for building in self.buildings]

        if filename is None:
            feature_lines.insert(0, header)
            sys.stdout.writelines(feature_lines)
        else:
            if os.path.exists(filename):
                mode = 'a'
            else:
                feature_lines.insert(0, header)
                mode = 'w'
            with open(filename, mode) as f:
                f.writelines(feature_lines)
            LOGGER.info("Features saved to %s", filename)

    def set_hm_entity_intersections(self, entity):
        """
        Gets all entity intersections with height map

        @param entity: Entity that intersects heightmap
        """
        first = entity.nodes.pop(0)
        new_nodes = [first]
        while entity.nodes:
            second = entity.nodes.pop(0)
            edge = Line(first, second)
            intersections = self.heightmap.get_intersections(edge, (first,
                                                                    second))
            intersections.sort(key=lambda x: (first - x).length)
            new_nodes.extend(intersections)
            new_nodes.append(second)
#            draw_intersections(self.heightmap, intersections, first, second,
#                    id(second))
            first = second
        entity.nodes = new_nodes

    def set_vertex_height(self, vertex, eps):
        """
        Sets the vertex height according heightmap

        @param vertex: Vertex object
        """
        try:
            plane = self.heightmap.get_plain(vertex.x, vertex.z)
            vertex.y = plane.get_y(vertex.x, vertex.z) + eps
        except PlaneNotFound:
            pass

    def set_heights(self, vertices, eps, parallel=False):
        """
        Sets heights to each vertex according heightmap

        @param vertices: List of vertices to set height of
        @param eps: Little value that is added to height
        @param parallel: Process each vertex in different thread
        """
        if parallel:
            results = list()
            with ThreadPoolExecutor(max_workers=common.MAX_WORKERS) as \
                    executor:
                for vertex in vertices:
                    results.append(executor.submit(self.set_vertex_height,
                                                   vertex, eps))
                for result in results:
                    if result.exception():
                        raise result.exception()
        else:
            for vertex in vertices:
                self.set_vertex_height(vertex, eps)

    def save_to_file(self, filename):
        """
        Saves world object to file

        @param filaname: Path to the file where world will be stored
        """
        pickable_world = PickableWorld(self)
        pickable_world.save(filename)

    @classmethod
    def load_from_file(cls, filename):
        """
        Loads world object from filename

        @param filename: Path to the file containing saved world
        """
        with open(filename, 'r') as world_fh:
            world = cPickle.load(world_fh)
        return world
