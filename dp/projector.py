"""
Projector module, maps GPS coordinates into cartesian
"""

import logging
import math

from dp import common
from dp.computations.geometrfunc import sphere_distance

LOGGER = logging.getLogger("dpLogger")
#logging.basicConfig(level=logging.DEBUG)

class Projector(object):
    """
    This class computes GPS coordinations to cartesian coordinations
    """

    def __init__(self, left, right, bottom, top):
        """
        Constructor that stores boundries of the map for future computations
        @param left: Left boundry (left longitude)
        @param right:  Right boundry (right longitude)
        @param up: Up boundry (top latitude)
        @param bottom: Bottom boundry (bottom latitude)
        """
        self.bounds = (left, bottom, right, top)
        self.gps_origin_deg = (bottom, left)
        self.gps_boundry_deg = (top, right)
        self.gps_origin = (math.radians(bottom), math.radians(left))
        self.gps_boundry = (math.radians(top), math.radians(right))

        self.width = sphere_distance((self.gps_origin), (self.gps_origin[0],
                               self.gps_boundry[1]))
        self.height = sphere_distance((self.gps_origin), (self.gps_boundry[0],
                                self.gps_origin[1]))

        LOGGER.debug("World size: %f x %f", self.width, self.height)

        self.mid_point = (round((self.gps_boundry[0]+self.gps_origin[0])/2,
                                common.ROUND_VAL),
                          round((self.gps_boundry[1]+self.gps_origin[1])/2,
                                 common.ROUND_VAL))

        self.base = self.get_point(self.gps_origin, normalize=False)

        normx, normy = self.get_point(self.gps_boundry, normalize=False)
        norm = (normx - self.base[0], normy - self.base[1])

        self.normalize_x = (self.width/norm[0]) if norm[0] != 0 else self.width
        self.normalize_y = (self.height/norm[1]) if norm[1] != 0 else\
                           self.height
        LOGGER.info("Projector based on %f, %f, %f, %f created", left, bottom,
                    right, top)

    def __str__(self):
        retval = "projector\n========\n"
        retval += "Origin: (%f, %f)\n" % self.gps_origin_deg
        retval += "Boundry: (%f, %f)\n" % self.gps_boundry_deg
        retval += 'Width: %f\tHeight: %f\n' % (self.width, self.height)


        return retval


    def get_points(self, points):
        """
        Computes for each point in a list new position
        @param points: List of tuples (longitude, latitude)
        @return: List of newly computed points
        """
        return [self.get_point(point) for point in points]

    def get_point(self, point, normalize=True, in_degrees=False):
        """
        Projects point
        @param point: tuple of (latitude, longitude)
        @return: New point stored in a tuple
        """
        old_point = None
        if in_degrees:
            old_point = (round(point[0], common.ROUND_VAL), 
                              round(point[1], common.ROUND_VAL))
            point = (math.radians(old_point[0]), math.radians(old_point[1]))
        cosc = math.sin(point[0])*math.sin(self.mid_point[0])-\
                math.cos(point[0])*math.cos(self.mid_point[0])*\
                math.cos(self.mid_point[1]-point[1])

        x = (math.cos(self.mid_point[0])*math.sin(self.mid_point[1]-point[1]))\
             /cosc
        y = (math.cos(point[0])*math.sin(self.mid_point[0])-\
             math.sin(point[0])*math.cos(self.mid_point[0])*\
             math.cos(self.mid_point[1]-point[1]))/cosc

        if normalize:
            x -= self.base[0]
            y -= self.base[1]
            x *= self.normalize_x
            y *= self.normalize_y

        x, y = round(x, common.ROUND_VAL), round(y, common.ROUND_VAL)

#        if old_point:
#            LOGGER.debug("Mapping point: %s -> %s -> %s", old_point,
#                    point, (x,y))
#        else:
#            LOGGER.debug("Mapping point: %s -> %s", point, (x, y))
#
#        if x < 0 or y < 0:
#            LOGGER.warning("Point out of the rendering rectangle: %s mapped "
#                           "to %s", point, (x,y))
        return x, y

