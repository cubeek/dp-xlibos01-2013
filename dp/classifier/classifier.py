"""
Module implementing SVM classifier
"""
import cPickle
import logging
from PyML import SparseDataSet, SVM, ker
from PyML.classifiers.multi import OneAgainstRest
import os

from dp.view import renderer

DEFAULT_CLASSIFIER_PATH=os.path.join(__file__, "building_classifier.svm")
LOGGER = logging.getLogger("dpLogger")


class BuildingClasses(object):
    #BOF - Block of flats
    #OLD - Old appartments
    #HOUSE - Family house
    #OTHER - Other
    NOTHING = renderer.NOTHING
    BOF = renderer.BOF
    OLD = renderer.OLD
    HOUSE = renderer.HOUSE
    OTHER = renderer.OTHER
    CLASS_TO_STRING = {
        BOF: 'bof',
        OLD: 'old',
        HOUSE: 'house',
        OTHER: 'other',
    }


class BuildingClassifier(object):
    """
    This object encapsulates classifier of buildings' platforms
    """

    def __init__(self, filename, training_file, testing_file=None,
                 show_roc=False):
        """
        Constructor of classifier

        @param filename: Path to file containing classifier
        @param training_file: Path to file containing trainign data
        @param testing_file: Path to file containig testing data
        """
        self.classifier = None
        self.kernel = None
        self.show_roc = show_roc
        self.classifier_file = filename
        self.training_file = training_file
        self.testing_data = SparseDataSet(testing_file) if testing_file \
                            is not None else None
        self.training_data = SparseDataSet(training_file)

    def __enter(self):
        """
        """
        return self

    def get_kernel(self, kernel_name, gamma):
        """
        Creates kernel

        @param kernel: name of kernel to use
        @param gamma: Parameter for kernel
        @returns: Kernel object
        """
        if kernel_name == 'gaussian':
            kernel = ker.Gaussian()
            if gamma is not None:
                kernel.gamma = gamma
        elif kernel_name == 'polynomial':
            kernel = ker.Polynomial()
            if gamma is not None:
                kernel.degree = gamma
        elif kernel_name == 'linear':
            kernel = ker.Linear()
        return kernel

    def __exit__(self, value, type_, traceback):
        """
        """
        pass

    def load(self):
        """
        Loads trained classifier from specified file

        @param filename: Path to the classifier's file
        """
        self.classifier = OneAgainstRest(SVM())
        self.classifier.load(self.classifier_file, self.training_data)
        direc = os.path.dirname(self.classifier_file)
        filename = ".%s.params" % os.path.basename(self.classifier_file)
        with open(os.path.join(direc, filename), 'r') as param_file:
            params = cPickle.load(param_file)
        self.classifier.classifier.C = params[0]
        self.classifier.classifier.kernel = \
            self.get_kernel(params[1], params[2])

    def save(self):
        """
        Saves the classifier to given filename

        @param filename: Path to file classifier will be saved into
        """
        def get_classifier_params():
            """
            Returns tuple of params for classifier
            """
            C = self.classifier.classifier.C
            gamma = None
            if isinstance(self.classifier.classifier.kernel, ker.Gaussian):
                kernel = 'gaussian'
                gamma = self.classifier.classifier.kernel.gamma
            elif isinstance(self.classifier.classifier.kernel, ker.Polynomial):
                kernel = 'polynomial'
                gamma = self.classifier.classifier.kernel.degree
            elif isinstance(self.classifier.classifier.kernel, ker.Linear):
                kernel = 'linear'
            return C, kernel, gamma

        if self.classifier_file is not None:
            self.classifier.save(self.classifier_file)
            direc = os.path.dirname(self.classifier_file)
            filename = ".%s.params" % os.path.basename(self.classifier_file)
            params = get_classifier_params()
            with open(os.path.join(direc, filename) ,'w') as param_file:
                cPickle.dump(params, param_file)
        else:
            print self.classifier

    def create_classifier(self, C, kernel, gamma):
        """
        Creates, trains and tests new classifier

        @param C: C parameter for SVM classifier
        @param kernel: Kernel function for dataset
        @param gamma: Kernel parameter
        """
        self.kernel = self.get_kernel(kernel, gamma)
        self.init_classifier(C)
#        self.experimental()
#        return
        self.train()
#        self.cv()
        print self.test()

    def experimental(self):
        """
        Helping function that experiments with parameters
        """
        cs = [0.001, 0.01, 0.1, 1.0, 10.0, 100.0, 1000.0, 10000.0]
        gammas = [0.001, 0.01, 0.1, 1.0, 10.0, 100.0, 1000.0, 10000.0]
        for c in cs:
            for gamm in gammas:
                self.classifier.classifier.kernel.gamma = gamm
                self.classifier.classifier.C = c
                self.train()
#                resultcv = self.cv()
                resultt = self.test()
#                with open('%f-%f-cv.txt' % (c, gamm), 'w') as fh:
#                    fh.write("%s" % resultcv)
                with open('%f-%f-test.txt' % (c, gamm), 'w') as fh:
                    fh.write("%s" % resultt)

    def init_classifier(self, C):
        """
        Initialize new OneAgainstRest classifier

        @param C: Parameter C for SVM classifier penalization
        """
        svm = SVM(self.kernel, C=C)
        self.classifier = OneAgainstRest(svm)

    def cv(self):
        """
        Performs cross-validation above given data
        """
        result = self.classifier.cv(self.testing_data)
        if self.show_roc:
            print result
            result.plotROC(
                'roc-%d.pdf' % (self.classifier.classifier.C),
                show=self.show_roc)
        return result

    def train(self):
        """
        Trains the classifier by given training dataset
        """
        self.classifier.train(self.training_data)

    def test(self):
        """
        Tests the classifier with given testing dataset

        @param show: If True, ROC curve will be displayed
        """
        result = self.classifier.test(self.testing_data)
        if self.show_roc:
            print result
            result.plotROC(
                'roc-%d.pdf' % self.classifier.classifier.C,
                show=self.show_roc)
        return result

    def classify(self, features):
        """
        Classifies building 

        @params features: Dictionary containing feature vector
        """
        features = [features[1], features[16], features[17]]
        data = SparseDataSet([features])
        bof = (0, self.classifier.classifiers[0].decisionFunc(data, 0))
        old = (1, self.classifier.classifiers[1].decisionFunc(data, 0))
        house = (2, self.classifier.classifiers[2].decisionFunc(data, 0))
        other = (3, self.classifier.classifiers[3].decisionFunc(data, 0)/100.0)
        classified = max(bof, old, house, other,key=lambda x: x[1])[0]
        LOGGER.debug("Building classified as %s",
                     BuildingClasses.CLASS_TO_STRING[classified])
        return classified
