#!/bin/bash

name=`uname`
if [ "x$name" == "xDarwin" ]; then
    library=dp/view/libs/librenderer.dylib
elif [ "x$name" == "xLinux" ]; then
    library=librenderer.so
else
    echo "Unknown OS $name"
fi

~/ctypesgen-read-only/ctypesgen.py -l $library dp/view/renderer.h -o dp/view/renderer.py

rc=$?

if [ $rc -eq 0 ]; then
    echo "Finished successfully"
else
    echo "Failed with return code $rc"
fi

